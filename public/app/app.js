'use strict';

angular.module('BlurAdmin', [
    'ngAnimate',
    'ngSanitize',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'ngTouch',
    'toastr',
    'ngFileUpload',
    'infinite-scroll',
    'smart-table',
    'ngFileUpload',
    'datatables',
    "xeditable",
    'ui.slimscroll',
    'ngJsTree',
    'angular-progress-button-styles',
    'ngTagsInput',
    'BlurAdmin.theme',
    'BlurAdmin.pages'
]);
