/**
 * Created by Raza on 7/18/2017.
 */
(function () {
    angular.module('BlurAdmin.pages').service('GlobalServices', GlobalServices);

    GlobalServices.$inject = ['$q', 'apiService'];
    function GlobalServices($q, apiService) {

        var GlobalServices = {};

        var currentUser = function () {
            var deferred = $q.defer();
            apiService.get("user/currentAccount", {}).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        GlobalServices.currentUser = currentUser;
        return GlobalServices;
    }
})();
