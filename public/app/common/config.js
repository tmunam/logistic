﻿var appConfig = {
    title: "Logistics Admin",
    lang: "en",
    dateFormat: "dd/mm/yy",
    //apiBase: 'http://54.191.103.99:3200/api/v1/',
    apiBase: window.location.protocol + "//" + window.location.host + "/api/v1/",
};
