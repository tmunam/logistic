/**
 * Created by Affan on 8/18/2017.
 */
(function() {
  angular.module('BlurAdmin.pages').service('share', share);
  share.$inject = ['$q', 'apiService'];
  function share($q, apiService) {
    var share = {};
    var productList = null;
    var addProduct = function(newObj) {
      productList = newObj;
    };
    var getProducts = function() {
      return productList;
    };
    return {
      addProduct: addProduct,
      getProducts: getProducts
    };
    share.addProduct = addProduct;
    share.getProducts = getProducts;
    // share.addProduct = addProduct;
    return GlobalServices;
  }
})();
