/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme.components')
        .controller('BaSidebarCtrl', BaSidebarCtrl);

    /** @ngInject */
    function BaSidebarCtrl($scope, baSidebarService, localStorageService, GlobalServices) {

        GlobalServices.currentUser().then(function (data) {
            if (data.success === 1) {
                $scope.menuItems = baSidebarService.getMenuItems(data.data);
                $scope.defaultSidebarState = $scope.menuItems[0].stateRef;
            }
        });


        $scope.hoverItem = function ($event) {
            $scope.showHoverElem = true;
            $scope.hoverElemHeight = $event.currentTarget.clientHeight;
            var menuTopValue = 66;
            $scope.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - menuTopValue;
        };

        $scope.$on('$stateChangeSuccess', function () {
            if (baSidebarService.canSidebarBeHidden()) {
                baSidebarService.setMenuCollapsed(true);
            }
        });
        $scope.user = null;

        if (localStorageService.get('user')) {
            $scope.user = localStorageService.get('user');
        }

        $scope.$on('userLogin', function (event, data) {
            $scope.user = localStorageService.get('user');
        });

        $scope.$on('userLogout', function (event, data) {
            $scope.user = localStorageService.get('user');
        });
    }
})();