﻿
(function() {
  angular.module('login').controller("loginCtrl", loginCtrl);

  loginCtrl.$inject = ['$scope', '$state', 'loginService', 'usSpinnerService', 'localStorageService', 'toastr'];

  function loginCtrl($scope, $state, loginService, usSpinnerService, localStorageService, toastr) {
    var login = this;

    login.signInUser = {};

    //access login
    login.login = function(data) {
      usSpinnerService.spin('spinner-1');
      return loginService.authenticateUser(data).then(function(response) {
        if (response.success == "1") {
          usSpinnerService.stop('spinner-1');
          localStorageService.set("user", response.data);
          $scope.user = localStorageService.get('user');
          toastr.success("Successfully logged into the system", 'Login Success!');
          $scope.$emit('userLogin', {
            user: $scope.user
          });
          //$state.go('dashboard');
            if (response.data.userType == 'ADMIN')
                window.open("/", "_self");
            else
                window.open("/invoices", "_self");
        } else {
          usSpinnerService.stop('spinner-1');
          toastr.error('Invalid email or password');
        }
      });
    };

    //registration
    login.register = function() {
      $state.go('signup');
    };
  }

  angular.module('login').controller("SignOutController", SignOutController);

  SignOutController.$inject = ['$scope', '$state', 'localStorageService', 'GlobalServices', 'logOutService'];

  function SignOutController($scope, $state, localStorageService, GlobalServices, logOutService) {
    checkCurrentUser();

    function checkCurrentUser() {
      return GlobalServices.currentUser().then(function(data) {
        if (data.success === 1) {
          var user = localStorageService.get('user');
          if (user !== null) {
            localStorageService.remove('user');
          }
          logOutService.logOutUser().then(function(res) {
            if (res.success === 1) {
              $scope.user = null;
              //$scope.$broadcast('userLogout', {user: $scope.user});
              $scope.$emit('userLogout', {
                user: $scope.user
              });
            }
            $state.go("login");
          });
        } else {
          $state.go("login");
        }
      });
    }
  }
})();
