
(function () {
    'use strict';

    angular.module('expenses', []).config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {

        function authentication(GlobalServices, $q, localStorageService, $state) {
            var d = $q.defer();
            var checkUser = localStorageService.get("user");
            if (checkUser !== null) {
                d.resolve(checkUser);
            } else {
                GlobalServices.currentUser().then(function (data) {
                    if (data.success === 0) {
                        $state.go('login');
                    } else {
                        localStorageService.set('user', data.data.account);
                        d.resolve(data.user);
                    }
                });
            }
            return d.promise;
        }
        function isLogin($q, localStorageService, $state,$location) {
          var d = $q.defer();
          var checkUser = localStorageService.get("user");
          if (checkUser !== null) {
            d.resolve(checkUser);
          } else {
            // $state.go('login');
            $location.url('/login')

          }
          return d.promise;
        }


        $stateProvider
            .state('expensesState', {
                url: '/expenses',
                templateUrl: 'app/pages/expenses/expensesListing.html',
                controller: 'expensesController',
                controllerAs: "exp",
                title: 'Expenses',
                /*sidebarMeta: {
                    icon: 'fa fa-motorcycle',
                    order: 0,
                },*/
                resolve: {
                    $user: isLogin
                },

            }).state('dailyExpenses', {
                url: '/espenses/dailyexpenses',
                templateUrl: 'app/pages/expenses/expensesListing.html',
                controller: 'dailyExpensesController',
                controllerAs: "exp",
                title: 'Daily Expenses',
                /*sidebarMeta: {
                    icon: 'fa fa-motorcycle',
                    order: 0,
                },*/
                resolve: {
                    $user: isLogin
                },

            }).state('repairMaintenance', {
                url: '/expenses/repairmaintenance',
                templateUrl: 'app/pages/expenses/expensesListing.html',
                controller: 'repairMaintenanceController',
                controllerAs: "exp",
                title: 'Repair & Maintenance',
                /*sidebarMeta: {
                    icon: 'fa fa-motorcycle',
                    order: 0,
                },*/
                resolve: {
                    $user: isLogin
                },

            }).state('tptFee', {
                url: '/expenses/tptFee',
                templateUrl: 'app/pages/expenses/expensesListing.html',
                controller: 'tptFeeController',
                controllerAs: "exp",
                title: 'Annual Token & Route Permit & Tranfer Fee',
                /*sidebarMeta: {
                    icon: 'fa fa-motorcycle',
                    order: 0,
                },*/
                resolve: {
                    $user: isLogin
                },

            })

        //$urlRouterProvider.when('/riders','/vehicles');
    }

})();
