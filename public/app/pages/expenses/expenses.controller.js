(function() {
    angular.module('expenses').controller("expensesController", expensesController);

    expensesController.$inject = ['$scope', 'toastr', '$state', 'expensesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', '$state', 'Upload', '$timeout', '$location'];

    function expensesController($scope, toastr, $state, expensesServices, share, usSpinnerService, $uibModal, $compile, $state, Upload, $timeout, $location) {
        var exp = this;
        exp.page = 0;
        exp.limit = 100;
        exp.expenses = [];
        exp.addExpense = addExpense;

        getExpense();

        function getExpense(){
            var params = {
                offset: exp.page,
                limit: exp.limit,
                text: {
                    searchText: ''
                }
            };

            expensesServices.getExpenses(params).then(function(response) {
                if (response.data){
                    exp.expenses = response.data.expenses;
                }
            });
        }

        function addExpense() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/expenses/addEditExpense.html",
                size: "lg",
                controller: 'AddEditExpenseController',
                controllerAs: 'vm',
                windowClass : 'invoices-modal',
                resolve: {
                    $expense: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                exp.page = 0;
                exp.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('expenses').controller("dailyExpensesController", dailyExpensesController);

    dailyExpensesController.$inject = ['$scope', 'toastr', '$state', 'expensesServices', 'invoicesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', '$state', 'Upload', '$timeout', '$location'];

    function dailyExpensesController($scope, toastr, $state, expensesServices, invoicesServices, share, usSpinnerService, $uibModal, $compile, $state, Upload, $timeout, $location) {
        var exp = this;
        exp.page = 0;
        exp.limit = 100;
        exp.expenses = [];
        exp.dailyExpenseType = ['Tool Tax', 'Police Fine', 'Diesel', 'Off-loading Expenses', 'Other Exp'];
        exp.selectedVehicle = '';
        exp.vehiclesList = [];
        exp.vehicleChange = vehicleChange;
        exp.addVehicleExpense = addVehicleExpense;

        invoicesServices.getOtherDetials().then(function (res){
            if (res.response === 200){
                exp.vehiclesList = res.data.vehicles;
            }
        });

        function vehicleChange(){
            getvehicleExpense();
        }

        function getvehicleExpense(){
            var param = {
                expenseCategory : 'Daily expenses',
                vehicle_id : exp.selectedVehicle._id,
                offset: exp.limit * exp.page,
                limit: exp.limit,
            }

            expensesServices.getVehicleExpense(param).then(function (res){
                if (res.response === 200){
                    exp.expenses = res.data.expenses;
                }
            });
        }

        function addVehicleExpense() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/expenses/addEditExpense.html",
                size: "lg",
                controller: 'AddEditExpenseController',
                controllerAs: 'vm',
                windowClass : 'invoices-modal',
                resolve: {
                    $expense: function() {
                        return {
                            isNew: true,
                            expenseCategory : 'Daily expenses'
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                exp.page = 0;
                exp.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        }
    }

    angular.module('expenses').controller("repairMaintenanceController", repairMaintenanceController);

    repairMaintenanceController.$inject = ['$scope', 'toastr', '$state', 'expensesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', '$state', 'Upload', '$timeout', '$location'];

    function repairMaintenanceController($scope, toastr, $state, expensesServices, share, usSpinnerService, $uibModal, $compile, $state, Upload, $timeout, $location) {
        var exp = this;
        exp.page = 0;
        exp.limit = 100;
        exp.expenses = [];
        exp.addExpense = addExpense;

        getExpense();

        function getExpense(){
            var params = {
                offset: exp.page,
                limit: exp.limit,
                text: {
                    searchText: ''
                }
            };

            expensesServices.getExpenses(params).then(function(response) {
                if (response.data){
                    exp.expenses = response.data.expenses;
                }
            });
        }

        function addExpense() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/expenses/addEditExpense.html",
                size: "lg",
                controller: 'AddEditExpenseController',
                controllerAs: 'vm',
                windowClass : 'invoices-modal',
                resolve: {
                    $expense: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                exp.page = 0;
                exp.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('expenses').controller("tptFeeController", tptFeeController);

    tptFeeController.$inject = ['$scope', 'toastr', '$state', 'expensesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', '$state', 'Upload', '$timeout', '$location'];

    function tptFeeController($scope, toastr, $state, expensesServices, share, usSpinnerService, $uibModal, $compile, $state, Upload, $timeout, $location) {
        var exp = this;
        exp.page = 0;
        exp.limit = 100;
        exp.expenses = [];
        exp.addExpense = addExpense;

        getExpense();

        function getExpense(){
            var params = {
                offset: exp.page,
                limit: exp.limit,
                text: {
                    searchText: ''
                }
            };

            expensesServices.getExpenses(params).then(function(response) {
                if (response.data){
                    exp.expenses = response.data.expenses;
                }
            });
        }

        function addExpense() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/expenses/addEditExpense.html",
                size: "lg",
                controller: 'AddEditExpenseController',
                controllerAs: 'vm',
                windowClass : 'invoices-modal',
                resolve: {
                    $expense: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                exp.page = 0;
                exp.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }


    angular.module('expenses').controller("AddEditExpenseController", AddEditExpenseController);

    AddEditExpenseController.$inject = ['$scope', 'toastr', '$state', 'expensesServices', 'invoicesServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$expense', 'Upload'];

    function AddEditExpenseController($scope, toastr, $state, expensesServices, invoicesServices, driversServices, usSpinnerService, $uibModalInstance, $expense, Upload) {

        var ctgAE = this;
        ctgAE.isNew = $expense.isNew;
        ctgAE.expenseCategory = $expense.expenseCategory;
        ctgAE.expenseCategoryFullName = '';
        ctgAE.expenseType = [];
        ctgAE.title = "Add New Expense";
        ctgAE.vehiclesList = [];
        ctgAE.expenseData = {};
        ctgAE.currentDateTimeObj = new Date();
        ctgAE.expenseData.expense_date = new Date();

        if (ctgAE.expenseCategory == 'Daily expenses'){
            ctgAE.expenseCategoryFullName = 'Daily Expenses';
            ctgAE.expenseData.expenseCategory = 'Daily expenses'
            ctgAE.expenseType = ['Tool Tax', 'Police Fine', 'Diesel', 'Off-loading Expenses', 'Other Exp'];
        }

        invoicesServices.getOtherDetials().then(function (res){
            if (res.response === 200){
                ctgAE.vehiclesList = res.data.vehicles;
            }
        });

        if (!$expense.isNew) {
            ctgAE.expenseData = $expense.expense;
            ctgAE.title = "Edit Expense";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        ctgAE.addEditExpense = function() {
            if ($expense.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                expensesServices.addVehicleExpense({
                    expense : ctgAE.expenseData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Expense added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                expensesServices.editExpense({
                    expense : ctgAE.expenseData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("KPO Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }

        // Date
        ctgAE.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            //  maxDate: new Date(2020, 5, 22),
            minDate: new Date(new Date().setDate(new Date().getDate()-300)),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            //  return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        ctgAE.toggleMin = function () {
            ctgAE.dateOptions.minDate = ctgAE.dateOptions.minDate;
        };

        ctgAE.popup2 = {
            opened: false
        };

        ctgAE.popup3 = {
            opened: false
        };

        ctgAE.toggleMin();

        ctgAE.open2 = function () {
            ctgAE.popup2.opened = true;
        };

        ctgAE.open3 = function () {
            ctgAE.popup3.opened = true;
        };



        ctgAE.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
        ctgAE.format = ctgAE.formats[1];
    }

    angular.module('expenses').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();