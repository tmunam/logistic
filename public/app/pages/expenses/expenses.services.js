
(function () {
    angular.module('expenses').service('expensesServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getExpenses = function (parameters,search) {
            var deferred = $q.defer();
            var text="expenses/getAllExpenses/" + parameters.offset + "/" + parameters.limit;
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters.text).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addVehicleExpense = function (parameters) {
            var deferred = $q.defer();
            apiService.create("expenses/addVehicleExpense", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var editExpense = function (parameters) {
            var deferred = $q.defer();
            apiService.create("expenses/editExpense", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var getVehicleExpense = function (parameters) {
            var deferred = $q.defer();
            apiService.create("expenses/getVehicleExpense", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };


        confServices.getExpenses = getExpenses;
        confServices.addVehicleExpense = addVehicleExpense;
        confServices.editExpense = editExpense;
        confServices.getVehicleExpense = getVehicleExpense;
        return confServices;

    }]);
})();
