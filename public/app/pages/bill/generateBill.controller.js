
(function() {
    angular.module('bills').controller("generateBillController", generateBillController);

    generateBillController.$inject = ['$window', '$scope', 'toastr', '$state', 'generateBillServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function generateBillController($window, $scope, toastr, $state, generateBillServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var vm = this;
        vm.page = 0;
        vm.limit = 100;
        vm.vehicles = [];
        vm.vehiclescopy = [];

        vm.generateBill = function (){
            var params = {
                startDate : vm.startDate,
                endDate : vm.endDate
            }

            generateBillServices.generateBill(params).then(function (res){
                var landingUrl = $window.location.origin + "/uploads/" + res.data.fileName;
                console.log(landingUrl);
                $window.open(landingUrl, "_blank");
            });
        }

        // date
        vm.startDate = new Date();
        vm.endDate = new Date();

        vm.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            //  maxDate: new Date(2020, 5, 22),
            minDate: new Date(new Date().setDate(new Date().getDate()-300)),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            //  return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        vm.toggleMin = function () {
            vm.dateOptions.minDate = vm.dateOptions.minDate;
        };

        vm.popup2 = {
            opened: false
        };

        vm.popup3 = {
            opened: false
        };

        vm.toggleMin();

        vm.open2 = function () {
            vm.popup2.opened = true;
        };

        vm.open3 = function () {
            vm.popup3.opened = true;
        };



        vm.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
        vm.format = vm.formats[1];

    }

})();
