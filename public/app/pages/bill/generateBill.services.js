/**
 * Created by Raza on 8/7/2017.
 */
(function () {
    angular.module('bills').service('generateBillServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var generateBill = function (parameters) {
            var deferred = $q.defer();
            apiService.create("bill/generateBill", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var downloadFile = function (fileName) {
            var deferred = $q.defer();
            apiService.download('uploads/' , fileName).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        confServices.generateBill = generateBill;
        confServices.downloadFile = downloadFile;
        return confServices;

    }]);
})();
