
(function () {
    'use strict';

    angular.module('bills', []).config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {

        function authentication(GlobalServices, $q, localStorageService, $state) {
            var d = $q.defer();
            var checkUser = localStorageService.get("user");
            if (checkUser !== null) {
                d.resolve(checkUser);
            } else {
                GlobalServices.currentUser().then(function (data) {
                    if (data.success === 0) {
                        $state.go('login');
                    } else {
                        localStorageService.set('user', data.data.account);
                        d.resolve(data.user);
                    }
                });
            }
            return d.promise;
        }
        function isLogin($q, localStorageService, $state,$location) {
            var d = $q.defer();
            var checkUser = localStorageService.get("user");
            if (checkUser !== null) {
                d.resolve(checkUser);
            } else {
                // $state.go('login');
                $location.url('/login')

            }
            return d.promise;
        }


        $stateProvider
            .state('bills', {
                url: '/bills/generate',
                templateUrl: 'app/pages/bill/generateBill.html',
                controller: 'generateBillController',
                controllerAs: "vm",
                title: 'Generate Bill',
                /*sidebarMeta: {
                    icon: 'fa fa-motorcycle',
                    order: 0,
                },*/
                resolve: {
                    $user: isLogin
                },

            })

        //$urlRouterProvider.when('/riders','/vehicles');
    }

})();
