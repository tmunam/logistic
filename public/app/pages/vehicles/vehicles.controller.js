
(function() {
    angular.module('vehicles').controller("vehiclesController", vehiclesController);

    vehiclesController.$inject = ['$scope', 'toastr', '$state', 'vehiclesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function vehiclesController($scope, toastr, $state, vehiclesServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 100;
        ctg.vehicles = [];
        ctg.vehiclescopy = [];

        getVehicles();

        function getVehicles(){
            var params = {
                offset: ctg.page,
                limit: ctg.limit,
                text: {
                    searchText: ''
                }
            };

            vehiclesServices.getVehicles(params).then(function(response) {
                if (response.data){
                    ctg.vehicles = response.data.vehicles;
                }
            });
        }

        /*function getData(sSource, aoData, fnCallback, oSettings, oSettings) {
            var draw = aoData[0].value;
            var order = aoData[2].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var searchText = aoData[5].value.value;
            var params = {
                offset: start,
                limit: length,
                text: {
                    searchText: searchText
                }
            };
            vehiclesServices.getRiders(params).then(function(response) {
                var records = {
                    'draw': 0,
                    'recordsTotal': 0,
                    'recordsFiltered': 0,
                    'data': []
                };
                if (response.data) {

                    records = {
                        'draw': draw,
                        'recordsTotal': response.data.count,
                        'recordsFiltered': response.data.count,
                        'data': response.data.riders
                    };
                }
                fnCallback(records);
            });
        }

        function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $compile(nRow)($scope);
        }

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withFnServerData(getData) // method name server call
            .withDataProp('data') // parameter name of list use in getLeads Fuction
            // .withOption('processing', true)
            .withOption('serverSide', true) // required
            .withOption('paging', true) // required
            .withOption('width', '5%')
            .withOption('order', [0, 'asc'])
            .withPaginationType('simple_numbers')
            .withDisplayLength(10)
            .withOption('rowCallback', rowCallback)
            .withLanguage({
                emptyTable: "No Matching records Found",
                LoadingRecords: "Henter data...",
                zeroRecords: "A different no matching records message"
            });

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('name', 'Name').withOption('defaultContent', "").withClass('text-center description'),
            DTColumnBuilder.newColumn('phone', 'Phone').withOption('defaultContent', "").withClass('text-center description'),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().withClass('text-center ')
                .renderWith(function(data, type, full, meta) {
                    return '<button class="btn btn-warning editGenBtn" data-id="' + data._id + '"  data-name="' + data.name + '" data-phone="' + data.phone + '" data-image="' + data.image + '" ng-click="editCategory($event)">' +
                        'Edit' +
                        '</button>&nbsp;' + '      ' +
                        '<button ng-class="' + data.status + '" && "btn-warning" ||"' + data.status + '" && btn-danger" class="btn btn-danger delCatBtn" data-id="' + data._id + '" data-status="' + data.status + '" ng-click="archiveCategory($event,$index)" ng-if="' + data.status + '==true">Archive</button>' +
                        '   ' +
                        '<button ng-class="' + data.status + '" && "btn-warning" ||"' + data.status + '" && btn-danger" class="btn btn-success delCatBtn" data-id="' + data._id + '" data-status="' + data.status + '" ng-click="archiveCategory($event,$index)" ng-if="' + data.status + '==false">Un Archive</button>'
                }),
        ];*/

        ctg.addCategory = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/vehicles/addEditVehicle.html",
                size: "md",
                controller: 'AddEditVehicleController',
                controllerAs: 'vm',
                resolve: {
                    $vehicle: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveCategory = function(vehicle, $index) {

            if (vehicle.status) {
                vehicle.status = 0
            } else {
                vehicle.status = 1
            }
            usSpinnerService.spin('spinner-1');
            vehiclesServices.EditVehicle({
                vehicle :vehicle
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Rider updated successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editCategory = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/vehicles/addEditVehicle.html",
                size: "md",
                controller: 'AddEditVehicleController',
                controllerAs: 'vm',
                resolve: {
                    $vehicle: function() {
                        return {
                            isNew: false,
                            vehicle: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('vehicles').controller("AddEditVehicleController", AddEditVehicleController);

    AddEditVehicleController.$inject = ['$scope', 'toastr', '$state', 'vehiclesServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$vehicle', 'Upload'];

    function AddEditVehicleController($scope, toastr, $state, vehiclesServices, driversServices, usSpinnerService, $uibModalInstance, $vehicle, Upload) {

        var ctgAE = this;
        ctgAE.title = "Add New Vehicle";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        ctgAE.vehicleData = {};
        ctgAE.driversList = [];

        if (!$vehicle.isNew) {
            ctgAE.vehicleData._id = $vehicle.vehicle._id;
            ctgAE.vehicleData.number = $vehicle.vehicle.number;
            ctgAE.vehicleData.type = $vehicle.vehicle.type;
            ctgAE.vehicleData.driver = $vehicle.vehicle.driver;
            ctgAE.vehicleData.is_token_paid = $vehicle.vehicle.is_token_paid;
            ctgAE.vehicleData.annual_token_fee = $vehicle.vehicle.annual_token_fee;
            ctgAE.vehicleData.detention_charges = $vehicle.vehicle.detention_charges;
            ctgAE.vehicleData.current_value = $vehicle.vehicle.current_value;
            ctgAE.vehicleData.is_fixed_veh = $vehicle.vehicle.is_fixed_veh;
            ctgAE.vehicleData.status = true;
            ctgAE.title = "Edit Vehicle";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        function getDrivers(){
            var params = {
                offset: 0,
                limit: 100,
                text: {
                    searchText: ''
                }
            };
            driversServices.getRiders(params).then(function (response) {
                if (response.data) {
                    ctgAE.driversList = response.data.riders;
                }
            });

        }
        getDrivers();

        ctgAE.addEditVehcicle = function() {
            if ($vehicle.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                vehiclesServices.addVehicle({
                    vehicle : ctgAE.vehicleData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Category added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                vehiclesServices.EditVehicle({
                    vehicle : ctgAE.vehicleData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Driver Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }
    }

    angular.module('vehicles').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();
