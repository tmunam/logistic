/**
 * Created by Raza on 8/7/2017.
 */
(function () {
    angular.module('vehicles').service('vehiclesServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getVehicles = function (parameters,search) {
            var deferred = $q.defer();
            var text="vehicles/getAllVehicles/" + parameters.offset + "/" + parameters.limit;
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters.text).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addVehicle = function (parameters) {
            var deferred = $q.defer();
            apiService.create("vehicle/addVehicle", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var EditVehicle = function (parameters) {
            var deferred = $q.defer();
            apiService.create("vehicle/editVehicle", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };



        confServices.getVehicles = getVehicles;
        confServices.addVehicle = addVehicle;
        confServices.EditVehicle = EditVehicle;
        return confServices;

    }]);
})();
