/**
 * Created by Raza on 8/7/2017.
 */
(function () {
    angular.module('BlurAdmin.pages.dashboard').service('dashServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getAnalytics = function (parameters) {
            var deferred = $q.defer();
            apiService.get("user/getAnalyticsCount").then(function (response) {
                    if (response){
                        deferred.resolve(response);
                      }
                    else{
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                      }
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        confServices.getAnalytics = getAnalytics;

        return confServices;

    }]);
})();
