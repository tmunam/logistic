/**
 * Created by Affan on 8/16/2018.
 */
(function() {
    angular.module('BlurAdmin.pages.dashboard').controller("dashController", dashController);

    dashController.$inject = ['$scope', 'toastr', '$state', 'dashServices', '$uibModal', 'usSpinnerService', '$compile', 'Upload', '$timeout', '$location'];

    function dashController($scope, toastr, $state, dashServices, $uibModal, usSpinnerService, $compile, Upload, $timeout, $location) {
        var ctg = this;
        ctg.analytics = {};

        ctg.getAnalytics = function() {
            usSpinnerService.spin('spinner-1');
            dashServices.getAnalytics().then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    ctg.analytics = res.data;
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };
        ctg.getAnalytics();
    }
})();
