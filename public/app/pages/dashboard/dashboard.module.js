/**
 * Created by Raza on 8/1/2017.
 */
(function() {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {

    function authentication(GlobalServices, $q, localStorageService, $state) {
      var d = $q.defer();
      var checkUser = localStorageService.get("user");
      if (checkUser !== null) {
        d.resolve(checkUser);
      } else {
        GlobalServices.currentUser().then(function(data) {
          if (data.success === 0) {
            $state.go('login');
          } else {
            localStorageService.set('user', data.data.account);
            d.resolve(data.user);
          }
        });
      }
      return d.promise;
    }

    function isLogin($q, localStorageService, $state, $location) {
      var d = $q.defer();
      var checkUser = localStorageService.get("user");
      if (checkUser !== null) {
        d.resolve(checkUser);
      } else {
        // $state.go('login');
        $location.url('/login')
      }
      return d.promise;
    }
    $stateProvider
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'app/pages/dashboard/dashboard.html',
        title: 'Dashboard',
        controller: 'dashController',
          controllerAs: 'dc',
        /*sidebarMeta: {
          icon: 'ion-android-home',
          order: 0,
        },*/
        resolve: {
          $user: isLogin
        },
      });
  }
})();
