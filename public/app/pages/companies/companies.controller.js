
(function() {
    angular.module('companies').controller("companiesController", companiesController);

    companiesController.$inject = ['$scope', 'toastr', '$state', 'companiesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function companiesController($scope, toastr, $state, companiesServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 100;
        ctg.companies = [];

        getCompanies();

        function getCompanies(){
            var params = {
                offset: ctg.page,
                limit: ctg.limit,
                text: {
                    searchText: ''
                }
            };

            companiesServices.getCompanies(params).then(function(response) {
                if (response.data){
                    ctg.companies = response.data.companies;
                }
            });
        }

        ctg.addCompany = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/companies/addEditCompany.html",
                size: "md",
                controller: 'AddEditCompanyController',
                controllerAs: 'vm',
                resolve: {
                    $company: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveCompany = function(company, $index) {

            if (company.status) {
                company.status = 0
            } else {
                company.status = 1
            }
            usSpinnerService.spin('spinner-1');
            companiesServices.EditCompany({
                company : company
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Company deleted successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editCompany = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/companies/addEditCompany.html",
                size: "md",
                controller: 'AddEditCompanyController',
                controllerAs: 'vm',
                resolve: {
                    $company: function() {
                        return {
                            isNew: false,
                            company: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('companies').controller("AddEditCompanyController", AddEditCompanyController);

    AddEditCompanyController.$inject = ['$scope', 'toastr', '$state', 'companiesServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$company', 'Upload'];

    function AddEditCompanyController($scope, toastr, $state, companiesServices, driversServices, usSpinnerService, $uibModalInstance, $company   , Upload) {

        var ctgAE = this;
        ctgAE.title = "Add New Company";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        ctgAE.companyData = {};

        if (!$company.isNew) {
            ctgAE.companyData._id = $company.company._id;
            ctgAE.companyData.company_name = $company.company.company_name;
            ctgAE.companyData.city = $company.company.city;
            ctgAE.companyData.rate = $company.company.rate;
            ctgAE.companyData.status = true;
            ctgAE.title = "Edit Company";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        ctgAE.addEditCompany = function() {
            if ($company.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                companiesServices.addCompany({
                    company : ctgAE.companyData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Company added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                companiesServices.EditCompany({
                    company : ctgAE.companyData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Company Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }
    }

    angular.module('destinations').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();
