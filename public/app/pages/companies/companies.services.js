
(function () {
    angular.module('companies').service('companiesServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getCompanies = function (parameters,search) {
            var deferred = $q.defer();
            var text="companies/getAllCompanies/" + parameters.offset + "/" + parameters.limit;
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters.text).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addCompany = function (parameters) {
            var deferred = $q.defer();
            apiService.create("companies/addCompany", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var EditCompany = function (parameters) {
            var deferred = $q.defer();
            apiService.create("companies/editCompany", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };



        confServices.getCompanies = getCompanies;
        confServices.addCompany = addCompany;
        confServices.EditCompany = EditCompany;
        return confServices;

    }]);
})();
