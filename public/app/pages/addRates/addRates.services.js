
(function () {
    angular.module('addRates').service('addRatesServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getRates = function (parameters,search) {
            var deferred = $q.defer();
            var text="rates/getRates";
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addRates = function (parameters) {
            var deferred = $q.defer();
            apiService.create("rates/addRates", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var editRates = function (parameters) {
            var deferred = $q.defer();
            apiService.create("rates/editRates", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var uploadFile = function (parameters) {
            var deferred = $q.defer();
            apiService.upload("file/rates/updatRates", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        confServices.getRates = getRates;
        confServices.addRates = addRates;
        confServices.editRates = editRates;
        confServices.uploadFile = uploadFile;
        return confServices;

    }]);
})();
