
(function() {
    angular.module('addRates').controller("addRatesController", addRatesController);

    addRatesController.$inject = ['$scope', 'toastr', '$state', 'invoicesServices', 'addRatesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function addRatesController($scope, toastr, $state, invoicesServices, addRatesServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 100;
        ctg.rates = [];
        ctg.searchTerm = '';
        ctg.searchRates = searchRates;

        function searchRates(){
            ctg.rates = [];
            getRates();
        }

        getRates();

        function getRates(){
            var params = {
                offset: ctg.page,
                limit: ctg.limit,
                searchTerm : ctg.searchTerm
            };

            addRatesServices.getRates(params).then(function(response) {
                if (response.data){
                    ctg.rates = response.data.rates;
                }
            });
        }

        ctg.addRates = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/addRates/addEditRates.html",
                size: "md",
                controller: 'AddEditRatesController',
                controllerAs: 'vm',
                resolve: {
                    $rates: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.updateRates = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/addRates/updateRates.html",
                size: "md",
                controller: 'updateRatesController',
                controllerAs: 'vm',
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
                $state.reload();
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveDestination = function(destination, $index) {

            if (destination.status) {
                destination.status = 0
            } else {
                destination.status = 1
            }
            usSpinnerService.spin('spinner-1');
            addRatesServices.EditDestination({
                destination :destination
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Destination deleted successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editRates = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/addRates/addEditRates.html",
                size: "md",
                controller: 'AddEditRatesController',
                controllerAs: 'vm',
                resolve: {
                    $rates: function() {
                        return {
                            isNew: false,
                            rate: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('addRates').controller("AddEditRatesController", AddEditRatesController);

    AddEditRatesController.$inject = ['$scope', 'toastr', '$state', 'invoicesServices', 'addRatesServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$rates', 'Upload'];

    function AddEditRatesController($scope, toastr, $state, invoicesServices, addRatesServices, driversServices, usSpinnerService, $uibModalInstance, $rates   , Upload) {

        var ctgAE = this;
        ctgAE.title = "Add New Rate";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        ctgAE.ratesData = {};
        ctgAE.driversList = [];

        invoicesServices.getOtherDetials().then(function (res){
            if (res.response === 200){
                ctgAE.driversList = res.data.drivers;
                ctgAE.vehiclesList = res.data.vehicles;
                ctgAE.companiesList = res.data.companies;
                ctgAE.destinationsList = res.data.destinations;
            }
        });

        if (!$rates.isNew) {
            ctgAE.ratesData._id = $rates.rate._id;
            ctgAE.ratesData.company = { _id : $rates.rate.company_id , company_name : $rates.rate.company_name };
            ctgAE.ratesData.destination = { _id : $rates.rate.destination_id , destination_name : $rates.rate.destination_name };
            ctgAE.ratesData.rate = $rates.rate.rate;
            ctgAE.ratesData.old_rate = $rates.rate.old_rate;
            ctgAE.title = "Edit Rate";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        ctgAE.addEditRates = function() {
            if ($rates.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                addRatesServices.addRates({
                    rates : ctgAE.ratesData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Rates added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                addRatesServices.editRates({
                    rates : ctgAE.ratesData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Rates Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }
    }

    angular.module('addRates').controller("updateRatesController", updateRatesController);

    updateRatesController.$inject = ['$scope', 'toastr', 'invoicesServices', 'addRatesServices', 'usSpinnerService', '$uibModalInstance', 'Upload'];

    function updateRatesController($scope, toastr, invoicesServices, addRatesServices, usSpinnerService, $uibModalInstance, Upload) {
        var vm = this;
        vm.uploadFile = uploadFile;
        vm.imgPath = '';
        vm.companiesList = [];
        vm.effectFrom = new Date();
        vm.inProgress = false;
        vm.company = '';
        invoicesServices.getOtherDetials().then(function (res){
            if (res.response === 200){
                vm.companiesList = res.data.companies;
            }
        });
        function uploadFile(){
            if (vm.imgPath && vm.effectFrom){
                vm.inProgress = true;
                usSpinnerService.spin('spinner-1');
                var fd = new FormData();
                fd.append('photo', vm.imgPath);
                fd.append('company', vm.company._id);
                fd.append('company_name', vm.company.company_name);
                fd.append('oldFuelRate', vm.oldFuelRate);
                fd.append('newFuelRate', vm.newFuelRate);
                fd.append('effectFrom', vm.effectFrom);
                addRatesServices.uploadFile(fd).then(function (res){
                    usSpinnerService.stop('spinner-1');
                    vm.inProgress = false;
                    $uibModalInstance.close();
                });
            }
        }

        // Date
        vm.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            //  maxDate: new Date(2020, 5, 22),
            minDate: new Date(new Date().setDate(new Date().getDate()-300)),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            //  return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        vm.toggleMin = function () {
            vm.dateOptions.minDate = vm.dateOptions.minDate;
        };

        vm.popup2 = {
            opened: false
        };

        vm.toggleMin();

        vm.open2 = function () {
            vm.popup2.opened = true;
        };

        vm.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
        vm.format = vm.formats[0];

    }

        angular.module('addRates').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });

    angular.module('addRates').directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);
})();
