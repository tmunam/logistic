
(function() {
    angular.module('destinations').controller("destinationsController", destinationsController);

    destinationsController.$inject = ['$scope', 'toastr', '$state', 'destinationsServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function destinationsController($scope, toastr, $state, destinationsServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 100;
        ctg.searchTerm = '';
        ctg.destinations = [];
        ctg.searchDestinatons = searchDestinatons;

        function searchDestinatons(){
            ctg.destinations = [];
            getDestinations();
        }
        getDestinations();

        function getDestinations(){
            var params = {
                offset: ctg.page,
                limit: ctg.limit,
                searchTerm : ctg.searchTerm
            };

            destinationsServices.getDestinations(params).then(function(response) {
                if (response.data){
                    ctg.destinations = response.data.destinations;
                }
            });
        }

        ctg.addDestination = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/destinations/addEditDestination.html",
                size: "md",
                controller: 'AddEditDestinationController',
                controllerAs: 'vm',
                resolve: {
                    $destination: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveDestination = function(destination, $index) {

            if (destination.status) {
                destination.status = 0
            } else {
                destination.status = 1
            }
            usSpinnerService.spin('spinner-1');
            destinationsServices.EditDestination({
                destination :destination
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Destination deleted successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editDestination = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/destinations/addEditDestination.html",
                size: "md",
                controller: 'AddEditDestinationController',
                controllerAs: 'vm',
                resolve: {
                    $destination: function() {
                        return {
                            isNew: false,
                            destination: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('destinations').controller("AddEditDestinationController", AddEditDestinationController);

    AddEditDestinationController.$inject = ['$scope', 'toastr', '$state', 'destinationsServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$destination', 'Upload'];

    function AddEditDestinationController($scope, toastr, $state, destinationsServices, driversServices, usSpinnerService, $uibModalInstance, $destination   , Upload) {

        var ctgAE = this;
        ctgAE.title = "Add New Destination";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        ctgAE.destinationData = {};
        ctgAE.driversList = [];

        if (!$destination.isNew) {
            ctgAE.destinationData._id = $destination.destination._id;
            ctgAE.destinationData.destination_id = $destination.destination.destination_id;
            ctgAE.destinationData.destination_name = $destination.destination.destination_name;
            ctgAE.destinationData.distance = $destination.destination.distance;
            ctgAE.destinationData.status = true;
            ctgAE.title = "Edit Destination";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        ctgAE.addEditDestination = function() {
            if ($destination.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                destinationsServices.addDestination({
                    destination : ctgAE.destinationData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Destination added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                destinationsServices.EditDestination({
                    destination : ctgAE.destinationData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Destination Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }
    }

    angular.module('destinations').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();
