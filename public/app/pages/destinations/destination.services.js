
(function () {
    angular.module('destinations').service('destinationsServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getDestinations = function (parameters,search) {
            var deferred = $q.defer();
            var text="destinations/getAllDestinations";
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addDestination = function (parameters) {
            var deferred = $q.defer();
            apiService.create("destinations/addDestination", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var EditDestination = function (parameters) {
            var deferred = $q.defer();
            apiService.create("destinations/editDestination", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var getDistributors = function (parameters){
            var deferred = $q.defer();
            var text="distributor/getAllDistributors";
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        }

        var addDistributor = function (parameters) {
            var deferred = $q.defer();
            apiService.create("distributor/addDistributor", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var editDistributor = function (parameters) {
            var deferred = $q.defer();
            apiService.create("distributor/editDistributor", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        confServices.getDestinations = getDestinations;
        confServices.getDistributors = getDistributors;
        confServices.addDestination = addDestination;
        confServices.addDistributor = addDistributor;
        confServices.EditDestination = EditDestination;
        confServices.editDistributor = editDistributor;
        return confServices;

    }]);
})();
