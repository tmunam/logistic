
(function() {
    angular.module('invoices').controller("invoicesController", invoicesController);

    invoicesController.$inject = ['$scope', 'toastr', '$state', 'invoicesServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function invoicesController($scope, toastr, $state, invoicesServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 30;
        ctg.searchTerm = '';
        ctg.searchTermBilti = '';
        ctg.searchTermDistributor = '';
        ctg.searchTermDCN = '';
        ctg.invoices = [];
        ctg.busy = false;
        ctg.dataCompleted = false;
        ctg.searchInvoices = searchInvoices;
        ctg.testList = ['ertert','ertert','ertert'];

        function searchInvoices(type){
            ctg.invoices = [];
            ctg.dataCompleted = false;
            ctg.page = 0;
            ctg.limit = 30;
            getInvoices(type);
        }

        getInvoices();

        ctg.loadMoreData = function () {
            getInvoices();
        }

        function getInvoices(type){
            if (ctg.dataCompleted == false){
                var searchType = '';
                if (ctg.searchTermBilti){
                    searchType = 'bilti';
                    ctg.searchTerm = ctg.searchTermBilti;
                }
                else if (ctg.searchTermDistributor){
                    searchType = 'distributor';
                    ctg.searchTerm = ctg.searchTermDistributor;
                }
                else if (ctg.searchTermDCN){
                    searchType = 'dcn';
                    ctg.searchTerm = ctg.searchTermDCN;
                }
                ctg.busy = true;
                var params = {
                    offset: ctg.limit * ctg.page,
                    limit: ctg.limit,
                    searchTerm: ctg.searchTerm,
                    searchType : searchType
                };
                ctg.page++;
                invoicesServices.getInvoices(params).then(function(response) {
                    if (response.data && response.data.invoices.length > 0){
                        if (response.data.invoices.length < ctg.limit)
                            ctg.dataCompleted = true;
                        angular.forEach(response.data.invoices, function (inv){
                            inv.dcnStr = inv.dcn_list.map(function(e){
                                return (e.dcn_no + ' (' + e.quantity + ')');
                            }).join('<br>');
                            ctg.invoices.push(inv);
                        });
                        //ctg.invoices = response.data.invoices;
                        ctg.busy = false;
                    }
                });
            }
        }

        ctg.addInvoice = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/invoices/addEditInvoice.html",
                size: "lg",
                controller: 'AddEditInvoiceController',
                controllerAs: 'vm',
                windowClass : 'invoices-modal',
                resolve: {
                    $invoice: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveInvoice = function(invoice, $index) {

            if (invoice.status) {
                invoice.status = 0
            } else {
                invoice.status = 1
            }
            usSpinnerService.spin('spinner-1');
            invoicesServices.deleteInvoice({
                invoice : invoice
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Invoice deleted successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editInvoice = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/invoices/addEditInvoice.html",
                size: "md",
                controller: 'AddEditInvoiceController',
                controllerAs: 'vm',
                windowClass : 'invoices-modal',
                resolve: {
                    $invoice: function() {
                        return {
                            isNew: false,
                            invoice: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('invoices').controller("AddEditInvoiceController", AddEditInvoiceController);

    AddEditInvoiceController.$inject = ['$scope', 'toastr', '$state', 'invoicesServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$invoice', 'Upload'];

    function AddEditInvoiceController($scope, toastr, $state, invoicesServices, driversServices, usSpinnerService, $uibModalInstance, $invoice   , Upload) {

        var ctgAE = this;
        ctgAE.title = "Add New Invoice";
        ctgAE.driversList = [];
        ctgAE.vehiclesList = [];
        ctgAE.vehiclesListCopy = [];
        ctgAE.companiesList = [];
        ctgAE.destinationsList = [];
        ctgAE.distributionsList = [];
        ctgAE.distributionsListCopy = [];
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;

        invoicesServices.getOtherDetials().then(function (res){
            if (res.response === 200){
                ctgAE.driversList = res.data.drivers;
                ctgAE.vehiclesList = res.data.vehicles;
                ctgAE.vehiclesListCopy = res.data.vehicles;
                ctgAE.companiesList = res.data.companies;
                ctgAE.destinationsList = res.data.destinations;
                ctgAE.distributionsList = res.data.distributions;
                ctgAE.distributionsListCopy = res.data.distributions;

                ctgAE.companiesList.push({ _id : 'NIL', company_name : 'Not in list' });
                if (ctgAE.invoiceData.is_shahzore){
                    var selectedVehicle = ctgAE.vehiclesList.filter(function (vh){
                        return vh.number == 'Shahzore For RWP/ISD';
                    });
                    ctgAE.invoiceData.vehicle = selectedVehicle[0];
                }
            }
        });

        ctgAE.getDestinationsList = function() {
           return  ctgAE.destinationsList;
        };
        ctgAE.getDistributionsList = function() {
           return  ctgAE.distributionsListCopy;
        };

        ctgAE.invoiceData = {};
        ctgAE.inProgress = false;
        ctgAE.addMoreDcn = addMoreDcn;
        ctgAE.addMoreProduct = addMoreProduct;
        ctgAE.addMoreTwoWayOrder = addMoreTwoWayOrder;
        ctgAE.removeDcn = removeDcn;
        ctgAE.removeProduct = removeProduct;
        ctgAE.removeTwoWayOrders = removeTwoWayOrders;
        ctgAE.addEditInvoice = addEditInvoice;
        ctgAE.customCompanySelected = customCompanySelected;
        ctgAE.destinationChange = destinationChange;
        ctgAE.fixedVehicle = fixedVehicle;
        ctgAE.currentDateTimeObj = new Date();
        ctgAE.invoiceData.date = ctgAE.currentDateTimeObj.toDateString();
        ctgAE.invoiceData.dcn_list = [{
            dcn_no : '' ,
            quantity : ''
        }];
        ctgAE.invoiceData.two_way_detail = [];
        /*ctgAE.invoiceData.fuel_charges = 0;
        ctgAE.invoiceData.toll_charges = 0;
        ctgAE.invoiceData.transit_loss = 0;
        ctgAE.invoiceData.offloading_charges = 0;
        ctgAE.invoiceData.police_challan = 0;
        ctgAE.invoiceData.detention_charges = 0;*/
        ctgAE.invoiceData.company = { _id : '5b145692b1724231c74ff1dd', company_name : 'Colgate'};
        if (!$invoice.isNew) {
            /*ctgAE.invoiceData._id = $invoice.company._id;
            ctgAE.invoiceData.company_name = $invoice.company.company_name;
            ctgAE.invoiceData.city = $invoice.company.city;
            ctgAE.invoiceData.rate = $invoice.company.rate;*/
            ctgAE.invoiceData = $invoice.invoice;
            ctgAE.invoiceData.bilti_date = new Date(ctgAE.invoiceData.bilti_date);
            ctgAE.invoiceData.delivery_date = new Date(ctgAE.invoiceData.delivery_date);
            ctgAE.title = "Edit Invoice";
        }
        else{
            ctgAE.invoiceData.bilti_date = new Date();
            ctgAE.invoiceData.delivery_date = new Date();
        }

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        function fixedVehicle() {
            if (ctgAE.invoiceData.is_fixed_veh){
                ctgAE.vehiclesListCopy = ctgAE.vehiclesList.filter(function (inv) {
                    return inv.is_fixed_veh == true;
                });
            }
            else{
                ctgAE.vehiclesListCopy = ctgAE.vehiclesList;
            }
        }
        
        function addMoreProduct(item) {
            item.product_list.push({ dcn_no : '' , quantity : '' });
        }

        function removeProduct(item, index){
            item.product_list.splice(index, 1);
        }

        function addMoreDcn() {
            ctgAE.invoiceData.dcn_list.push({
                dcn_no : '' ,
                quantity : ''
            });
        }

        function removeDcn(index){
            ctgAE.invoiceData.dcn_list.splice(index, 1);
        }

        function addMoreTwoWayOrder() {
            ctgAE.invoiceData.two_way_detail.push({
                company_name: '',
                description: '',
                no_of_carton: '',
                amount: '',
                destination : ''
            });
        }

        function removeTwoWayOrders(index){
            ctgAE.invoiceData.two_way_detail.splice(index, 1);
        }

        ctgAE.vehicleChange = function (){
            var selectedVehicle = ctgAE.invoiceData.vehicle;
            if (selectedVehicle.number == 'Shahzore For RWP/ISD'){
                ctgAE.invoiceData.is_shahzore = true;
            }
            else
                ctgAE.invoiceData.is_shahzore = false;
            var vehicle = ctgAE.vehiclesList.filter(function (d){
                return d.number == selectedVehicle.number;
            });

            ctgAE.invoiceData.driver = vehicle[0].driver;
        };

        ctgAE.otherOrderChange = function (){
            if (ctgAE.invoiceData.is_other_orders == false)
                ctgAE.invoiceData.dcn_list = [];
            else {
                ctgAE.invoiceData.dcn_list.push({
                    dcn_no : '' ,
                    quantity : ''
                });
            }
        };

        ctgAE.twoWayOrderChange = function (){
            if (ctgAE.invoiceData.is_two_way == false)
                ctgAE.invoiceData.two_way_detail = [];
            else {
                ctgAE.invoiceData.two_way_detail.push({
                    company_name: '',
                    description: '',
                    no_of_carton: '',
                    amount: '',
                    destination : ''
                });
            }
        };

        function customCompanySelected(item, value) {
            if (item.company.company_name === 'Not in list') {
                item.is_other = true;
            }
        }

        function addEditInvoice() {
            if (ctgAE.invoiceData.is_fixed_veh){
                console.log(ctgAE.invoiceData.fixed_vehicle_no);
                ctgAE.invoiceData.vehicle = null;
            }
            if ($invoice.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                invoicesServices.addInvoice({
                    invoice : ctgAE.invoiceData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Invoice added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            }
            else {
                usSpinnerService.spin('spinner-1');
                invoicesServices.EditInvoice({
                    invoice : ctgAE.invoiceData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Invoice Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }

        // Date
        ctgAE.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            //  maxDate: new Date(2020, 5, 22),
            minDate: new Date(new Date().setDate(new Date().getDate()-300)),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            //  return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        ctgAE.toggleMin = function () {
            ctgAE.dateOptions.minDate = ctgAE.dateOptions.minDate;
        };

        ctgAE.popup2 = {
            opened: false
        };

        ctgAE.popup3 = {
            opened: false
        };

        ctgAE.toggleMin();

        ctgAE.open2 = function () {
            ctgAE.popup2.opened = true;
        };

        ctgAE.open3 = function () {
            ctgAE.popup3.opened = true;
        };



        ctgAE.formats = ['dd-MMMM-yyyy', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
        ctgAE.format = ctgAE.formats[1];

        function destinationChange() {
            var destId = ctgAE.invoiceData.destination._id;

            var distSuggestion = ctgAE.distributionsList.filter(function(dist){
               return dist.destination_id == destId;
            });

            ctgAE.distributionsListCopy = distSuggestion;
            //ctgAE.invoiceData.distribution = ctgAE.distributionsListCopy[0];
        }
    }

    angular.module('destinations').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();
