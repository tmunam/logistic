
(function () {
    angular.module('invoices').service('invoicesServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getInvoices = function (parameters,search) {
            var deferred = $q.defer();
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create("invoices/getAllInvoices",parameters).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addInvoice = function (parameters) {
            var deferred = $q.defer();
            apiService.create("invoices/addInvoice", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var EditInvoice = function (parameters) {
            var deferred = $q.defer();
            apiService.create("invoices/editInvoice", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var getOtherDetials = function (parameters) {
            var deferred = $q.defer();
            apiService.get("invoices/getOtherDetials").then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var deleteInvoice = function (parameters) {
            var deferred = $q.defer();
            apiService.create("invoices/deleteInvoice", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        confServices.getInvoices = getInvoices;
        confServices.addInvoice = addInvoice;
        confServices.EditInvoice = EditInvoice;
        confServices.deleteInvoice = deleteInvoice;
        confServices.getOtherDetials = getOtherDetials;
        return confServices;

    }]);
})();
