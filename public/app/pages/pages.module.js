/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';
    angular.module('BlurAdmin.pages', [
        'ui.router', 'LocalStorageModule', 'BlurAdmin.pages.dashboard',

        "login","users", "kpo", "invoices", "bills", "expenses", "addRates", "drivers", "vehicles", "destinations", "distributor", "companies", "angularSpinner", "angular-google-maps-geocoder"

    ]).config(routeConfig);

    /** @ngInject */
    function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
        $urlRouterProvider.otherwise('/login');

        baSidebarServiceProvider.addStaticItem(
            {
                title: 'Admin Dashboard',
                icon: 'ion-android-home',
                order: 0,
                access : ['ADMIN'],
                stateRef: 'dashboard'
            },
            {
                title: 'Freight Invoices',
                icon: 'ion-document',
                order: 1,
                access : ['KPO', 'ADMIN', 'MANAGER'],
                stateRef: 'invoices'
            },
            {
                title: 'Drivers',
                icon: 'ion-person-stalker',
                order: 2,
                access : ['KPO', 'ADMIN', 'MANAGER'],
                stateRef: 'drivers'
            },
            {
                title: 'KPO',
                icon: 'ion-android-person',
                order: 3,
                access : ['ADMIN'],
                stateRef: 'kpo'
            },
            {
                title: 'Vehicles',
                icon: 'ion-android-car',
                order: 4,
                access : ['KPO', 'ADMIN', 'MANAGER'],
                stateRef: 'vehicles'
            },
            {
                title: 'Destination',
                icon: 'ion-android-map',
                order: 5,
                access : ['ADMIN'],
                stateRef: 'destinations'
            },
            {
                title: 'Distributors',
                icon: 'ion-android-map',
                order: 5,
                access : ['KPO', 'ADMIN', 'MANAGER'],
                stateRef: 'distributor'
            },
            {
                title: 'Companies',
                icon: 'fa fa-building-o',
                order: 6,
                access : ['ADMIN'],
                stateRef: 'companies'
            },
            {
                title: 'Expenses',
                icon: 'fa fa-money',
                order: 7,
                //stateRef: 'expensesState',
                access : ['KPO', 'ADMIN', 'MANAGER'],
                subMenu: [{
                    title: 'Daily Expenses',
                    stateRef: 'dailyExpenses',
                    access : ['KPO', 'ADMIN', 'MANAGER']
                }, {
                    title: 'Repair & Maintenance',
                    stateRef: 'repairMaintenance',
                    access : ['KPO', 'ADMIN', 'MANAGER']
                },{
                    title: 'Annual Token & Route Permit & Tranfer Fee',
                    stateRef: 'tptFee',
                    access : ['KPO', 'ADMIN', 'MANAGER']
                }   ]
            },
            {
                title: 'Add Rates',
                icon: 'fa fa-money',
                order: 8,
                access : ['ADMIN', 'MANAGER'],
                stateRef: 'addRatesCT'
            },
            {
                title: 'Bill',
                icon: 'fa fa-money',
                order: 8,
                access : ['ADMIN', 'MANAGER'],
                stateRef: 'bills'
            },
            {
                title: 'Logout',
                icon: 'fa fa-power-off',
                order: 20,
                access : ['KPO', 'ADMIN', 'MANAGER'],
                stateRef: 'logout'
            }/*,{
                title: 'Users',
                icon: 'ion-person-stalker',
                order: 21,
                access : ['KPO', 'ADMIN', 'MANAGER'],
                stateRef: 'userslisting'
            }*/
        );
    }

    angular.module('BlurAdmin.pages').config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode(true);
    }]);

    angular.module('BlurAdmin.pages').config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('LabourChoice')
            .setStorageType('sessionStorage');
    });

    angular.module('BlurAdmin.pages').config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
        usSpinnerConfigProvider.setDefaults({color: 'red', radius: 15, width:6, length: 12});
    }]);

    angular.module('BlurAdmin.pages').constant('appSettings', appConfig);

    angular.module('BlurAdmin.pages').config(function ($httpProvider) {
        $httpProvider.defaults.withCredentials = true;
    });

    angular.module('BlurAdmin.pages').run([
    '$rootScope', '$uibModalStack',
    function ($rootScope, $uibModalStack) {
        $rootScope.$on('$locationChangeStart', function (event) {
            var top = $uibModalStack.getTop();
            if (top) {
                $uibModalStack.dismiss(top.key);
                event.preventDefault();
            }
        });
    }
])


})();
