
(function() {
    angular.module('kpo').controller("kpoController", kpoController);

    kpoController.$inject = ['$scope', 'toastr', '$state', 'usersServices', 'share', 'usSpinnerService', '$uibModal', '$compile', '$state', '$timeout', '$location'];

    function kpoController($scope, toastr, $state, usersServices, share, usSpinnerService, $uibModal, $compile, $state, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 100;
        ctg.kpo = [];

        getKpo();

        function getKpo(){
            var params = {
                offset: ctg.page,
                limit: ctg.limit,
                text: {
                    searchText: ''
                }
            };

            usersServices.getKpo(params).then(function(response) {
                if (response.data){
                    ctg.kpo = response.data.kpo;
                }
            });
        }

        ctg.addKpo = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/kpo/addEditKpo.html",
                size: "md",
                controller: 'AddEditKpoController',
                controllerAs: 'vm',
                resolve: {
                    $kpo: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveKpo = function(kpo, $index) {

            if (kpo.status) {
                kpo.status = 0
            } else {
                kpo.status = 1
            }
            usSpinnerService.spin('spinner-1');
            usersServices.editKpo({
                kpo :kpo
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Kpo updated successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editKpo = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/kpo/addEditKpo.html",
                size: "md",
                controller: 'AddEditKpoController',
                controllerAs: 'vm',
                resolve: {
                    $kpo: function() {
                        return {
                            isNew: false,
                            kpo: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('kpo').controller("AddEditKpoController", AddEditKpoController);

    AddEditKpoController.$inject = ['$scope', 'toastr', '$state', 'usersServices', 'usSpinnerService', '$uibModalInstance', '$kpo', 'Upload'];

    function AddEditKpoController($scope, toastr, $state, usersServices, usSpinnerService, $uibModalInstance, $kpo, Upload) {

        var ctgAE = this;
        ctgAE.isNew = $kpo.isNew;
        ctgAE.title = "Add New KPO";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        ctgAE.kpoData = {};
        ctgAE.driversList = [];
        ctgAE.userTypeList = ['ADMIN', 'KPO', 'MANAGER'];

        if (!$kpo.isNew) {
            ctgAE.kpoData._id = $kpo.kpo._id;
            ctgAE.kpoData.name = $kpo.kpo.name;
            ctgAE.kpoData.email = $kpo.kpo.email;
            ctgAE.kpoData.mobileNo = $kpo.kpo.mobileNo;
            ctgAE.kpoData.status = $kpo.kpo.status;
            ctgAE.kpoData.userType = $kpo.kpo.userType;
            ctgAE.title = "Edit Kpo";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        ctgAE.addEditKpo = function() {
            if ($kpo.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                usersServices.addKpo({
                    kpo : ctgAE.kpoData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("KPO added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                usersServices.editKpo({
                    kpo : ctgAE.kpoData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("KPO Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }
    }

    angular.module('kpo').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();
