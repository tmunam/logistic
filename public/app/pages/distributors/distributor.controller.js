
(function() {
    angular.module('distributor').controller("distributorController", distributorController);

    distributorController.$inject = ['$scope', 'toastr', '$state', 'destinationsServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function distributorController($scope, toastr, $state, destinationsServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        ctg.page = 0;
        ctg.limit = 1000;
        ctg.distributors = [];
        ctg.searchTerm = '';
        ctg.searchDistributors = searchDistributors;

        function searchDistributors(){
            ctg.distributors = [];
            getDistributors();
        }

        getDistributors();

        function getDistributors(){
            var params = {
                offset: ctg.page,
                limit: ctg.limit,
                searchTerm : ctg.searchTerm
            };

            destinationsServices.getDistributors(params).then(function(response) {
                if (response.data){
                    ctg.distributors = response.data.distributors;
                }
            });
        }

        ctg.addDistribution = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/distributors/addEditDistributor.html",
                size: "md",
                controller: 'AddEditDistributorController',
                controllerAs: 'vm',
                resolve: {
                    $distributor: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        ctg.archiveDistribution = function(destination, $index) {

            if (destination.status) {
                destination.status = 0
            } else {
                destination.status = 1
            }
            usSpinnerService.spin('spinner-1');
            destinationsServices.editDistributor({
                distributor :destination
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Destination deleted successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        ctg.editDistribution = function(item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/distributors/addEditDistributor.html",
                size: "md",
                controller: 'AddEditDistributorController',
                controllerAs: 'vm',
                resolve: {
                    $distributor: function() {
                        return {
                            isNew: false,
                            distributor: item
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };
    }

    angular.module('distributor').controller("AddEditDistributorController", AddEditDistributorController);

    AddEditDistributorController.$inject = ['$scope', 'toastr', '$state', 'destinationsServices', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$distributor', 'Upload'];

    function AddEditDistributorController($scope, toastr, $state, destinationsServices, driversServices, usSpinnerService, $uibModalInstance, $distributor, Upload) {

        var ctgAE = this;
        ctgAE.title = "Add New Distributor";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        ctgAE.distributorData = {};
        ctgAE.destinationsList = [];

        destinationsServices.getDestinations({offset: 0, limit: 1000}).then(function (res){
            ctgAE.destinationsList = res.data.destinations;
        });

        if (!$distributor.isNew) {
            ctgAE.distributorData = $distributor.distributor;
            ctgAE.distributorData.destination = { _id : $distributor.distributor.destination_id, destination_name : $distributor.distributor.destination_name };
            ctgAE.title = "Edit Distributor";
        }

        ctgAE.inProgress = false;

        ctgAE.close = function (){
            $uibModalInstance.close();
        };

        ctgAE.addEditDistributor = function() {
            if ($distributor.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                destinationsServices.addDistributor({
                    distributor : ctgAE.distributorData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Destination added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                destinationsServices.editDistributor({
                    distributor : ctgAE.distributorData
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Destination Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {};
                ctgAE.inProgress = true;
            }
        }
    }

    angular.module('distributor').directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();
