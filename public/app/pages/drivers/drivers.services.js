/**
 * Created by Raza on 8/7/2017.
 */
(function () {
    angular.module('drivers').service('driversServices', ['$q', 'apiService', function ($q, apiService) {

        var confServices = {};

        var getRiders = function (parameters,search) {
            var deferred = $q.defer();
            var text="rider/getAllRiders/" + parameters.offset + "/" + parameters.limit;
            // apiService.get("categories/getAllCategories/" + parameters.offset + "/" + parameters.limit).then(function (response) {
            apiService.create(text,parameters.text).then(function (response) {

                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addRider = function (parameters) {
            var deferred = $q.defer();
            apiService.create("rider/addRider", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var EditRider = function (parameters) {
            var deferred = $q.defer();
            apiService.create("rider/editRider", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };



        confServices.getRiders = getRiders;
        confServices.addRider = addRider;
        confServices.EditRider = EditRider;
        return confServices;

    }]);
})();
