/**
 * Created by Affan on 8/16/2018.
 */
(function() {
    angular.module('drivers').controller("driversController", ridersController);

    ridersController.$inject = ['$scope', 'toastr', '$state', 'driversServices', 'share', 'usSpinnerService', '$uibModal', '$compile', 'DTColumnBuilder', 'DTOptionsBuilder', '$state', 'Upload', '$timeout', '$location'];

    function ridersController($scope, toastr, $state, driversServices, share, usSpinnerService, $uibModal, $compile, DTColumnBuilder, DTOptionsBuilder, $state, Upload, $timeout, $location) {
        var ctg = this;
        $scope.page = 0;
        $scope.limit = 10;
        $scope.categories = [];

        function getData(sSource, aoData, fnCallback, oSettings, oSettings) {
            var draw = aoData[0].value;
            var order = aoData[2].value;
            var start = aoData[3].value;
            var length = aoData[4].value;
            var searchText = aoData[5].value.value;
            var params = {
                offset: start,
                limit: length,
                text: {
                    searchText: searchText
                }
            };
            driversServices.getRiders(params).then(function(response) {
                var records = {
                    'draw': 0,
                    'recordsTotal': 0,
                    'recordsFiltered': 0,
                    'data': []
                };
                if (response.data) {

                    records = {
                        'draw': draw,
                        'recordsTotal': response.data.count,
                        'recordsFiltered': response.data.count,
                        'data': response.data.riders
                    };
                }
                fnCallback(records);
            });
        }

        function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $compile(nRow)($scope);
        }

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withFnServerData(getData) // method name server call
            .withDataProp('data') // parameter name of list use in getLeads Fuction
            // .withOption('processing', true)
            .withOption('serverSide', true) // required
            .withOption('paging', true) // required
            .withOption('width', '5%')
            .withOption('order', [0, 'asc'])
            .withPaginationType('simple_numbers')
            .withDisplayLength(10)
            .withOption('rowCallback', rowCallback)
            .withLanguage({
                emptyTable: "No Matching records Found",
                LoadingRecords: "Henter data...",
                zeroRecords: "A different no matching records message"
            });

        $scope.dtColumns = [
            DTColumnBuilder.newColumn('name', 'Name').withOption('defaultContent', "").withClass('text-center description'),
            DTColumnBuilder.newColumn('cnic', 'CNIC').withOption('defaultContent', "").withClass('text-center description'),
            DTColumnBuilder.newColumn('address', 'Address').withOption('defaultContent', "").withClass('text-center description'),
            DTColumnBuilder.newColumn('phone', 'Phone').withOption('defaultContent', "").withClass('text-center description'),
            DTColumnBuilder.newColumn(null).withTitle('Action').notSortable().withClass('text-center ')
                .renderWith(function(data, type, full, meta) {
                    return '<button class="btn btn-warning editGenBtn" data-id="' + data._id + '"  data-name="' + data.name + '" data-phone="' + data.phone + '" data-image="' + data.image + '" data-cnic="' + data.cnic + '"data-address="' + data.address + '" ng-click="editCategory($event)">' +
                        'Edit' +
                        '</button>&nbsp;' + '      ' +
                        '<button ng-class="' + data.status + '" && "btn-warning" ||"' + data.status + '" && btn-danger" class="btn btn-danger delCatBtn" data-id="' + data._id + '" data-status="' + data.status + '" ng-click="archiveCategory($event,$index)" ng-if="' + data.status + '==true">Archive</button>' +
                        '   ' +
                        '<button ng-class="' + data.status + '" && "btn-warning" ||"' + data.status + '" && btn-danger" class="btn btn-success delCatBtn" data-id="' + data._id + '" data-status="' + data.status + '" ng-click="archiveCategory($event,$index)" ng-if="' + data.status + '==false">Un Archive</button>'
                }),
        ];

        $scope.subCatRedirect = function(categoryId) {
            var id = categoryId.currentTarget.getAttribute("data-id");
            share.addProduct(id);
            $location.url('/subCategories')
        }
        $scope.addCategory = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/drivers/addEditDriver.html",
                size: "md",
                controller: 'AddEditDriverController',
                controllerAs: 'vm',
                resolve: {
                    $category: function() {
                        return {
                            isNew: true
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                ctg.page = 0;
                ctg.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.archiveCategory = function(categoryId, $index) {
            var id = categoryId.currentTarget.getAttribute("data-id");
            var status = categoryId.currentTarget.getAttribute("data-status");
            var myobj = {}
            if (status == "true") {
                myobj.categoryId = id,
                    myobj.status = 0
            } else {
                myobj.categoryId = id,
                    myobj.status = 1
            }
            usSpinnerService.spin('spinner-1');
            driversServices.EditRider({
                riderId: myobj.categoryId,
                status: myobj.status
            }).then(function(res) {
                if (res.success === 1) {
                    usSpinnerService.stop('spinner-1');
                    $state.reload();
                    toastr.success("Rider updated successfully.");
                } else {
                    usSpinnerService.stop('spinner-1');
                    toastr.error(res.error);
                }
            });
        };

        $scope.editCategory = function(item) {
            var category = {};
            category.id = item.currentTarget.getAttribute("data-id");
            category.name = item.currentTarget.getAttribute("data-name");
            category.phone = item.currentTarget.getAttribute("data-phone");
            category.cnic = item.currentTarget.getAttribute("data-cnic");
            category.address = item.currentTarget.getAttribute("data-address");
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/pages/drivers/addEditDriver.html",
                size: "md",
                controller: 'AddEditDriverController',
                controllerAs: 'vm',
                resolve: {
                    $category: function() {
                        return {
                            isNew: false,
                            category: category
                        };
                    }
                }
            });

            modalInstance.result.then(function() {
                $scope.page = 0;
                $scope.categories = [];
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        }
    }

    angular.module('drivers').controller("AddEditDriverController", AddEditDriverController);

    AddEditDriverController.$inject = ['$scope', 'toastr', '$state', 'driversServices', 'usSpinnerService', '$uibModalInstance', '$category', 'Upload'];

    function AddEditDriverController($scope, toastr, $state, driversServices, usSpinnerService, $uibModalInstance, $category, Upload) {
        var ctgAE = this;
        ctgAE.title = "Add New Driver";
        ctgAE.re = /^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/;
        if (!$category.isNew) {
            ctgAE.categoryName = $category.category.name;
            ctgAE.categoryPhone = $category.category.phone;
            ctgAE.cnic = $category.category.cnic;
            ctgAE.address = $category.category.address;
            ctgAE.riderId = $category.category.id;
            ctgAE.status = 1;
            ctgAE.title = "Edit Driver";
        }
        ctgAE.inProgress = false;
        ctgAE.addEditTask = function(file) {
            if ($category.isNew) {
                ctgAE.inProgress = true;
                $scope.thumbnail = [];
                usSpinnerService.spin('spinner-1');
                driversServices.addRider({
                    name: ctgAE.categoryName,
                    phone: ctgAE.categoryPhone,
                    cnic : ctgAE.cnic,
                    address : ctgAE.address
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Driver added successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
            } else {
                usSpinnerService.spin('spinner-1');
                driversServices.EditRider({
                    name: ctgAE.categoryName,
                    phone: ctgAE.categoryPhone,
                    riderId: ctgAE.riderId,
                    status: ctgAE.status,
                    cnic : ctgAE.cnic,
                    address : ctgAE.address
                }).then(function(res) {
                    if (res.success === 1) {
                        $state.reload();
                        usSpinnerService.stop('spinner-1');
                        toastr.success("Driver Info updated successfully.");
                        $uibModalInstance.close();
                    } else {
                        usSpinnerService.stop('spinner-1');
                        ctgAE.inProgress = false;
                        toastr.error(res.message);
                    }
                });
                var myobj = {}
                ctgAE.inProgress = true;
            }
        }
    }
})();
