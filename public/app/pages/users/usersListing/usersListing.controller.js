/**
 * Created by Raza on 8/1/2017.
 */
(function () {
    angular.module('users').controller("UserListingController", UserListingController);

    UserListingController.$inject = ['toastr', 'usersServices'];

    function UserListingController(toastr, usersServices) {
        var users = this;
        users.page = 0;
        users.limit = 50;
        users.users = [];

        getUsers();
        function getUsers() {
            var params = {
                offset: users.limit * users.page,
                limit: users.limit
            };

            return usersServices.getUsers(params).then(function (res) {
                if (res.success == "1") {
                    if (res.data.users.length > 0) {
                        res.data.users.forEach(function (lUser) {
                            users.users.push(lUser);
                        });
                    }

                    users.page++;
                } else {
                    toastr.error(response.error, 'Get Users');
                }
            });
        }
    }
})();



