/**
 * Created by Raza on 8/7/2017.
 */
(function () {
    angular.module('users').service('usersServices', ['$q', 'apiService', function ($q, apiService) {

        var usersServices = {};

        var getUsers = function (parameters) {
            var deferred = $q.defer();
            apiService.get("admin/users?offset=" + parameters.offset + "&limit=" + parameters.limit).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var addKpo = function (parameters) {
            var deferred = $q.defer();
            apiService.create("user/addKpo", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var getKpo = function (parameters) {
            var deferred = $q.defer();
            apiService.get("user/getKpo?offset=" + parameters.offset + "&limit=" + parameters.limit).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        var editKpo = function(parameters) {
            var deferred = $q.defer();
            apiService.create("user/editKpo", parameters).then(function (response) {
                    if (response)
                        deferred.resolve(response);
                    else
                        deferred.reject("Something went wrong while processing your request. Please Contact Administrator.");
                },
                function (response) {
                    deferred.reject(response);
                });
            return deferred.promise;
        };

        usersServices.getUsers = getUsers;
        usersServices.addKpo = addKpo;
        usersServices.getKpo = getKpo;
        usersServices.editKpo = editKpo;

        return usersServices;

    }]);
})();