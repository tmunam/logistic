/**
 * Created by Raza on 8/7/2017.
 */
(function () {
    'use strict';

    angular.module('users', []).config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {

        function authentication(GlobalServices, $q, localStorageService, $state) {
            var d = $q.defer();
            var checkUser = localStorageService.get("user");
            if (checkUser !== null) {
                d.resolve(checkUser);
            } else {
                GlobalServices.currentUser().then(function (data) {
                    if (data.success === 0) {
                        $state.go('login');
                    } else {
                        localStorageService.set('user', data.data.account);
                        d.resolve(data.user);
                    }
                });
            }
            return d.promise;
        }

        $stateProvider
            /*.state('users', {
                url: '/users',
                template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
                abstract: true,
                title: 'Users',
                sidebarMeta: {
                    icon: 'ion-person-stalker', //ion-grid
                    order: 30,
                },
                resolve: {
                    $user: authentication
                },
            })*/
            .state('userslisting', {
                url: '/users/listing',
                templateUrl: 'app/pages/users/usersListing/usersListing.html',
                controller: 'UserListingController',
                controllerAs: "listing",
                title: 'User Listing',
                /*sidebarMeta: {
                    order: 0,
                },*/
            });

        //$urlRouterProvider.when('/users', '/users/listing');
    }

})();