/**
 * Created by Bilal on 8/22/2017.
 */

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _promotions = mongoose.model('promotions'),
    responseModule = require('../../config/response'),
    agendaHelper = require('../agenda/agenda.helper'),
    products = mongoose.model('products'),
    pushNotifications = mongoose.model('pushNotifications'),
    moment = require('moment');


let listAllPromotions = (req, res, next) => {
    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchPromotions = {};
    if (searchText) {
        searchPromotions.label = {'$regex': searchText, '$options': 'i'};
    }
    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    let countPromotions = 0;
    return _promotions.count(searchPromotions).then(promotionsCount => {
        countPromotions = promotionsCount;
        return _promotions.find(searchPromotions, {
            label: 1,
            description: 1,
            image: 1,
            discountPercentage: 1,
            isDateRange: 1,
            startDate: 1,
            endDate: 1,
            status: 1,
            products: 1,
            isPushNotificationsSend: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'});

    }).then(promotionsFound => {
        if (promotionsFound && promotionsFound.length) {
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promotions fetched successfully.",
                data: {promotions: promotionsFound, count: countPromotions}

            });
        }
        else {
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promotions fetched successfully.",
                data: {promotions: promotionsFound, count: countPromotions}

            });

        }
    }).catch(err => {
        return next(err);
    });
};


let getPromotionsWithProducts = (req, res, next) => {
    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    let countPromotions = 0,
        currentDate = moment.utc().unix();


    return _promotions.count().then(promotionsCount => {

        return _promotions.find({"products.0": {"$exists": true},"status" :{$eq: false} , "endDate": {$gte: currentDate}, "startDate": {$lte: currentDate}}, {
            label: 1,
            description: 1,
            image: 1,
            discountPercentage: 1,
            isDateRange: 1,
            startDate: 1,
            endDate: 1,
            status: 1,
            products: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'});

    }).then(promotionsFound => {
        if (promotionsFound && promotionsFound.length) {
            countPromotions = promotionsFound.length;
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promotions fetched successfully.",
                data: {promotions: promotionsFound, count: countPromotions}

            });
        }
        else {
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promotions fetched successfully.",
                data: {promotions: promotionsFound, count: countPromotions}

            });

        }
    }).catch(err => {
        return next(err);
    });
}


let addPromotions = (req, res, next) => {

    const label = _.trim(req.body.label),
        description = _.trim(req.body.description) || "",
        image = _.trim(req.body.image),
        discountPercentage = parseInt(req.body.discountPercentage),
        isDateRange = req.body.isDateRange,
        startDate = moment.utc(req.body.startDate).add(1 , 'days').hours(0).unix(),
        endDate = moment.utc(req.body.endDate).add(1 , 'days').hours(24).unix();

    let promotionsObject = {
        label: label,
        image: image,
        description: description,
        discountPercentage: discountPercentage,
        isDateRange: isDateRange,
        startDate: startDate,
        endDate: endDate
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    if (isDateRange == true) {
        promotionsObject.startDate = startDate;
    }
    else {
        delete promotionsObject.startDate
    }
    if (isDateRange == true) {
        promotionsObject.endDate = endDate;
    }
    else {
        delete promotionsObject.endDate
    }
    if (startDate > endDate) {
        return next({msgCode: 1217});
    } else {
        promotionsObject.startDate = startDate;
    }
    if (startDate < endDate) {
        promotionsObject.endDate = endDate;
    } else {
        return next({msgCode: 1217});
    }
    if (discountPercentage >= 100) {
        return next({msgCode: 1215});
    }
    else {
        promotionsObject.discountPercentage = discountPercentage;
    }

    return _promotions.findOne({label: label}).then(promotionLabelFound => {
        if (promotionLabelFound) {
            throw {msgCode: 1212};
        }
        else {
            let promotionsCreateObject = new _promotions(promotionsObject);
            return promotionsCreateObject.save();

        }
    }).then(promotionsCreated => {

        resultObject.message = "Promotions created successfully.";
        resultObject.data = promotionsCreated;
        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}


let addProductsToPromotions = (req, res, next) => {

    const productId = req.body.productId,
        enforce = parseInt(req.body.enforce) || 0, //0 or 1
        promotionId = _.trim(req.body.promotionId);

    let productFoundObject = {};

    return _promotions.findOne({
        products: productId
    }, {products: 1}).then(promotionsFound => {
        if (promotionsFound && promotionsFound._id == promotionId) {
            throw next({msgCode: 1216});
        }
        else {
            if (promotionsFound && promotionsFound._id != promotionId && !enforce) {
                throw {msgCode: 1206};
            }
            else if (promotionsFound && promotionsFound.products && promotionsFound.products.length > 0 && enforce) {
                return _promotions.findOneAndUpdate({_id: promotionsFound._id}, {$pull: {products: productId}}, {multi: true}).exec();
            }
            else {
                return true;
            }
        }
    }).then(() => {
        return products.findOne({_id: productId}, {_id: 1, price: 1, isDiscounted: 1});
    }).then(productFound => {
        if (productFound && productFound.isDiscounted && !enforce) {
            throw {msgCode: 1207};
        } else {
            productFoundObject = productFound;
            return _promotions.findOneAndUpdate({_id: promotionId}, {$push: {products: productFound._id}}, {new: true}).exec();
        }
    }).then(promotionUpdated => {

        promotionUpdated = promotionUpdated.toObject();

        let discountPrice = productFoundObject.price * (parseInt(promotionUpdated.discountPercentage) / 100),
            finalPrice = productFoundObject.price - discountPrice;

        products.findOneAndUpdate({_id: productId}, {
            $set: {
                isDiscounted: true,
                discountedPrice: finalPrice.toFixed(2),
                discountPrice: discountPrice.toFixed(2),
                discountPercentage: promotionUpdated.discountPercentage
            }
        }).exec();

        promotionUpdated.discountPrice = finalPrice;
        let resultObject = {
            success: 1,
            message: "Product added to promotion successfully.",
            data: promotionUpdated
        }

        responseModule.successResponse(res, resultObject);
    }).catch(err => {

        if (err && err.msgCode === 1207) {
            responseModule.successResponse(res, {
                success: 1,
                message: "Product is already discounted.",
                data: {enforce: 1}
            });
        }
        else if (err && err.msgCode === 1206) {
            responseModule.successResponse(res, {
                success: 1,
                message: "Product is already in another promotion.",
                data: {enforce: 1}
            });
        }
        else {
            return next(err);
        }
    });
}


let editProductsToPromotions = (req, res, next) => {

    const promotionId = _.trim(req.body.promotionId),
        productId = _.trim(req.body.productId);

    let idObject = {};

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    if (productId) {
        idObject.$pull = {products: productId};
    }
    else {
        delete idObject.products;
    }

    return _promotions.findOneAndUpdate({_id: promotionId}, idObject, {new: true}).then(promotionUpdated => {
        if (promotionUpdated) {
            return products.findOneAndUpdate({_id: productId}, {$set: {isDiscounted: false}});
        }
        else {
            throw {msgCode: 8005};
        }
    }).then(productUpdate => {
        resultObject.message = "Product deleted from promotion successfully";
        resultObject.data = {};

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}


let editPromotions = (req, res, next) => {


    const label = _.trim(req.body.label),
        description = _.trim(req.body.description) || "",
        image = _.trim(req.body.image) || "",
        discountPercentage = parseInt(req.body.discountPercentage) || 0,
        isDateRange = req.body.isDateRange,
        startDate =moment.utc(req.body.startDate).unix(),
        endDate = moment.utc(req.body.endDate).hours(23).unix(),
        status = parseInt(req.body.status),
        promotionId = _.trim(req.body.promotionId);


    let promotionsObject = {
        label: label,
        description: description,
        image: image,
        discountPercentage: discountPercentage,
        isDateRange: true,
        startDate: startDate,
        endDate: endDate,
        status: 0
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }
    if (!isDateRange) {
        delete promotionsObject.isDateRange;
    }
    if (!startDate) {

        delete promotionsObject.startDate;
    }
    else if(startDate < endDate){
        promotionsObject.startDate = startDate;
    }
    else {
        throw {msgCode: 1217};
    }
    if (!endDate) {
        delete promotionsObject.endDate;
    }
    else if(startDate < endDate){
        promotionsObject.endDate = endDate;
    }
    else {
        throw {msgCode: 1217};
    }
    if (isDateRange == "true") {
        promotionsObject.startDate = startDate;
    }
    else {

        delete promotionsObject.startDate
    }
    if (isDateRange == "true") {
        promotionsObject.endDate = endDate;
    }
    else {
        delete promotionsObject.endDate
    }

    if (discountPercentage >= 100) {
        return next({msgCode: 1215});
    }
    else {
        promotionsObject.discountPercentage = discountPercentage;
    }
    if (!label) {
        delete promotionsObject.label;
    }

    if (!image) {
        delete promotionsObject.image;
    }
    if (!description) {
        delete promotionsObject.description;
    }

    if (!discountPercentage) {
        delete promotionsObject.discountPercentage;
    }

    if (status == 1) {
        promotionsObject.status = status;
    }
    else if (status == 0) {
        promotionsObject.status = status;
    }


    return _promotions.findOneAndUpdate({_id: promotionId}, {$set: promotionsObject}, {new: true}).exec().then(promotionsCreated => {

        resultObject.message = "Promotion updated successfully.";
        resultObject.data = promotionsCreated;
        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}
let loadSinglePromotion = (req, res, next) => {

    const promotionId = _.trim(req.params.promotionId);

    return _promotions.findOne({_id: promotionId}, {
        label: 1,
        image: 1,
        discountPercentage: 1,
        isDateRange: 1,
        startDate: 1,
        description: 1,
        endDate: 1,
        status: 1,
        products: 1
    }).populate({
        path: "products",
        model: "products",
        select: "_id name"
    }).then(promotionFound => {
        if (promotionFound) {
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promotion loaded.",
                data: {promotion: promotionFound}
            });
        }
        else {
            throw {msgCode: 8012}
        }

    }).catch(err => {
        return next(err);
    });
}


let getProductsByPromotionId = (req, res, next) => {

    const promotionId = _.trim(req.body.promotionId);

    return _promotions.findOne({_id: promotionId}, {
        label: 1,
        image: 1,
        discountPercentage: 1,
        isDateRange: 1,
        startDate: 1,
        description: 1,
        endDate: 1,
        status: 1,
        products: 1
    }).populate({
        path: "products",
        model: "products",
        select: "_id name price unit description category discountPercentage discountedPrice subCategory discountPrice isDiscounted status images"
    }).then(promotionFound => {
        if (promotionFound) {
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promotion loaded.",
                data: {promotion: promotionFound}
            });
        }
        else {
            throw {msgCode: 8012}
        }

    }).catch(err => {
        return next(err);
    });
}

let sendPromotionPushNotification = (req, res, next) => {
    const promotionId = _.trim(req.params.promotionId);
    return _promotions.findOne({_id: promotionId}).exec()
        .then(promotions => {

            let message = promotions.label;

            pushNotifications.find({}).exec().then(userAPNS => {

                if (userAPNS.length > 0) {
                    _.forEach(userAPNS , (apns) => {
                        agendaHelper.pushJob({
                            userId: apns.user,
                            message: message,
                            messageType: 'promotions',
                            senderName: 'Raft',
                            badge: 1,
                            resource: promotionId,
                            statusCase: 1
                        }, 1);
                    });
                    next();
                } else {
                    let resultObject = {
                        success: 0,
                        message: "No user apns token found.",
                    };
                    responseModule.successResponse(res, resultObject);
                }
            });
        }).catch(err => {
            return next(err);
        });
};

let updatePromotionPushNotification = (req, res, next) => {
    const promotionId = _.trim(req.params.promotionId);
    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    let promotionsObject = {isPushNotificationsSend: true};

    return _promotions.findOneAndUpdate({_id: promotionId}, {$set: promotionsObject}, {new: true}).exec().then(promotionsCreated => {
        resultObject.message = "Promotion push notifications has been sent successfully.";
        resultObject.data = promotionsCreated;
        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
};

module.exports = {
    listAllPromotions,
    addPromotions,
    editPromotions,
    addProductsToPromotions,
    editProductsToPromotions,
    loadSinglePromotion,
    getProductsByPromotionId,
    getPromotionsWithProducts,
    sendPromotionPushNotification,
    updatePromotionPushNotification
}
