const promotionsMiddleware = require('./promotions.middleware.js'),
    promotionsController = require('./promotions.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/promotions/addPromotions', passport.isAuthenticated, promotionsMiddleware.checkPromotionAddParam, promotionsController.addPromotions);
    app.post(version + '/promotions/listAllPromotions/:offset/:limit', passport.isAuthenticated, promotionsController.listAllPromotions);
    app.post(version + '/promotions/getProductsByPromotionId', passport.isAuthenticated, promotionsController.getProductsByPromotionId);
    app.post(version + '/promotions/editPromotions', passport.isAuthenticated, promotionsMiddleware.checkPromotionEditParam, promotionsController.editPromotions);
    app.post(version + '/promotions/addProductsToPromotions', passport.isAuthenticated, promotionsMiddleware.checkPromotionId, promotionsController.addProductsToPromotions);
    app.post(version + '/promotions/editProductsToPromotions', passport.isAuthenticated, promotionsMiddleware.checkPromotionId, promotionsController.editProductsToPromotions);
    app.get(version + '/promotions/loadSinglePromotion/:promotionId', passport.isAuthenticated, promotionsMiddleware.checkPromotionEditParam, promotionsController.loadSinglePromotion);
    app.get(version + '/promotions/push/notifications/:promotionId', passport.isAuthenticated, promotionsMiddleware.checkPromotionEditParam, promotionsController.sendPromotionPushNotification, promotionsController.updatePromotionPushNotification);
    app.get(version + '/promotions/getPromotionsWithProducts/:offset/:limit', promotionsController.getPromotionsWithProducts);
}