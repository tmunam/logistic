/**
 * Created by Bilal on 8/25/2017.
 */

'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let promotionsSchema = new Schema({
    label: {type: String, required: true},
    image: {type: String},
    discountPercentage: {type: Number},
    isDateRange: {type: Boolean},
    startDate: {type: Number},
    endDate: {type: Number},
    description: {type: String},
    status: {type: Boolean, default: false},
    products: [
        {type: Schema.Types.ObjectId, ref: 'products'}
    ],
    isPushNotificationsSend: {type: Boolean, default: false},
});
promotionsSchema.plugin(timestamps);
module.exports = mongoose.model('promotions', promotionsSchema);
