/**
 * Created by Bilal on 8/22/2017.
 */
const winston = require('winston');
let checkPromotionEditParam = (req, res, next) => {

    req.assert('promotionId', 1202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}
let checkPromotionAddParam = (req, res, next) => {

    req.assert('label', 1203).notEmpty();
    req.assert('image', 1204).notEmpty();/*
    req.assert('startDate', 1213).isValidDateAndTime();
    req.assert('endDate', 1213).isValidDateAndTime();*/

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let checkPromotionId = (req, res, next) => {

    req.assert('productId', 1210).notEmpty();
    req.assert('promotionId', 1209).notEmpty();
    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}


module.exports = {
    checkPromotionEditParam,
    checkPromotionAddParam,
    checkPromotionId
}