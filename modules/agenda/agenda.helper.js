/**
 * Created by Asif on 7/14/2017.
 */


const Agenda = require('agenda');

let agenda = new Agenda({
    db: {
        address: config.mongodb.host + config.mongodb.db_name
    }
});

let pushJob = (data, type) => {
    agenda.now('pushNotification', {
        type : type,
        notificationInfo : data
    });
}


module.exports = {
    pushJob
}