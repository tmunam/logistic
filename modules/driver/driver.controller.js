/**
 * Created by Asif on 8/22/2017.
 */


const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _driver = mongoose.model('driver'),
    responseModule = require('../../config/response');


let listAllRiders = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchRider = { status : true };

    if (searchText) {
        searchRider.name = {'$regex': searchText, '$options': 'i'};
    }

    let countRiders = 0;

    return _driver.count(searchRider).then(ridersCount => {
        countRiders = ridersCount;

        return _driver.find(searchRider, {
            status: 1,
            name: 1,
            description: 1,
            address: 1,
            phone: 1,
            cnic : 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(ridersFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Riders fetched successfully.",
            data: {riders: ridersFound, count: countRiders}
        });


    }).catch(err => {
        return next(err);
    });

}

let getAllRiders = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchRider = {status: true};

    if (searchText) {
        searchRider.name = {'$regex': searchText, '$options': 'i'};
    }

    let countRiders = 0;

    return _driver.count(searchRider).then(ridersCount => {
        countRiders = ridersCount;

        return _driver.find(searchRider, {
            status: 1,
            name: 1,
            description: 1,
            address: 1,
            phone: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(ridersFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Riders fetched successfully.",
            data: {riders: ridersFound, count: countRiders}
        });


    }).catch(err => {
        return next(err);
    });

}

let addRider = (req, res, next) => {

    const name = _.trim(req.body.name),
        description = _.trim(req.body.description) || "",
        address = _.trim(req.body.address) || "",
        cnic = _.trim(req.body.cnic) || "",
        salary = _.trim(req.body.salary) || "",
        phone = _.trim(req.body.phone) || "",
        status = parseInt(req.body.status) || 1,
        latitude = req.body.latitude || 0,
        longitude = req.body.longitude || 0;

    let riderObject = {
        name: name,
        description: description,
        address: address,
        cnic: cnic,
        salary: salary,
        phone: phone,
        status: status,
        riderLocation: {type: 'Point', coordinates: [latitude, longitude]}
    }

    if (!latitude && !longitude) {
        delete riderObject.riderLocation;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }
    return _driver.findOne({name: name}).then(riderFound => {
        if (riderFound) {
            throw {msgCode: 9206};
        }
        else {
            let riderCreateObject = new _driver(riderObject);

            return riderCreateObject.save();
        }
    }).then(riderCreated => {

        resultObject.message = "Rider created successfully.";
        resultObject.data = riderCreated;

        responseModule.successResponse(res, resultObject);

    }).catch(err => {
        return next(err);
    });
}

let editRider = (req, res, next) => {

    const name = _.trim(req.body.name),
        description = _.trim(req.body.description) || "",
        address = _.trim(req.body.address) || "",
        phone = _.trim(req.body.phone) || "",
        status = parseInt(req.body.status) ? 1 : 0,
        latitude = req.body.latitude || 0,
        longitude = req.body.longitude || 0,
        riderId = _.trim(req.body.riderId);


    let riderObject = {
        name: name,
        description: description,
        address: address,
        phone: phone,
        status: 0,
        riderLocation: {type: 'Point', coordinates: [latitude, longitude]}
    }

    if (!name) {
        delete riderObject.name;
    }

    if (!description) {
        delete riderObject.description;
    }

    if (!address) {
        delete riderObject.address;
    }

    if (!phone) {
        delete riderObject.phone;
    }

    if (!req.body.hasOwnProperty('status')) {
        delete riderObject.status;
    }
    if (status == 1) {
        riderObject.status = status;
    }
    else if (status == 0) {
        riderObject.status = status;
    }
    else {
        delete riderObject.status;
    }

    if (!latitude && !longitude) {
        delete riderObject.riderLocation;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return _driver.findOneAndUpdate({_id: riderId}, {$set: riderObject}, {new: true}).exec().then(riderCreated => {
        resultObject.message = "Rider updated successfully.";
        resultObject.data = riderCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

module.exports = {
    listAllRiders,
    addRider,
    editRider,
    getAllRiders
}
