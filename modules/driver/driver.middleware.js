
const winston = require('winston');

let checkRiderAddParam = (req, res, next) => {

    req.assert('name', 9200).notEmpty();
    // req.assert('address', 9201).notEmpty();
    req.assert('phone', 9202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let checkRiderEditParam = (req, res, next) => {

    req.assert('riderId', 9204).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

module.exports = {
    checkRiderAddParam,
    checkRiderEditParam
}
