
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let driverSchema = new Schema({
    name: {type: String, required: true},
    description: {type: String},
    cnic: {type: String},
    salary: {type: String},
    address: {type: String},
    phone: {type: String},
    status: {type: Boolean, default: true},
    riderLocation : {
        type: { type: String },
        coordinates : [Number]
    }
});

driverSchema.plugin(timestamps);
//riderSchema.index({riderLocation: '2dsphere'});

module.exports = mongoose.model('driver', driverSchema);
