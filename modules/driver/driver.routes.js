const riderMiddleware = require('./driver.middleware.js'),
    riderController = require('./driver.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/rider/getAllRiders/:offset/:limit', passport.isAuthenticated, riderController.listAllRiders);
    app.post(version + '/rider/addRider', passport.isAuthenticated , riderMiddleware.checkRiderAddParam, riderController.addRider);
    app.post(version + '/rider/editRider', passport.isAuthenticated, riderMiddleware.checkRiderEditParam, riderController.editRider);
    app.get(version + '/rider/getAllRidersArchived', passport.isAuthenticated, riderController.getAllRiders);

};