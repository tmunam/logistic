const ratingsController = require('./ratings.controller'),
    passport = require('../../config/passport'),
    multer = require('../../config/multer'),
    ratingsMiddleware = require('./ratings.middleware');

module.exports = function (app, version) {

    app.post(version + '/ratings/addRatings',ratingsMiddleware.createRatingParams, ratingsController.addRatings);

};