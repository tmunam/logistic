/**
 * Created by Bilal on 9/8/2017.
 */

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    ratings = mongoose.model('ratings'),
    responseModule = require('../../config/response'),
    _orders = mongoose.model('orders');


let addRatings = (req, res, next) => {
    const orderRatings = _.trim(req.body.orderRatings),
        orderId = _.trim(req.body.orderId),
        appRatings = _.trim(req.body.appRatings);

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }


    const ratingsObject = {
        orderRatings: orderRatings,
        orderId: orderId,
        appRatings: appRatings
    }

    if (orderRatings > 5) {
        throw {msgCode: 1401};
    }
    else {
        ratingsObject.orderRatings = orderRatings;
    }
    if (appRatings > 5) {
        throw {msgCode: 1402};
    }
    else {
        ratingsObject.appRatings = appRatings;
    }
    return ratings.findOne({orderId: orderId}).then(ratingFound => {
        if (ratingFound) {
            throw next({msgCode: 1400});
        }
        else {
            return _orders.findOne({orderId: orderId});
        }

    }).then(orderFound => {
            if (orderFound) {

                let ratingsCreate = new ratings(ratingsObject);
                return ratingsCreate.save();
            } else {
                throw {msgCode: 1403};
            }
        })
        .then(ratingCreated => {
            resultObject.message = "Ratings created successfully.";
            resultObject.data = ratingCreated;
            responseModule.successResponse(res, resultObject);
        }).catch(err => {
            return next(err);
        });
}

module.exports = {
    addRatings

};