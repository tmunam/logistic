
const winston = require('winston');

let createRatingParams = (req, res, next) => {

    req.assert('orderId', 1404).notEmpty();
    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

module.exports = {
    createRatingParams

}