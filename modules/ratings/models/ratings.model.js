/**
 * Created by Bilal on 9/8/2017.
 */
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let ratingsSchema = new Schema({
    appRatings: {type: Number, default:0},
    orderRatings: {type: Number, default:0},
    orderId: {type: Number},
    status: {type: Boolean, default: true}

});

ratingsSchema.plugin(timestamps);
module.exports = mongoose.model('ratings', ratingsSchema);