
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let expensesSchema = new Schema({
    invoice_id : { type : Schema.Types.ObjectId , ref : 'invoices'},
    expense_type : { type : String, required : true, enum: [
            'Diesel',
            'Oil change',
            'Vehicle maintainance',
            'Driver salary',
            'Staff salary',
            'Office rent',
            'Driver room rent',
            'Utility bills',
            'Toll tax',
            'Annual Token',
            'Route permit',
            'Transfer fee',
            'Police fine',
            'Misc Expenses',
            'Office expenses',
            'Offloading charges',
            'Transit loss',
            'Detention charges'
        ]},
    amount : { type : Number, required : true }
});

expensesSchema.plugin(timestamps);

module.exports = mongoose.model('expenses', expensesSchema);
