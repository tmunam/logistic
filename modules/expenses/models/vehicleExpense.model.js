
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let vehicleExpensesSchema = new Schema({
    vehicle_id : { type : Schema.Types.ObjectId , ref : 'vehicles'},
    driver_id : { type : Schema.Types.ObjectId , ref : 'driver'},
    expense_category : {
        type : String,
        required : true,
        enum: [
            'Daily expenses',
            'rnm',
            'tpt'
        ]
    },
    expense_type : {
        type : String,
        required : true,
        enum: [
            'Tool Tax',
            'Police Fine',
            'Diesel',
            'Off-loading Expenses',
            'Other Exp'
        ]
    },
    amount : { type : Number, required : true },
    remarks : { type : String },
    expense_date : { type : Date },
    status : { type : Boolean, default : true }
});

vehicleExpensesSchema.plugin(timestamps);

module.exports = mongoose.model('vehiclexpenses', vehicleExpensesSchema);
