

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    _vehicles = mongoose.model('vehicles'),
    _drivers = mongoose.model('driver'),
    _companies = mongoose.model('companies'),
    _destinations = mongoose.model('destinations'),
    _addVehicleExpense = mongoose.model('vehiclexpenses'),
    uuidv4 = require('uuid/v4');
    responseModule = require('../../config/response');


let listAllExpenses = (req, res, next) => {

    const offset = parseInt(req.body.offset) || 0,
        limit = parseInt(req.body.limit) || 10,
        vehicle_id = (req.body.vehicle_id),
        expense_category = (req.body.expenseCategory);

    let searchExpenses = { status : true, vehicle_id : vehicle_id, expense_category : expense_category };
    

    let countExpenses = 0;

    return _addVehicleExpense.count(searchExpenses).then(ExpensesCount => {
        countExpenses = ExpensesCount;

        return _addVehicleExpense.find(searchExpenses).skip(offset).limit(limit).sort({'createdAt': '-1'})
            .populate("driver_id", "name")
            .populate("vehicle_id", "number")
            .then(invoicesFound => {
            return invoicesFound;
        });
    }).then(expensesFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Expenses fetched successfully.",
            data: { expenses: expensesFound, count: countExpenses }
        });


    }).catch(err => {
        return next(err);
    });

};

let addVehicleExpense = (req, res, next) => {
    var expense_type = req.body.expense.expense_type,
        expense_category = req.body.expense.expenseCategory,
        remarks = req.body.expense.remarks,
        vehicle_id = req.body.expense.vehicle._id,
        driver_id = req.body.expense.vehicle.driver._id,
        expense_date = req.body.expense.expense_date,
        amount = req.body.expense.amount;

    var objExpense = {
        expense_type,
        amount,
        expense_category,
        expense_date,
        remarks,
        vehicle_id,
        driver_id
    };

    var tempInstance = new _addVehicleExpense(objExpense);

    return tempInstance.save().then(expAdded => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Expense fetched successfully.",
            data: { expense: expAdded }
        });
    }).catch(err => {
        return next(err);
    });

};

let addExpenseHelper = ({invoice_id = null, expense_type = '', amount = 0} = {}) => {
    var obj = {
        invoice_id,
        expense_type,
        amount
    };
    if (!obj.invoice_id)
        delete obj.invoice_id;
    var tempInstance = new _addVehicleExpense(obj);

    return tempInstance.save();
};

let editExpense = (req, res, next) => {

};

let getVehicleExpense = (req, res, next) => {
    var expenseCategory = req.body.expenseCategory,
        vehicle = req.body.vehicle;

    
};

module.exports = {
    listAllExpenses,
    addVehicleExpense,
    editExpense,
    getVehicleExpense,
    addExpenseHelper
};
