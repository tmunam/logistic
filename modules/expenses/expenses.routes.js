const expensesMiddleware = require('./expenses.middleware.js'),
    expensesController = require('./expenses.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/expenses/getAllExpenses/:offset/:limit', passport.isAuthenticated, expensesController.listAllExpenses);
    app.post(version + '/expenses/addVehicleExpense', passport.isAuthenticated , expensesController.addVehicleExpense);
    app.post(version + '/expenses/editExpense', passport.isAuthenticated , expensesController.editExpense);
    app.post(version + '/expenses/getVehicleExpense', passport.isAuthenticated , expensesController.listAllExpenses);
};