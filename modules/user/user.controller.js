const mongoose = require('mongoose');
const User = mongoose.model('User');
const _driver = mongoose.model('driver');
const _invoices = mongoose.model('invoices');
const _vehicles = mongoose.model('vehicles');
const _companies = mongoose.model('companies');
const passport = require('passport');
const _ = require('lodash');
const async = require('async');
const randomize = require('randomatic');
const mailer = require('../../config/mailer');
const lib = require('../common/common.library');
const sms = require('../../config/sms');
const AccountVerification = mongoose.model('AccountVerification');
const moment = require("moment");
const winston = require('winston'),
    responseModule = require('../../config/response'),
    multer = require('../../config/multer'),
    pushNotification = require('../../config/notifications'),
    PN = mongoose.model('pushNotifications');

function sendVerificationSms(number, verificationCode, text, cb) {
    let smsObject = {
        to: number,
        text: text + ' \nVerification code: ' + verificationCode + ' \nThank You! \nTeam RAFT Bazaar',
    };

    sms.sendMessage(smsObject, (err) => {
        if (err) {
            return cb(err);
        }
        else {
            return cb();
        }
    });
}

module.exports = {
    logInAccount(req, res, next) {
        passport.authenticate('local', (err, acct, info) => {
            if (err) {
                return next(err);
            }
            if (!acct) {
                return next(info);
            }
            req.logIn(acct, (err) => {
                if (err) {
                    return next(err);
                }

                if (req.body.deviceType && req.body.deviceToken) {
                    pushNotification.generateEndPoint(req).then(endPoint => {
                        if (endPoint) {
                            pushNotification.pushDeviceNotificationDetail(req.sessionID, req.user._id, endPoint, req.body.deviceType, req.body.deviceToken);
                        }
                    });
                }

                responseModule.successResponse(res, {
                    success: 1,
                    message: "User logged in successfully.",
                    data: lib.createAccountObjectForResponse(acct)
                });
            });
        })(req, res, next);
    },
    sendSignUpVerification (req, res, next) {
        User.findOne({$or: [{email: req.body.email}, {mobileNo: req.body.mobileNo}]}, (err, existingAcct) => {
            if (err) {
                return next(err);
            }
            if (existingAcct) {
                return next({msgCode: 5011});
            }
            else {
                User.findOne({$or: [{email: req.body.email}, {mobileNo: req.body.mobileNo}]}, (err, existingAcct) => {
                    if (err) {
                        return next(err);
                    }
                    if (existingAcct) {

                        let verificationCode = randomize('0', config.verificationCodeLength);
                        let expTime = new Date();
                        let verificationCodeExpirationTime = expTime.setMinutes(expTime.getMinutes() + config.verifyAcctCodeExpiryMinutes);
                        req.body.verificationCode = verificationCode;
                        let mobile = req.body.mobileNo,
                            trimmed = mobile.replace(/\b0+/g, "");
                        mobile = "92" + trimmed;

                        User.update({_id: existingAcct._id}, {
                            $set: {
                                verificationCode: verificationCode,
                                verificationCodeExpirationTime: verificationCodeExpirationTime,
                                isAccountVerified: false,
                                email: req.body.email,
                                mobileNumber: mobile,
                                name: req.body.name
                            }
                        }, (err) => {
                            if (err) {
                                return next(err);
                            }
                            else {
                                sendVerificationCode();
                            }
                        });

                    } else {
                        req.body.verificationCode = randomize('0', config.verificationCodeLength);

                        let expTime = new Date();
                        req.body.verificationCodeExpirationTime = expTime.setMinutes(expTime.getMinutes() + config.verifyAcctCodeExpiryMinutes);

                        saveVerificationAccount(req.body, (err, acctVerification) => {
                            if (err) {
                                return next(err);
                            }
                            else {
                                sendVerificationCode();
                            }
                        });
                    }
                });

                function saveVerificationAccount(acctVerificationObject, cb) {


                    let trimmed = req.body.mobileNo.replace(/\b0+/g, ""),
                        mobile = "92" + trimmed;

                    let account = {
                        name: req.body.name,
                        email: req.body.email,
                        password: req.body.password,
                        mobileNo: mobile,
                        status: 0,
                        addressLocation: {
                            [req.body.addressType]: req.body.addressName
                        },
                        addressCoordinates: {
                            [req.body.addressType]: {
                                coordinates: req.body.latLong
                            }
                        },
                        verificationCode: req.body.verificationCode,
                        verificationCodeExpirationTime: req.body.verificationCodeExpirationTime,
                        isNumberVerified: false,
                        name: req.body.name,
                        deviceType: req.body.deviceType || "",
                        deviceToken: req.body.deviceToken || ""
                    };
                    const userSave = new User(account);
                    userSave.save((err) => {
                        if (err) {
                            return cb(err);
                        }

                        return cb(null, userSave);
                    });
                }

                function sendVerificationCode() {
                    let text = "Your Account Verification Code";
                    async.parallel([
                        (mailCb) => {

                            let vars = {
                                verificationCode: req.body.verificationCode,
                                name: req.body.name
                            };
                            lib.sendEmail(req.body.email, text, vars, 'account_verification', (err) => {
                                if (err) {
                                    return mailCb(err);
                                } else {
                                    return mailCb();
                                }
                            });
                        },
                        (smsCb) => {
                            let txt = 'Dear ' + req.body.name + ', To complete your sign in, please enter the following code on the Mobile Application:';
                            let trimmed = req.body.mobileNo.replace(/\b0+/g, ""),
                                mobile = "92" + trimmed;
                            sendVerificationSms(mobile, req.body.verificationCode, txt, (err) => {
                                if (err) {
                                    return smsCb(err);
                                }
                                else {
                                    return smsCb();
                                }
                            });


                        }
                    ], (err) => {
                        if (err) {
                            return next(err);
                        }
                        else {
                            responseModule.successResponse(res, {
                                success: 1,
                                message: "A message with a verification code has been sent to your device.",
                                data: {}
                            });
                        }
                    });
                }
            }
        });
    },
    verifyAccount(req, res, next) {

        let acctId;
        async.series([
                (emailCb) => {
                    lib.queryAccount({
                            email: req.body.email
                        },
                        (err, acct) => {
                            if (err) {
                                return emailCb(err);
                            }
                            else {
                                if (acct == false) {
                                    return emailCb({msgCode: 5021});
                                }
                                else {
                                    acctId = acct._id;
                                    return emailCb();
                                }
                            }
                        });
                },
                (confirmCb) => {
                    lib.queryAccount({
                            email: req.body.email,
                            verificationCode: req.body.verificationCode,
                            isNumberVerified: true
                        },
                        (err, acct) => {
                            if (err) {
                                return confirmCb(err);
                            }
                            else {
                                if (acct) {
                                    return confirmCb({msgCode: 5044});
                                }
                                else {
                                    return confirmCb();
                                }
                            }
                        });
                },
                (codeCb) => {
                    lib.queryAccount({
                            email: req.body.email,
                            verificationCode: req.body.verificationCode
                        },
                        (err, acct) => {
                            if (err) {
                                return codeCb(err);
                            }
                            else {
                                if (acct == false) {
                                    return codeCb({msgCode: 5022});
                                }
                                else {
                                    return codeCb();
                                }
                            }
                        });
                },
                (expireCb) => {
                    lib.queryAccount({
                            email: req.body.email,
                            verificationCode: req.body.verificationCode,
                            verificationCodeExpirationTime: {
                                $gte: new Date()
                            }
                        },
                        (err, acct) => {
                            if (err) {
                                return expireCb(err);
                            }
                            else {
                                if (acct == false) {
                                    return expireCb({msgCode: 5023});
                                }
                                else {
                                    return expireCb();
                                }
                            }
                        });
                }],
            (err) => {
                if (err) {
                    return next(err);
                }
                else {
                    User.findOneAndUpdate({
                            _id: acctId
                        },
                        {
                            $set: {
                                isNumberVerified: true
                            }
                        },
                        (err, acct) => {
                            if (err) {
                                return next(err);
                            }
                            else {
                                async.parallel([
                                        (smsCb) => {
                                            smsCb();
                                        },
                                        (mail) => {

                                            let vars = {
                                                name: acct.name
                                            }
                                            lib.sendEmail(req.body.email, 'Welcome to RAFT Bazaar!', vars, 'user_welcome', (err) => {
                                                if (err) {
                                                    return mail(err);
                                                } else {
                                                    return mail();
                                                }
                                            });

                                        }

                                    ],
                                    (err) => {
                                        if (err) {
                                            return next(err);
                                        }
                                        else {

                                            req.logIn(acct, (err) => {
                                                if (err) {
                                                    return next(err);
                                                }

                                                if (req.body.deviceType && req.body.deviceToken) {
                                                    pushNotification.generateEndPoint(req).then(endPoint => {
                                                        if (endPoint) {
                                                            pushNotification.pushDeviceNotificationDetail(req.sessionID, req.user._id, endPoint, req.body.deviceType, req.body.deviceToken);
                                                        }
                                                    });
                                                }

                                                responseModule.successResponse(res, {
                                                    success: 1,
                                                    message: "Account verified successfully.",
                                                    data: req.user ? {name: req.user.name, email: req.user.email} : {}
                                                });
                                            });

                                        }
                                    });
                            }
                        });
                }
            }
        );
    },
    logOutAccount(req, res, next) {

        try {
            let sid = req.sessionID;

            pushNotification.removeEndPointUsingSessionId(sid);

            req.session.destroy(function (err) {
                req.logout();
                req.logOut();
            });
        }
        catch (err) {
            winston.error(err);
        }

        responseModule.successResponse(res, {
            success: 1,
            message: "User logged out successfully.",
            data: {}
        });
    },
    getCurrentAccount(req, res, next) {
        responseModule.successResponse(res, {
            success: 1,
            message: "",
            data: req.user ? req.user : {}
        });
    },
    loadProfileData(req, res, next) {
        const userId = _.trim(req.user._id);

        User.findOne({_id: userId}, {
            name: 1,
            email: 1,
            mobileNo: 1,
            address: 1,
            status: 1,
            profileImage: 1
        }).then(userProfile => {
            responseModule.successResponse(res, {
                success: 1,
                message: "User data fetched successfully.",
                data: userProfile
            });
        }).catch(err => {
            return next(err);
        });
    },
    editProfile(req, res, next){
        let userId = _.trim(req.user._id);

        let name = _.trim(req.body.name) || "",
            profileImage = _.trim(req.body.profileImage) || "";

        let updateObject = {
            name: name,
            profileImage: profileImage
        }

        if (!name) {
            delete updateObject.name;
        }

        if (!profileImage) {
            delete updateObject.profileImage;
        }

        User.update({_id: userId}, {$set: updateObject}).exec().then(updatedObject => {
            responseModule.successResponse(res, {
                success: 1,
                message: "User profile updated successfully.",
                data: updatedObject
            });
        }).catch(err => {
            return next(err);
        });
    },
    addAddress(req, res, next){

        const userId = _.trim(req.user._id),
            address = _.trim(req.body.address),
            latitude = req.body.latitude || 0,
            longitude = req.body.longitude || 0,
            alias = req.body.alias || "OTHER";

        return User.findOne({_id: userId}, {address: 1}).then(userFound => {
            const address = userFound.address;

            if (alias === 'HOME') {
                let homeWorkExist = _.find(address, (addressObject) => {
                    return addressObject.alias === alias;
                });

                if (homeWorkExist) {
                    throw {msgCode: 5053};
                }
            }
            else if (alias === 'OTHER') {
                let homeWorkExist = _.find(address, (addressObject) => {
                    return addressObject.alias === alias;
                });

                if (homeWorkExist) {
                    throw {msgCode: 5052};
                }
            }
            else if (alias === 'WORK') {
                let homeWorkExist = _.find(address, (addressObject) => {
                    return addressObject.alias === alias;
                });

                if (homeWorkExist) {
                    throw {msgCode: 5052};
                }
            }
            else {
                throw {msgCode: 5056};
            }

            return User.update({
                _id: userId,
                'address.isSelected': true
            }, {$set: {'address.$.isSelected': false}}, {multi: true});

        }).then(userUpdated => {
            if (userUpdated) {
                return User.findOneAndUpdate({_id: userId}, {
                    $push: {
                        address: {
                            address: address,
                            location: {type: 'Point', coordinates: [latitude, longitude]},
                            alias: alias,
                            isSelected: true
                        }
                    }
                });
            }
            else {
                throw "There was error updating user.";
            }
        }).then(userUpdated => {
            responseModule.successResponse(res, {
                success: 1,
                message: "User address added successfully.",
                data: {}
            });
        }).catch(err => {
            return next(err);
        });
    },
    editAddress(req, res, next){

        let userId = _.trim(req.user._id),
            addressId = _.trim(req.body.addressId),
            address = _.trim(req.body.address) || "",
            latitude = req.body.latitude || 0,
            longitude = req.body.longitude || 0;
        //isSelected = req.body.isSelected || 0;

        if (address) {

            User.update({_id: userId, 'address._id': addressId}, {
                $set: {
                    'address.$.address': address,
                    'address.$.location': {type: 'Point', coordinates: [latitude, longitude]}
                }
            }).exec().then(userUpdated => {
                responseModule.successResponse(res, {
                    success: 1,
                    message: "User address updated successfully.",
                    data: userUpdated
                });
            }).catch(err => {
                return next(err);
            });
        }
        else {
            return next({msgCode: 5054});
        }
    },
    forgetPassword(req, res, next) {
        let verificationCode = randomize('0', config.verificationCodeLength);

        const userId = _.trim(req.body.userId);

        User.findOne({$or: [{mobileNo: userId}, {email: userId.toLowerCase()}]}, (err, userObject) => {
            if (err) {
                return next(err);
            }
            else {
                if (!userObject) {
                    return next({msgCode: 5015});
                }
                else {

                    let expTime = new Date();
                    expTime.setMinutes(expTime.getMinutes() + config.forgetPassCodeExpiryMinutes);
                    let forgetPassObject = {
                        verificationCodeExpirationTime: expTime,
                        verificationCode: verificationCode,
                        isVerificationCodeExpired: false
                    };

                    User.update({_id: userObject._id},
                        {$set: forgetPassObject},
                        {upsert: true},
                        (err)=> {
                            if (err) {
                                return next(err);
                            }
                            else {

                                let mobile = userObject.mobileNo,
                                    txt = 'Dear User, To reset password, please enter the following code  on the Mobile Application:';
                                sendVerificationSms(mobile, verificationCode, txt, (error, res) => {
                                    if (error) {
                                        return next(error);
                                    }
                                    else {
                                        return res;
                                    }
                                });

                                let vars = {
                                        verificationCode: verificationCode,
                                        name: userObject.name
                                    },
                                    text = "RAFT Password assistance";
                                lib.sendEmail(userObject.email, text, vars, 'forgot_password', (err) => {
                                    if (err) {
                                        return next(err);
                                    } else {
                                        responseModule.successResponse(res, {
                                            success: 1,
                                            message: "Forgot password requested.",
                                            data: {}
                                        });
                                    }
                                });
                            }
                        }
                    );
                }
            }
        });
    },
    resetPassword(req, res, next) {

        let userObject;

        const userId = _.trim(req.body.userId);

        async.series([
                (emailCb) => {
                    lib.queryAccount({$or: [{mobileNo: userId}, {email: userId.toLowerCase()}]},
                        (err, user) => {
                            if (err) {
                                return emailCb(err);
                            }
                            else {
                                if (user == false) {
                                    return emailCb({msgCode: 5021});
                                }
                                else {
                                    userObject = user;
                                    return emailCb();
                                }
                            }
                        });
                },
                (codeCb) => {
                    lib.queryAccount({
                            $or: [{mobileNo: userId}, {email: userId.toLowerCase()}],
                            verificationCode: req.body.verificationCode
                        },
                        (err, user) => {
                            if (err) {
                                return codeCb(err);
                            }
                            else {
                                if (user == false) {
                                    return codeCb({msgCode: 5022});
                                }
                                else {
                                    return codeCb();
                                }
                            }
                        });
                },
                (expireTimeCb) => {
                    lib.queryAccount({
                            $or: [{mobileNo: userId}, {email: userId.toLowerCase()}],
                            verificationCode: req.body.verificationCode,
                            verificationCodeExpirationTime: {
                                $gte: new Date()
                            },
                            isVerificationCodeExpired: false
                        },
                        (err, user) => {
                            if (err) {
                                return expireTimeCb(err);
                            }
                            else {
                                if (user == false) {
                                    return expireTimeCb({msgCode: 5023});
                                }
                                else {
                                    return expireTimeCb();
                                }
                            }
                        });
                },
                (passwdCb) => {
                    if (req.body.newPassword != req.body.confirmPassword) {
                        return passwdCb({msgCode: 5024})
                    }
                    else {
                        return passwdCb();
                    }
                }],
            (err) => {
                if (err) {
                    return next(err);
                }
                else {
                    async.parallel([
                        (expireCb) => {
                            userObject.isVerificationCodeExpired = true;
                            userObject.save((err) => {
                                if (err) {
                                    return expireCb(err);
                                }
                                return expireCb();
                            });
                        },
                        (newPassCb) => {
                            //set up new password
                            userObject.password = req.body.newPassword;
                            userObject.save((err) => {
                                if (err) {
                                    return newPassCb(err);
                                }
                                return newPassCb();
                            });
                        }
                    ], (err) => {
                        if (err) {
                            return next(err);
                        }
                        else {
                            let vars = {
                                    name: userObject.name
                                },
                                text = "Password Updated Successfully!";

                            lib.sendEmail(userObject.email, text, vars, 'reset_password', (err) => {
                                if (err) {
                                    return next(err);
                                } else {
                                    responseModule.successResponse(res, {
                                        success: 1,
                                        message: "Password updated successfully.",
                                        data: {}
                                    });
                                }
                            });
                        }
                    });
                }
            }
        );
    },
    resendVerificationCode(req, res, next) {

        let expTime = new Date();

        const userId = _.trim(req.body.userId),
            confirmationCode = randomize('0', config.confirmationCodeLength),
            confirmationCodeExpirationTime = lib.addDays(expTime, config.confirmAcctCodeExpiryMinutes);

        let updateObject = {
            verificationCode: confirmationCode,
            verificationCodeExpirationTime: confirmationCodeExpirationTime,
            isVerificationCodeExpired: false
        }

        User.findOneAndUpdate({$or: [{mobileNo: userId}, {email: userId.toLowerCase()}]}, {$set: updateObject}, {upsert: true}, (err, userFound) => {

            if (err) {
                return next(err);
            }
            let mobile = userFound.mobileNo,
                txt = 'Dear User, To verify your account , please enter the following code on the Mobile Application:';
            sendVerificationSms(mobile, updateObject.verificationCode, txt, (error, res) => {
                if (error) {
                    return next(error);
                }
                else {
                    return res;
                }
            });
            let vars = {
                    verificationCode: updateObject.verificationCode,
                    name: userFound.name
                },
                text = "Your Account Verification Code";

            lib.sendEmail(userFound.email, text, vars, 'account_verification', (err) => {
                if (err) {
                    return next(err);
                } else {
                    responseModule.successResponse(res, {
                        success: 1,
                        message: "Verification code resent successfully.",
                        data: {}
                    });
                }
            });
            /*    sendVerificationSms(923462339026, updateObject.verificationCode, text, (err) => {
             if (err) {
             return next(err);
             }
             else {
             return next();
             }
             });*/
        });
    },
    selectAddress(req, res, next){
        let userId = _.trim(req.user._id),
            addressId = _.trim(req.body.addressId);

        return User.update({
            _id: userId,
            'address.isSelected': true
        }, {$set: {'address.$.isSelected': false}}, {multi: true}).exec().then(() => {
            return User.update({
                _id: userId,
                'address._id': addressId
            }, {$set: {'address.$.isSelected': true}}).exec();
        }).then(userUpdated => {
            responseModule.successResponse(res, {
                success: 1,
                message: "User address updated successfully.",
                data: userUpdated
            });
        }).catch(err => {
            return next(err);
        });
    },
    loadUserAddresses(req, res, next){
        const userId = _.trim(req.user._id);

        return User.findOne({_id: userId}, {address: 1}).lean().then(userFound => {
            userFound.address.forEach((user) => {
                let obj = {
                    lat: user.location.coordinates[0],
                    long: user.location.coordinates[1]
                };
                user.location.coordinates = obj;
            });
            let home = null,
                work = null,
                other = null;

            userFound.address.forEach((address) => {
                if (address.alias == 'HOME') {
                    home = address;
                }
                if (address.alias == 'WORK') {
                    work = address;
                }
                if (address.alias == 'OTHER') {
                    other = address;
                }
            });
            let address = [];
            if (home) {
                address.push(home);
            }
            if (work) {
                address.push(work);
            }
            if(other){
                address.push(other);
            }
            responseModule.successResponse(res, {
                success: 1,
                message: "User address loaded successfully.",
                data: {location: address}
            });
        }).catch(err => {
            return next(err);
        })
    },
    deleteAddress(req, res, next){
        const userId = _.trim(req.user._id),
            addressId = _.trim(req.body.addressId);

        return User.findOneAndUpdate({_id: userId}, {$pull: {address: addressId}}).then(userFound => {
            responseModule.successResponse(res, {
                success: 1,
                message: "User address deleted successfully.",
                data: {}
            });
        }).catch(err => {
            return next(err);
        })
    },
    mediaUploaded(req, res, next){
        try {
            multer.resizeAndUpload(req.files[0].location, req.files[0].key.split('1000X1000/')[1], 2);
            return responseModule.successResponse(res, {
                success: 1,
                message: "",
                data: {url: req.files[0].location}
            });
        }
        catch (err) {
            winston.error(err);
            return next(err);
        }
    },

    addKpo(req, res, next){
        var name = _.trim(req.body.kpo.name),
            email = _.trim(req.body.kpo.email),
            password = _.trim(req.body.kpo.password),
            userType = _.trim(req.body.kpo.userType),
            mobileNo = req.body.kpo.mobileNo;

        User.findOne({email: email, status : 1}).then(userFound => {
            if (!userFound) {
                let createUserObject = {
                    name: name,
                    email: email,
                    password: password,
                    mobileNo : mobileNo,
                    status : 1,
                    userType : userType,
                    isNumberVerified: true
                };

                return new User(createUserObject).save().then(userCreated => {
                    responseModule.successResponse(res, {
                        success: 1,
                        message: "Kpo created  successfully.",
                        data: "Kpo created  successfully."
                    });
                }).catch(err => {
                    responseModule.errorResponse(res, {
                        success: 0,
                        message: "Error.",
                        data: err
                    });
                });

            }
            else{
                responseModule.errorResponse(res, {
                    success: 0,
                    message: "User already exist with same email.",
                    data: "User already exist with same email."
                });
            }

        });
    },

    getKpo(req, res, next){
        const offset = parseInt(req.params.offset) || 0,
            limit = parseInt(req.params.limit) || 10,
            searchText = _.trim(req.body.searchText) || "";

        let searchKpo = {status : 1, userType : { $in : ['KPO', 'MANAGER']}};

        if (searchText) {
            searchKpo.name = {'$regex': searchText, '$options': 'i'};
        }

        let countKpo = 0;

        return User.count(searchKpo).then(kpoCount => {
            countKpo = kpoCount;

            return User.find(searchKpo).skip(offset).limit(limit).sort({'createdAt': '-1'})
        }).then(kpoFound => {

            return responseModule.successResponse(res, {
                success: 1,
                message: "Kpo fetched successfully.",
                data: {kpo: kpoFound, count: countKpo}
            });


        }).catch(err => {
            return next(err);
        });
    },

    editKpo(req, res, next){
        var name = _.trim(req.body.kpo.name),
            _id  = req.body.kpo._id,
            email = _.trim(req.body.kpo.email),
            status = _.trim(req.body.kpo.status),
            userType = _.trim(req.body.kpo.userType),
            mobileNo = req.body.kpo.mobileNo;

        let resultObject = {
            success: 1,
            message: "",
            data: ""
        };

        if (status == 0){
            return User.remove({ _id : _id }).exec().then(kpoCreated => {
                resultObject.message = "kpo Deleted successfully.";
                resultObject.data = kpoCreated;

                responseModule.successResponse(res, resultObject);
            }).catch(err => {
                return next(err);
            });
        }
        else {
            let createKpoObject = {
                name: name,
                email: email,
                userType: userType,
                mobileNo : mobileNo,
                status : status,
                isNumberVerified: true
            };

            return User.findOneAndUpdate({ _id : _id }, {$set: createKpoObject}, {new: true}).exec().then(kpoCreated => {
                resultObject.message = "kpo updated successfully.";
                resultObject.data = kpoCreated;

                responseModule.successResponse(res, resultObject);
            }).catch(err => {
                return next(err);
            });
        }
    },

    getAnalyticsCount(req, res, next){
        async.parallel({
            driversCount : function(callback) {
                _driver.find({ status : true }).count().then(drivers =>{
                    callback(null, drivers);
                });
            },
            invoicesCount : function(callback) {
                _invoices.find({ status : true }).count().then(invoices =>{
                    callback(null, invoices);
                });
            },
            vehiclesCount : function(callback) {
                _vehicles.find({ status : true }).count().then(vehicles =>{
                    callback(null, vehicles);
                });
            },
            companiesCount : function(callback) {
                _companies.find({ status : true }).count().then(companies =>{
                    callback(null, companies);
                });
            }
        }, function(err, results) {
            if (results){
                return responseModule.successResponse(res, {
                    success: 1,
                    message: "Analytics fetched successfully.",
                    data: results
                });
            }
        });
    }
};

