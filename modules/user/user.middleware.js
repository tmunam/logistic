/**
 * Created by Asif on 8/4/2017.
 */


const winston = require('winston');

let validateSignUpParams = (req, res, next) => {

    req.body.email = req.body.email.toLowerCase();

    req.assert('name', 5003).notEmpty();
    req.assert('email', 5000).notEmpty();
    req.assert('email', 5001).isEmail();
    req.assert('password', 5002).notEmpty();
    req.assert('password', 5004)
        .isPasswordValid();
    req.assert('mobileNo', 5005).notEmpty();
    req.assert('mobileNo', 5006).isMobileNumberValid();

    //req.assert('latLong', 'Please enter latitude and longitude.').notEmpty();
    //req.assert('latLong', 'Please enter valid latitude and longitude e.g. 1.0000,-2.000.').isLatLongValid();

    //req.assert('addressType', 'Please select address type').notEmpty();
    //req.assert('addressName', 'Please enter address').notEmpty();

    //if (req.body.addressType != config.addressTypes[0] && req.body.addressType != config.addressTypes[1] && req.body.addressType != config.addressTypes[2]) {
    //    return next({message: 'Type must be Home, Work or Other.'});
    //}

    const errors = req.validationErrors();

    if (errors) {
        winston.error('User could not be signed up', errors[0]);
        return next(errors[0]);
    }
    return next();
}

let validateVerifyAccountParams = (req, res, next) => {


    req.assert('email', 5000).notEmpty();
    req.assert('verificationCode', 5016).notEmpty();

    req.body.email = req.body.email.toLowerCase();

    const errors = req.validationErrors();

    if (errors) {
        winston.error('user could not perform confirm account action.', errors[0]);
        return next(errors[0]);
    }

    return next();
}

let validateLogInParams = (req, res, next) => {

    req
        .assert('userId', 5007)
        .notEmpty();

    req
        .assert('password', 5002)
        .notEmpty();

    if (req.body.userId)
        req.body.userId = req.body.userId.toLowerCase();

    const errors = req.validationErrors();

    if (errors) {
        winston.error('user could not be logged in', errors[0]);
        return next(errors[0]);
    }

    return next();
}

let addAddressParams = (req, res, next) => {

    req.assert('address', 5035).notEmpty();
    //req.assert('latitude', 5047).notEmpty();
    //req.assert('longitude', 5048).notEmpty();
    req.assert('alias', 5049).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        winston.error('User address params not provided.', errors[0]);
        return next(errors[0]);
    }

    return next();
}

let editAddressParams = (req, res, next) => {

    req.assert('addressId', 5050);

    const errors = req.validationErrors();

    if (errors) {
        winston.error('User address params not provided.', errors[0]);
        return next(errors[0]);
    }
    return next();
}

let forgetPasswordValidate = (req, res, next) => {

    req
        .assert('userId', 5007)
        .notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        winston.error('user could not perform forget password action.', errors[0]);
        return next(errors[0]);
    }

    return next();
}

let resetPasswordValidate = (req, res, next) => {
    req
        .assert('userId', 5007)
        .notEmpty();
    req
        .assert('verificationCode', 5016)
        .notEmpty();
    req
        .assert('newPassword', 5002)
        .notEmpty();
    req
        .assert('newPassword', 5004)
        .isPasswordValid();
    req
        .assert('confirmPassword', 5002)
        .notEmpty();
    req
        .assert('confirmPassword', 5004)
        .isPasswordValid();

    const errors = req.validationErrors();

    if (errors) {
        winston.error('user could not perform reset password action.', errors[0]);
        return next(errors[0]);
    }

    return next();
}

module.exports = {
    validateSignUpParams,
    validateVerifyAccountParams,
    validateLogInParams,
    addAddressParams,
    editAddressParams,
    forgetPasswordValidate,
    resetPasswordValidate
}