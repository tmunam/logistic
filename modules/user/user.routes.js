const userController = require('./user.controller'),
    passport = require('../../config/passport'),
    userMiddleware = require('./user.middleware'),
    multer = require('../../config/multer');

let imageUpload = multer.upload(config.aws.s3.profileImageDirectory),
    notifications = require('../../config/notifications');

module.exports = function (app, version) {
    app.post(version + '/user/signUp', userMiddleware.validateSignUpParams, userController.sendSignUpVerification);
    app.post(version + '/user/verifyAccount', userMiddleware.validateVerifyAccountParams, userController.verifyAccount);
    app.post(version + '/user/logIn', userMiddleware.validateLogInParams, userController.logInAccount);
    app.post(version + '/user/logOut', passport.isAuthenticated, userController.logOutAccount);
    app.get(version + '/user/logout', passport.isAuthenticated, userController.logOutAccount);
    app.get(version + '/user/currentAccount', passport.isAuthenticated, userController.getCurrentAccount);
    app.get(version + '/user/loadProfile', passport.isAuthenticated, userController.loadProfileData);
    app.post(version + '/user/editProfile', passport.isAuthenticated, userController.editProfile);
    app.post(version + '/user/addAddress', passport.isAuthenticated, userMiddleware.addAddressParams, userController.addAddress);
    app.post(version + '/user/editAddress', passport.isAuthenticated, userMiddleware.editAddressParams, userController.editAddress);
    app.post(version + '/user/selectAddress', passport.isAuthenticated, userMiddleware.editAddressParams, userController.selectAddress);
    app.post(version + '/user/forgetPassword', userMiddleware.forgetPasswordValidate, userController.forgetPassword);
    app.post(version + '/user/resetPassword', userMiddleware.resetPasswordValidate, userController.resetPassword);
    app.post(version + '/user/resendConfirmationCode', userMiddleware.forgetPasswordValidate, userController.resendVerificationCode);
    app.get(version + '/user/loadUserAddresses', passport.isAuthenticated, userController.loadUserAddresses);
    app.get(version + '/user/deleteAddress', passport.isAuthenticated, userMiddleware.editAddressParams ,userController.deleteAddress);
    app.post(version + '/user/uploadImage', passport.isAuthenticated, imageUpload.array('image', 1), userController.mediaUploaded);
    //kpo
    app.get(version + '/user/getKpo', passport.isAuthenticated, userController.getKpo);
    app.post(version + '/user/addKpo', passport.isAuthenticated, userController.addKpo);
    app.post(version + '/user/editKpo', passport.isAuthenticated, userController.editKpo);

    // analytics
    app.get(version + '/user/getAnalyticsCount', passport.isAuthenticated, userController.getAnalyticsCount);

    app.get(version + '/testPush', notifications.testPush)

};