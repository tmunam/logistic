/**
 * Created by Raza on 6/23/2017.
 */
var mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp');

var Schema = mongoose.Schema;

var verificationSchema = new Schema({
    name: {type: String},
    email: {type: String, required: true, index: {unique: true}},
    mobileNumber: String,
    verificationCode: String,
    verificationCodeExpirationTime: Date,
    isAccountVerified: {type: Boolean, default: false},
    userType: {type: String, default: "USER", enum: config.userTypes},
});

verificationSchema.plugin(timestamps);
verificationSchema.index({email: 1}, {background: true, name: 'IDX_VERIFICATION_EMAIL'});

module.exports = mongoose.model('AccountVerification', verificationSchema);
