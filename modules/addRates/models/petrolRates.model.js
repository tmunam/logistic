
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let petrolRateSchema = new Schema({
    current_rate : { type : Number },
    old_rate : { type : Number },
    effect_from : { type : Date }
});

petrolRateSchema.plugin(timestamps);

module.exports = mongoose.model('petrolRate', petrolRateSchema);
