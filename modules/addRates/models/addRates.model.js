
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let addRatesSchema = new Schema({
    company_id : { type : Schema.Types.ObjectId },
    company_name : { type : String },
    destination_id : { type : Schema.Types.ObjectId },
    destination_name : { type : String },
    rate : { type : Number },
    old_rate : { type : Number }
});

addRatesSchema.plugin(timestamps);

module.exports = mongoose.model('addrates', addRatesSchema);
