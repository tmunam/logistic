

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    _addRates = mongoose.model('addrates'),
    _invoices = mongoose.model('invoices'),
    _destinations = mongoose.model('destinations'),
    _petrolRate = mongoose.model('petrolRate'),
    uuidv4 = require('uuid/v4'),
    xlsx = require('xlsx');
    responseModule = require('../../config/response');


let getRates = (req, res, next) => {

    const offset = parseInt(req.body.offset) || 0,
        limit = parseInt(req.body.limit) || 10,
        searchTerm = _.trim(req.body.searchTerm) || "";

    let searchRates = {  };

    if (searchTerm) {
        searchRates = { $or : [{destination_name : {'$regex': searchTerm, '$options': 'i'} },
                {company_name : {'$regex': searchTerm, '$options': 'i'}}] };
    }

    let countRates = 0;

    return _addRates.count(searchRates).then(invoicesCount => {
        countRates = invoicesCount;

        return _addRates.find(searchRates).skip(offset).limit(limit).sort({'createdAt': '-1'}).then(ratesFound => {
            return ratesFound;
        });
    }).then(ratesFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "invoices fetched successfully.",
            data: { rates: ratesFound, count: countRates }
        });


    }).catch(err => {
        return next(err);
    });

};

let addRates = (req, res, next) => {

    const company_id = (req.body.rates.company._id),
        company_name = (req.body.rates.company.company_name),
        destination_id = (req.body.rates.destination._id),
        destination_name = (req.body.rates.destination.destination_name),
        rate = req.body.rates.rate,
        old_rate = req.body.rates.old_rate || null;

    let addRateObject = {
        company_id : company_id,
        company_name : company_name,
        destination_id : destination_id,
        destination_name : destination_name,
        rate : rate,
        old_rate : old_rate
    };

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    let addRateCreateObject = new _addRates(addRateObject);
    return addRateCreateObject.save().then(rateCreated => {

        resultObject.message = "Rate created successfully.";
        resultObject.data = rateCreated;

        responseModule.successResponse(res, resultObject);

    }).catch(err => {
        return next(err);
    });
};

let editRates = (req, res, next) => {
    const company_id = (req.body.rates.company._id),
        _id = (req.body.rates._id),
        company_name = (req.body.rates.company.company_name),
        destination_id = (req.body.rates.destination._id),
        destination_name = (req.body.rates.destination.destination_name),
        rate = req.body.rates.rate,
        old_rate = req.body.rates.old_rate || null;

    let addRateObject = {
        company_id : company_id,
        company_name : company_name,
        destination_id : destination_id,
        destination_name : destination_name,
        rate : rate,
        old_rate : old_rate
    };

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    _addRates.findOneAndUpdate({ _id : _id },{ $set : addRateObject },{new: true}).exec().then(rateUpdated => {
        resultObject.message = "Rates updated successfully.";
        resultObject.data = rateUpdated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
};

let uploadFile = (req, res, next) => {
    //console.log((JSON.stringify(req.body, null, 4)));
    var companyId = req.body.company,
        comapnyName = req.body.company_name,
        oldFuelRate = req.body.oldFuelRate,
        newFuelRate = req.body.newFuelRate,
        effectFrom = req.body.effectFrom;
    var workbook = xlsx.readFile(req.file.path);
    var sheetList = workbook.SheetNames;
    var ratesList = (xlsx.utils.sheet_to_json(workbook.Sheets[sheetList[0]]));

    async.series([
        function(callback) {
            async.eachOfSeries(ratesList, function(row, index, cb) {
                _addRates.findOne({company_id : companyId, destination_name : row.TOWN}).then(tempRate => {
                    if (tempRate){
                        tempRate.rate = row.REVISED;
                        tempRate.old_rate = row.EXISTING;
                        tempRate.save();
                        cb();
                    }
                    else{
                        var destinationObject = {
                            destination_name: row.TOWN,
                            distance : null,
                            status : true
                        };
                        var destinationCreateObject = new _destinations(destinationObject);
                        destinationCreateObject.save().then(destinationCreated => {
                            var obj = {
                                company_id : companyId,
                                company_name : comapnyName,
                                destination_id : destinationCreated._id,
                                destination_name : destinationCreated.destination_name,
                                rate : row.REVISED,
                                old_rate : row.EXISTING
                            };
                            var newRateInstance = new _addRates(obj);
                            newRateInstance.save().then(newlyAddedRate => {
                                cb();
                            });
                        });
                    }
                });
            }, function(err){
                if (err) {
                    console.log(err);
                    responseModule.errorResponse(res, {
                        success: 0,
                        message: "Error.",
                        data: err
                    });
                }
                else{
                    callback(null, 'one');
                }
            });
        },
        function(callback) {
            _invoices.find({createdAt : {$gte : effectFrom}}).then(invoices => {
                if (invoices && invoices.length > 0){
                    async.each(invoices, function(inv, cb) {
                        if (inv && inv.dcn_list.length > 0){
                            async.each(inv.dcn_list, function (dcn, dcnCb){
                                _addRates.findOne({destination_id : inv.destination}).then(rate => {
                                    dcn.bill = dcn.quantity * rate.rate;
                                    dcnCb();
                                });
                            }, function (err){
                                if( err ) {
                                    console.log(err);
                                    responseModule.errorResponse(res, {
                                        success: 0,
                                        message: "Error.",
                                        data: err
                                    });
                                }
                                inv.total_bill = inv.dcn_list.map(item => item.bill).reduce((prev, next) => prev + next);
                                inv.save().then(updatedInv => {
                                    cb();
                                });
                            })
                        }
                        else{
                            cb();
                        }
                    }, function(err) {
                        if( err ) {
                            console.log(err);
                            responseModule.errorResponse(res, {
                                success: 0,
                                message: "Error.",
                                data: err
                            });
                        } else {
                            callback(null, 'two');
                        }
                    });
                }
                else{
                    callback(null, 'two');
                }
            });
        },
        function (callback){
            var petrolRateObj = {
                current_rate : newFuelRate,
                old_rate : oldFuelRate,
                effect_from : effectFrom
            };
            var petrolRateInstance = new _petrolRate(petrolRateObj);
            petrolRateInstance.save().then(savedRate => {
                callback(null, 'three');
            });
        }
        ], function(err, results) {
        if (err){
            console.log(err);
            responseModule.errorResponse(res, {
                success: 0,
                message: "Error.",
                data: err
            });
        }
        console.log('done');
        let resultObject = {
            success: 1,
            message: "",
            data: ""
        };
        responseModule.successResponse(res, resultObject);
    });

};

module.exports = {
    getRates,
    addRates,
    editRates,
    uploadFile
};
