const addRatesMiddleware = require('./addRates.middleware.js'),
    addRatesController = require('./addRates.controller.js'),
    passport = require('../../config/passport');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' +file.originalname)
    }
});
var upload = multer({storage: storage});

module.exports = function (app, version) {

    app.post(version + '/rates/getRates', passport.isAuthenticated, addRatesController.getRates);
    app.post(version + '/rates/addRates', passport.isAuthenticated , addRatesController.addRates);
    app.post(version + '/rates/editRates', passport.isAuthenticated , addRatesController.editRates);
    app.post(version + '/file/rates/updatRates', passport.isAuthenticated, upload.single('photo'), addRatesController.uploadFile);
};