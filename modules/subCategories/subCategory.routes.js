/**
 * Created by Asif on 8/4/2017.
 */

const subCategoryMiddleware = require('./subCategory.middleware.js'),
    subCategoryController = require('./subCategory.controller.js'),
    passport = require('../../config/passport'),
    multer = require('../../config/multer');

let imageUpload = multer.upload(config.aws.s3.subCategoryDirectory);

module.exports = function (app, version) {

    app.get(version + '/subCategories/getAllSubCategories/:categoryId/:offset/:limit', subCategoryMiddleware.checkSubCategoryId,subCategoryController.getAllSubCategories);

    app.post(version + '/subCategories/getAllSubCategories/:categoryId/:offset/:limit',passport.isAuthorized('ADMIN') ,subCategoryMiddleware.checkSubCategoryId,subCategoryController.getAllSubCategoriesAdmin);

    app.post(version + '/subCategories/category', passport.isAuthenticated,passport.isAuthorized('ADMIN'), subCategoryMiddleware.createSubCategoryParams, subCategoryController.addSubCategoryAsync);

    app.post(version + '/subCategories/category/edit', passport.isAuthenticated,passport.isAuthorized('ADMIN'), subCategoryMiddleware.editSubCategoryParams, subCategoryController.editSubCategoryAsync);

    app.post(version + '/subCategories/uploadImage', passport.isAuthenticated,passport.isAuthorized('ADMIN'), imageUpload.array('image', 1), subCategoryController.mediaUploaded);
};