/**
 * Created by Asif on 8/5/2017.
 */

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    category = mongoose.model('categories'),
    subCategory = mongoose.model('subCategories'),
    responseModule = require('../../config/response');

let getAllSubCategories = (req, res, next) => {

    const limit = parseInt(req.params.limit) || 1000,
        offset = parseInt(req.params.offset) || 0,
        categoryId = _.trim(req.params.categoryId);

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return subCategory.find({category: categoryId}).skip(offset).limit(limit).then(subCategories => {
        if (subCategories && subCategories.length) {
            resultObject.data = {subCategories: subCategories};
            resultObject.message = 'subCategories fetched successfully.';
        }
        else {
            resultObject.data = {subCategories: []};
            resultObject.message = 'No sub categories found.'
        }

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        winston.error(err);
        return next(err);
    });
}

let getAllSubCategoriesAdmin = (req, res, next) => {

    const limit = parseInt(req.params.limit) || 1000,
        offset = parseInt(req.params.offset) || 0,
        categoryId = _.trim(req.params.categoryId),
        searchText = _.trim(req.body.searchText) || "";

    let subCategoryCount = 0;

    let queryObject = {
        category: categoryId,
        name: ""
    }

    if (searchText) {
        queryObject.name = {'$regex': searchText, '$options': 'i'};
    }
    else {
        delete queryObject.name;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return subCategory.count(queryObject).then(count => {
        subCategoryCount = count;
        return subCategory.find(queryObject).skip(offset).limit(limit).sort({'createdAt': '-1'});
    }).then(subCategories => {
        if (subCategories && subCategories.length) {
            resultObject.data = {subCategories: subCategories, subCategoryCount: subCategoryCount};
            resultObject.message = 'subCategories fetched successfully.';
        }
        else {
            resultObject.data = {subCategories: [], subCategoryCount: 0};
            resultObject.message = 'No sub categories found.'
        }

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        winston.error(err);
        return next(err);
    });
}


let addSubCategoryAsync = (req, res, next) => {

    let resultObject = {
            success: 1,
            message: "",
            data: ""
        },
        thisObject = {};

    const name = _.startCase(_.trim(req.body.name)),
        description = _.trim(req.body.description),
        image = _.trim(req.body.image) || "",
        categoryId = _.trim(req.body.categoryId);

    const subCategoryObject = {
        name: name,
        description: description,
        image: image,
        category: categoryId
    }

    return category.findOne({_id: categoryId}, {
        name: 0,
        description: 0,
        status: 0,
        image: 0,
        subCategory: 0
    }).then(categoryFound => {
        if (!categoryFound) {
            throw {msgCode: 7004};
        }
        else {
            thisObject.categoryFound = categoryFound;

            return subCategory.findOne({category: categoryId, name: name}, {
                name: 0,
                description: 0,
                status: 0,
                image: 0,
                category: 0
            });
        }
    }).then(subCategoryFound => {

        if (subCategoryFound) {
            throw {msgCode: 7005};
        }

        let subCategoryCreate = new subCategory(subCategoryObject);

        return subCategoryCreate.save();

    }).then(subCategoryCreated => {
        category.update({_id: categoryId}, {$push: {subCategory: subCategoryCreated._id}}).exec();

        resultObject.message = "Sub category created successfully.";

        resultObject.data = subCategoryCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

let editSubCategoryAsync = (req, res, next) => {

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    const name = _.startCase(_.trim(req.body.name)),
        description = _.trim(req.body.description) || "",
        image = _.trim(req.body.image),
        status = parseInt(req.body.status),
        categoryId = _.trim(req.body.categoryId),
        subCategoryId = _.trim(req.body.subCategoryId);

    const subCategoryObject = {
        name: "",
        description: "",
        image: "",
        status: 0,
        category: ""
    }

    if (name) {
        subCategoryObject.name = name;
    }
    else {
        delete subCategoryObject.name;
    }

    if (req.body.hasOwnProperty('description')) {
        subCategoryObject.description = description;
    }
    else {
        delete subCategoryObject.description;
    }

    if (image) {
        subCategoryObject.image = image;
    }
    else {
        delete subCategoryObject.image;
    }

    if (status == 1) {
        subCategoryObject.status = status;
    }
    else if (status == 0) {
        subCategoryObject.status = status;
    }
    else {
        delete subCategoryObject.status;
    }

    if (categoryId) {
        subCategoryObject.category = categoryId;
    }
    else {
        delete subCategoryObject.category;
    }

    return subCategory.findOne({name: name, category: categoryId}).then(subCategoryFound => {
        if (subCategoryFound) {
            throw {msgCode: 7005};
        }
        else {

            if (categoryId) {

                subCategory.findOne({_id: subCategoryId}).then(subCategorySingle => {

                    if (categoryId && subCategorySingle.category) {

                        category.update({_id: subCategorySingle.category}, {$pull: {subCategory: subCategorySingle._id}}).exec().then(() => {

                            category.update({_id: categoryId}, {$push: {subCategory: subCategorySingle._id}}).exec();
                        });
                    }
                });
            }

            return subCategory.update({_id: subCategoryId}, {$set: subCategoryObject}, {new: true});
        }
    }).then(subCategoryEdited => {
        resultObject.message = "Sub category updated successfully.";
        resultObject.data = subCategoryEdited;

        responseModule.successResponse(res, resultObject);


    }).catch(err => {
        return next(err);
    });
}

let mediaUploaded = (req, res, next) => {
    try {
        return responseModule.successResponse(res, {
            success: 1,
            message: "",
            data: {url: req.files[0].location}
        });
    }
    catch (err) {
        winston.error(err);
        return next(err);
    }
}

module.exports = {
    getAllSubCategories,
    addSubCategoryAsync,
    editSubCategoryAsync,
    mediaUploaded,
    getAllSubCategoriesAdmin
};