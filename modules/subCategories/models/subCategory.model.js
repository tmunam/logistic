/**
 * Created by Asif on 8/4/2017.
 */
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let subCategorySchema = new Schema({
    name: {type: String, required: true},
    description: {type: String},
    status: {type: Boolean, default: true},
    image: {type: String, default: "https://s3-us-west-2.amazonaws.com/raft/assets/default-subcategory-image.png"},
    category: {type: Schema.Types.ObjectId, ref: 'categories'}
});

subCategorySchema.plugin(timestamps);
subCategorySchema.index({name: 1}, {background: true, name: 'IDX_SUBCATEGORY_NAME'});

module.exports = mongoose.model('subCategories', subCategorySchema);