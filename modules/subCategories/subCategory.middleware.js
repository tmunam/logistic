/**
 * Created by Asif on 8/4/2017.
 */


const winston = require('winston');

let createSubCategoryParams = (req, res, next) => {

    req.assert('name', 7000).notEmpty();
    //req.assert('image', 7001).notEmpty();
    req.assert('categoryId', 7002).notEmpty();
    req.assert('categoryId', 7007).isValidObjectId();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let editSubCategoryParams = (req, res, next) => {

    req.assert('subCategoryId', 7003).notEmpty();
    req.assert('categoryId', 7002).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let checkSubCategoryId = (req, res, next) => {

    req.assert('categoryId', 7002).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

module.exports = {
    createSubCategoryParams,
    editSubCategoryParams,
    checkSubCategoryId
}