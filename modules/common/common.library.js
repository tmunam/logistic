const mongoose = require('mongoose');
const User = mongoose.model('User');
const _ = require('lodash');
const countryCities = require('countries-cities');
const AccountVerification = mongoose.model('AccountVerification');
const moment = require('moment'),
    mailer = require('../../config/mailer');

module.exports = {
    createAccountObjectForResponse(acct) {
        return {
            _id: acct._id,
            name: acct.name,
            email: acct.email,
            mobileNo: acct.mobileNo,
            status: acct.status,
            profileImage:acct.profileImage,
            isNumberVerified: acct.isNumberVerified,
            userType : acct.userType
        };
    },
    queryAccount(queryObject, cb) {
        User.findOne(queryObject, (err, acctObject) => {
            if (err) {
                return cb(err);
            }
            else {
                if (!acctObject) {
                    return cb(null, false);
                }
                else {
                    return cb(null, acctObject);
                }
            }
        });
    },
    checkLimitValue(limit) {
        if (parseInt(limit) <= 0) {
            return false;
        }
        return true;
    },
    isUserIdMatchWithSession (user, id) {
        if (user._id != id.toString()) {
            return false;
        }
        else {
            return true;
        }
    },
    queryVerificationAccount(queryObject, cb) {
        AccountVerification.findOne(queryObject, (err, acctObject) => {
            if (err) {
                return cb(err);
            }
            else {
                if (!acctObject) {
                    return cb(null, false);
                }
                else {
                    return cb(null, acctObject);
                }
            }
        });
    },
    dateFormat(date) {
        if (date) {
            return moment(date).format("DD MMM, kk:mm");
        } else return date;
    },
    sendEmail(email, body, vars, template, cb){
        mailer.sendEmailer(email, body, vars, template, (err) => {
            if (err) {
                return cb(err);
            } else {
                return cb();
            }
        });
    },
    addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    },
    checkForValidObjectId(value){
        let checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");

        if (checkForHexRegExp.test(value)) {
            return true;
        }
        else {
            return false;
        }
    },formatDate(date) {

        let createdAtDate = date;

        if (createdAtDate) {
            createdAtDate = moment(createdAtDate, 'DD/MM/YYYY').format("DD MMM, YYYY");
        }

        return createdAtDate;
    }
};