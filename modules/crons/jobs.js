/**
 * Created by Asif on 8/31/2017.
 */
const mongoose = require('mongoose'),
    _promotions = mongoose.model('promotions'),
    _products = mongoose.model('products'),
    _orders = mongoose.model('orders'),
    promoCodes = mongoose.model('promoCodes'),
    async = require('async'),
    User = mongoose.model('User'),
    winston = require('winston'),
    _ = require('lodash'),
    sms = require('../../config/sms'),
    moment = require("moment");

winston.info('Jobs ready to Run');

function sendVerificationSms(number, verificationCode, text, cb) {
    let smsObject = {
        to: number,
        text: text + ' is: ' + verificationCode,
    };

    sms.sendMessage(smsObject, (err) => {
        if (err) {
            return cb(err);
        }
        else {
            return cb();
        }
    });
}

let checkIfAnyPromotionHasExpired = () => {

    winston.info("CRON JOB TRIGGERED");
    winston.info("Promotion has Expired JOB TRIGGERED");

    let currentDate = Math.round(new Date().getTime() / 1000);
    let promotionIds = [],
        productIds = [];

    return _promotions.find({status: false, isDateRange: true, endDate: {$lt: currentDate}}, {
        products: 1,
        discountPercentage: 1
    }).then(promotions => {

        if (promotions && promotions.length) {
            _.forEach(promotions, (promotionObject) => {

                promotionIds.push(promotionObject._id);

                productIds.push.apply(productIds, promotionObject.products);
            });

            winston.info(productIds.length + " promotions have been expired.");
            winston.info(productIds.length + " products removing from promotions.");

            return _products.update({_id: {$in: productIds}}, {
                $set: {
                    isDiscounted: false,
                    discountPrice: 0,
                    discountPercentage: 0,
                    discountedPrice: 0
                }
            }, {multi: true});
        }
        else {
            throw "No promotions found."
        }
    }).then(productsUpdated => {

        return _promotions.update({_id: {$in: promotionIds}}, {$set: {status: true}});
    }).catch(err => {
        winston.error(err);
    });
}

let checkIfOrderCancelTimeHasExpired = () => {
    winston.info('Orders Jobs ready to Run');

    winston.info("CRON JOB TRIGGERED");

    let currentDate = new Date();
    currentDate.setMinutes(currentDate.getMinutes());
    return _orders.update({
        isCancelledEnable: true,
        orderCancelTime: {$lt: currentDate}
    }, {$set: {isCancelledEnable: false}}, {multi: true}).then(orders => {
    }).catch(err => {
        winston.error(err);
    });
}

module.exports = {
    checkIfAnyPromotionHasExpired,
    checkIfOrderCancelTimeHasExpired
};