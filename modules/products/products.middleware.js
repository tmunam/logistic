/**
 * Created by Asif on 8/5/2017.
 */

const winston = require('winston');


let convertImagesToArray = (req) => {
    if (req.body.images) {
        req.body.images = req.body.images.toString().split(',').map(function (item) {
            return item.trim();
        });
    }
}

let checkProductsParams = (req, res, next) => {

    req.assert('productName', 8000).notEmpty();
    req.assert('price', 8001).notEmpty();
    req.assert('unit', 8002).notEmpty();
    req.assert('categoryId', 8007).notEmpty();
    req.assert('categoryId', 8007).isValidObjectId();

    convertImagesToArray(req);

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let checkProductId = (req, res, next) => {

    req.assert('productId', 8004).notEmpty();
    req.assert('productId', 8004).isValidObjectId();

    convertImagesToArray(req);

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let convertIdsToArray = (req, res, next) => {

    req.assert('products', 8009).notEmpty();

    if (req.body.products) {
        req.body.products = req.body.products.toString().split(',').map(function(item) {
            return item.trim();
        });
        return next();
    }

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let convertKeywordIdsToArray = (req, res, next) => {

    if (req.body.keywords) {
        req.body.keywords = req.body.keywords.toString().split(',').map(function(item) {
            return item.trim();
        });
        return next();
    }
    else{
        return next();
    }
}


module.exports = {
    checkProductsParams,
    checkProductId,
    convertIdsToArray,
    convertKeywordIdsToArray
}