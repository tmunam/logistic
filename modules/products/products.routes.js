/**
 * Created by Asif on 8/5/2017.
 */



const productsMiddleware = require('./products.middleware.js'),
    productsController = require('./products.controller.js'),
    passport = require('../../config/passport'),
    multer = require('../../config/multer');

let imageUpload = multer.upload(config.aws.s3.productDirectory);

module.exports = function (app, version) {

    app.post(version + '/products/getAllProducts/:offset/:limit', productsController.getAllProducts);

    app.post(version + '/products/getAllProductsAdmin/:offset/:limit',passport.isAuthorized('ADMIN'), productsController.getAllProductsAdmin);

    app.post(version + '/products/product', passport.isAuthenticated, passport.isAuthorized('ADMIN'), productsMiddleware.checkProductsParams, productsMiddleware.convertKeywordIdsToArray, productsController.addProduct);

    app.post(version + '/products/product/edit', passport.isAuthenticated, passport.isAuthorized('ADMIN'), productsMiddleware.checkProductId, productsMiddleware.convertKeywordIdsToArray, productsController.editProduct);

    app.get(version + '/products/productId/:productId', productsMiddleware.checkProductId, productsController.getProductDetail);

    app.post(version + '/products/uploadImage', passport.isAuthenticated,passport.isAuthorized('ADMIN'), imageUpload.array('image', 1), productsController.mediaUploaded);

    app.post(version + '/products/getProductsByIds', productsMiddleware.convertIdsToArray ,productsController.getProductsByIds);

};