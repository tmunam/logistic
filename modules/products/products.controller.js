/**
 * Created by Asif on 8/5/2017.
 */


const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _products = mongoose.model('products'),
    _categories = mongoose.model('categories'),
    _subCategories = mongoose.model('subCategories'),
    responseModule = require('../../config/response'),
    multer = require('../../config/multer'),
    async = require('async'),
    checkValidObjectId = require('../common/common.library').checkForValidObjectId,
    _promotions = mongoose.model('promotions')

let getAllProducts = (req, res, next) => {

    let searchProduct = _.trim(req.body.searchText) || "",
        categoryId = _.trim(req.body.categoryId) || "",
        subCategoryId = _.trim(req.body.subCategoryId) || "",
        offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 20,
        sortByPrice = parseInt(req.body.sortByPrice) || 0,
        sortByName = parseInt(req.body.sortByName) || 0,
        maxPrice = parseInt(req.body.maxPrice) || 0,
        minPrice = parseInt(req.body.minPrice) || 0,
        promotionId = _.trim(req.body.promotionId) || "";

    let sortObject = {};

    if (sortByPrice) {
        sortObject.price = -1;
    }

    if (sortByName) {
        sortObject.name = -1;
    }
    else {
        sortObject.name = 1;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    let queryObject = {
        name: "",
        category: "",
        subCategory: "",
        status: false
    }

    if (searchProduct) {
        queryObject.name = {'$regex': searchProduct, '$options': 'i'};
    }
    else {
        delete queryObject.name;
    }

    if (categoryId) {
        let flag = false;
        categoryId = categoryId.toString().split(',').map(function (item) {

            if (!checkValidObjectId(item)) {
                flag = true;
            }

            return item.trim();
        });

        if (flag) {
            return next({msgCode: 8010});
        }

        queryObject.category = {$in: categoryId};
    }
    else {
        delete queryObject.category;
    }

    if (subCategoryId) {
        queryObject.subCategory = subCategoryId;
    }
    else {
        delete queryObject.subCategory;
    }

    if (maxPrice && minPrice) {
        queryObject.$and = [{price: {$lte: maxPrice}}, {price: {$gte: minPrice}}];
    }
    else if (maxPrice) {
        queryObject.price = {$lte: maxPrice};
    }
    else if (minPrice) {
        queryObject.price = {$gte: minPrice};
    }
    if (promotionId) {
        return _promotions.findOne({_id: promotionId}).populate({
            path: "products",
            model: "products",
            select: "_id name price unit description category discountPercentage discountedPrice subCategory discountPrice isDiscounted status images"
        }).then(promotionFound => {
            if (promotionFound) {
                return responseModule.successResponse(res, {
                    success: 1,
                    message: "Promotion with Product loaded.",
                    data: {products: promotionFound.products}
                });
            } else {
                 throw {msgCode: 8015};
            }
            }).catch(err => {
            return next(err);
        });
    }
    else {
        return _products.find(queryObject).sort(sortObject).skip(offset).limit(limit).then(productsArray => {
            if (productsArray && productsArray.length) {
                resultObject.message = "Products fetched successfully.";
                resultObject.data = {products: productsArray};
            }
            else {
                resultObject.message = "No products found.";
                resultObject.data = {products: []};
            }

            responseModule.successResponse(res, resultObject);
        }).catch(err => {
            return next(err);
        });
    }
}

let getAllProductsAdmin = (req, res, next) => {

    let searchProduct = _.trim(req.body.searchText) || "",
        categoryId = _.trim(req.body.categoryId) || "",
        subCategoryId = _.trim(req.body.subCategoryId) || "",
        offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 20,
        sortByPrice = parseInt(req.body.sortByPrice) || 0,
        sortByName = parseInt(req.body.sortByName) || 0;

    let sortObject = {};

    if (sortByPrice) {
        sortObject.price = -1;
    }

    if (sortByName) {
        sortObject.name = -1;
    }
    else {
        sortObject.name = 1;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    let queryObject = {
        name: "",
        category: "",
        subCategory: ""
    }

    if (searchProduct) {
        queryObject.name = {'$regex': searchProduct, '$options': 'i'};
    }
    else {
        delete queryObject.name;
    }

    if (categoryId) {
        queryObject.category = categoryId;
    }
    else {
        delete queryObject.category;
    }

    if (subCategoryId) {
        queryObject.subCategory = subCategoryId;
    }
    else {
        delete queryObject.subCategory;
    }

    let countOfProducts = 0;

    return _products.count(queryObject).then(productsCount => {
        countOfProducts = productsCount;

        return _products.find(queryObject).skip(offset).limit(limit).sort({'createdAt': -1});

    }).then(productsArray => {
        if (productsArray && productsArray.length) {
            resultObject.message = "Products fetched successfully.";
            resultObject.data = {products: productsArray, productsCount: countOfProducts};
        }
        else {
            resultObject.message = "No products found.";
            resultObject.data = {products: [], productsCount: 0};
        }
        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

let addProduct = (req, res, next) => {

    let productName = _.startCase(_.trim(req.body.productName)),
        price = parseInt(req.body.price),
        unit = _.trim(req.body.unit),
        description = _.trim(req.body.description) || "",
        images = req.body.images || [],
        categoryId = req.body.categoryId,
        subCategoryId = req.body.subCategoryId || "",
        keywords = req.body.keywords || [],
        storeId = req.body.storeId || "",
        isDiscounted = req.body.isDiscounted,
        discountPercentage = parseInt(req.body.discountPercentage) || 0,
        discountedPrice = 0;

    let productObject = {
        name: productName,
        price: price,
        unit: unit,
        description: description,
        images: images,
        category: "",
        subCategory: "",
        keywords: keywords,
        store: "",
        isDiscounted: isDiscounted,
        discountPercentage: discountPercentage,
        discountedPrice: discountedPrice
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    if (categoryId) {
        productObject.category = categoryId;
    }
    else {
        delete productObject.category;
    }

    if (subCategoryId) {
        productObject.subCategory = subCategoryId;
    }
    else {
        delete productObject.subCategory;
    }
    if (storeId) {
        productObject.store = storeId;
    }
    else {
        delete productObject.store;
    }
    if (keywords[0] !== "" && keywords.length > 0) {
        productObject.keywords = keywords;

    }
    else {
        delete productObject.keywords;
    }
    if (discountPercentage > 100) {

        throw next({
            msgCode: 8011
        });
    }
    else {
        productObject.discountPercentage = discountPercentage;
    }

    if (isDiscounted && discountPercentage) {
        let discountPrice = isDiscounted ? price * (discountPercentage / 100) : 0;
        discountedPrice = price - discountPrice;
        productObject.discountedPrice = discountedPrice.toFixed(2);
    }


    async.waterfall([
        (cb) => {
            if (!subCategoryId) {
                _subCategories.findOne({name: "Other", category: categoryId}).then(subCategory => {
                    if (subCategory) {
                        cb(null, subCategory._id);
                    }
                    else {
                        new _subCategories({
                            name: "Other",
                            description: "",
                            image: "",
                            category: categoryId
                        }).save().then(subCategoryCreated => {
                            subCategoryId = subCategoryCreated._id;
                            return _categories.findOneAndUpdate({_id: categoryId}, {$push: {subCategory: subCategoryCreated._id}});
                        }).then(()=> {
                            cb(null, subCategoryId);
                        }).catch(err => {
                            winston.error(err);
                            cb(err);
                        });
                    }
                });
            }
            else {
                cb(null, subCategoryId);
            }
        },
        (arg1, cb) => {

            productObject.subCategory = arg1;

            cb(null, 'done');
        }
    ], function (err, result) {

        if (err) {
            return next(err);
        }

        return _products.findOne({name: productName}, {_id: 1, name: 1}).then(productFound => {
            if (productFound) {
                throw {msgCode: 8014};
            }
            else {
                let productSaveObject = new _products(productObject);

                return productSaveObject.save();
            }
        }).then(productCreated => {
            if (productCreated) {
                resultObject.message = "Product created successfully.";
                resultObject.data = productCreated;

                responseModule.successResponse(res, resultObject);
            }
            else {
                throw {msgCode: 8003};
            }
        }).catch(err => {
            return next(err);
        });
    });
}

let editProduct = (req, res, next) => {

    const productId = _.trim(req.body.productId);

    let productName = _.startCase(_.trim(req.body.productName)) || "",
        price = parseInt(req.body.price) || 0,
        unit = _.trim(req.body.unit) || "",
        description = _.trim(req.body.description) || "",
        images = req.body.images || [],
        categoryId = _.trim(req.body.categoryId) || "",
        subCategoryId = _.trim(req.body.subCategoryId) || "",
        storeId = _.trim(req.body.storeId) || "",
        status = parseInt(req.body.status) || 0,
        isDiscounted = parseInt(req.body.isDiscounted) || 0,
        discountPercentage = _.trim(req.body.discountPercentage) || 0,
        keywords = req.body.keywords || [],
        enforce = parseInt(req.body.enforce) || 0;

    let discountedPrice = 0;
    let productObject = {
        name: "",
        price: 0,
        unit: "",
        description: "",
        images: [],
        category: "",
        subCategory: "",
        store: "",
        status: 0,
        isDiscounted: 0,
        keywords: [],
        discountPercentage: 0,
        discountedPrice: 0
    };

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    if (discountPercentage) {
        productObject.discountPercentage = discountPercentage;
    }
    else {
        delete productObject.discountPercentage;
    }
    if (productName) {
        productObject.name = productName;
    }
    else {
        delete productObject.name;
    }

    if (price !== 0) {
        productObject.price = price;
    }
    else {
        delete productObject.price;
    }

    if (unit) {
        productObject.unit = unit;
    }
    else {
        delete productObject.unit;
    }
    if (discountPercentage > 100) {

        throw next({msgCode: 8011});
    }
    else {
        productObject.discountPercentage = discountPercentage;
    }
    if (keywords[0] !== "" && keywords.length > 0) {
        productObject.keywords = keywords;

    }
    else {
        delete productObject.keywords;
    }
    if (description) {
        productObject.description = description;
    }
    else {
        delete productObject.description;
    }

    if (images && images.length) {
        productObject.images = images;
    }
    else {
        delete productObject.images;
    }

    if (categoryId) {
        productObject.category = categoryId;
    }
    else {
        delete productObject.category;
    }

    if (subCategoryId) {
        productObject.subCategory = subCategoryId;
    }
    else {
        delete productObject.subCategory;
    }

    if (storeId) {
        productObject.store = storeId;
    }
    else {
        delete productObject.store;
    }

    if (status !== 0 || status !== 1) {
        productObject.status = status;
    }
    else {
        delete productObject.status;
    }

    if (req.body.hasOwnProperty('isDiscounted')) {
        productObject.isDiscounted = isDiscounted;
    }
    else {
        delete productObject.isDiscounted;
    }

    if (isDiscounted && discountPercentage) {
        let discountPrice = isDiscounted ? price * (discountPercentage / 100) : 0;
        discountedPrice = price - discountPrice;
        productObject.discountedPrice = discountedPrice.toFixed(2);
        productObject.discountPrice = discountPrice.toFixed(2);
    }
    else {
        productObject.discountPrice = 0;
    }

    if (isDiscounted && !enforce) {
        return _promotions.findOne({products: productId}, {_id: 1}).then(promotions => {
            if (promotions) {
                throw {msgCode: 8013};
            } else {
                return _products.findOneAndUpdate({_id: productId}, {$set: productObject}, {new: true});
            }
        }).then(productUpdated => {
            if (productUpdated) {
                resultObject.message = "Product updated successfully";
                resultObject.data = productUpdated;

                responseModule.successResponse(res, resultObject);
            }
            else {
                throw {msgCode: 8005};
            }
        }).catch(err => {
            if (err && err.msgCode === 8013) {
                responseModule.successResponse(res, {
                    success: 1,
                    message: "Product is in promotion. Do you want to remove from promotion.",
                    data: {enforce: 1}
                });
            }
            else {
                return next(err);
            }
        });
    }
    else if (isDiscounted && enforce) {
        return _promotions.findOneAndUpdate({products: productId}, {$pull: {products: productId}}).exec().then(promotions => {
            return _products.findOneAndUpdate({_id: productId}, {$set: productObject}, {new: true});
        }).then(productUpdated => {
            if (productUpdated) {
                resultObject.message = "Product updated successfully";
                resultObject.data = productUpdated;

                responseModule.successResponse(res, resultObject);
            }
            else {
                throw {msgCode: 8005};
            }
        }).catch(err => {
            return next(err);
        });
    }
    else {
        return _products.findOneAndUpdate({_id: productId}, {$set: productObject}, {new: true}).then(productUpdated => {
            if (productUpdated) {
                resultObject.message = "Product updated successfully";
                resultObject.data = productUpdated;

                responseModule.successResponse(res, resultObject);
            }
            else {
                throw {msgCode: 8005};
            }
        }).catch(err => {
            return next(err);
        });
    }
}

let getProductDetail = (req, res, next) => {

    let productId = _.trim(req.params.productId);

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return _products.findOne({_id: productId}, {
        name: 1,
        price: 1,
        unit: 1,
        description: 1,
        images: 1,
        status: 1,
        isDiscounted: 1,
        discountPercentage: 1,
        category: 1,
        subCategory: 1,
        keywords: 1,
        store: 1
    }).populate('category subCategory store keywords').then(productDetail => {
        resultObject.message = "Product fetched successfully.";
        resultObject.data = productDetail;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

let mediaUploaded = (req, res, next) => {
    try {
        multer.resizeAndUpload(req.files[0].location, req.files[0].key.split('1000X1000/')[1], 1);
        return responseModule.successResponse(res, {
            success: 1,
            message: "",
            data: {url: req.files[0].location}
        });
    }
    catch (err) {
        winston.error(err);
        return next(err);
    }
}

let getProductsByIds = (req, res, next) => {

    let arrayOfIds = req.body.products;

    return _products.find({_id: {$in: arrayOfIds}}, {
        name: 1,
        price: 1,
        unit: 1,
        description: 1,
        images: 1,
        status: 1,
        isDiscounted: 1,
        discountPrice: 1,
        discountPercentage: 1,
        discountedPrice: 1
    }).then(productArray => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Products fetched successfully.",
            data: {products: productArray}
        });
    }).catch(err => {
        return next(err);
    });
}

module.exports = {
    getAllProducts,
    getAllProductsAdmin,
    addProduct,
    editProduct,
    getProductDetail,
    mediaUploaded,
    getProductsByIds
}
