/**
 * Created by Asif on 8/4/2017.
 */

'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let productsSchema = new Schema({
    name: {type: String, required: true},
    price: {type: Number, required: true},
    unit: {type: String, required: true},
    description: {type: String},
    images: [String],
    subCategory: {type: Schema.Types.ObjectId, ref: 'subCategories'},
    category: {type: Schema.Types.ObjectId, ref: 'categories'},
    store: {type: Schema.Types.ObjectId, ref: 'stores'},
    status: {type: Boolean, default: false},
    isDiscounted: {type: Boolean, default: false},
    discountPrice: {type: Number},
    discountPercentage: {type: Number, max: 100},
    discountedPrice: {type: Number},
    keywords: [
        {type: Schema.Types.ObjectId, ref: 'keywords'}
    ],
    promotionId: {type: Schema.Types.ObjectId, ref: 'promotions'}
});

productsSchema.plugin(timestamps);
productsSchema.index({name: 1}, {background: true, name: 'IDX_PRODUCT_NAME'});
productsSchema.index({price: 1}, {background: true, name: 'IDX_PRODUCT_PRICE'});


module.exports = mongoose.model('products', productsSchema);