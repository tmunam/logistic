
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

const twoWayDetailObj = {
    dcn_no : { type : String },
    quantity : { type : Number },
    bill : { type : Number }
};

let invoicesSchema = new Schema({
    tripId : { type: String, unique : true },
    bilti_no : { type : Number, unique : true },
    bilti_date : { type : Date },
    delivery_date : { type : Date },
    driver : { type : Schema.Types.ObjectId , ref : 'driver'},
    vehicle : { type : Schema.Types.ObjectId , ref : 'vehicles'},
    destination : { type : Schema.Types.ObjectId , ref : 'destinations'},
    distribution : { type : Schema.Types.ObjectId , ref : 'distributions'},
    distribution_name : { type : String },
    company : { type : Schema.Types.ObjectId , ref : 'companies'},
    dcn_list : [ twoWayDetailObj ],
    dcn_description : { type : String },
    status : {type: Boolean},
    is_fixed_veh : {type: Boolean},
    is_shahzore : {type: Boolean},
    zone_to_zone : {type: Boolean},
    shahzore_bill : {type: Number},
    shahzore_driver_name : {type: String},
    total_bill : { type : Number },
    fixed_price : { type : Number },
    fixed_vehicle_no : { type : String },
    fixed_driver_name : { type : String },

    //field on return
    is_delivered : {type: Boolean},
    is_two_way : {type: Boolean},
    //two_way_detail : [ twoWayDetailObj ],
    fuel_charges : { type : Number },
    toll_charges : { type : Number },
    transit_loss : { type : Number },
    offloading_charges : { type : Number },
    police_challan : { type : Number },
    detention_charges : { type : Number },
    is_returned : {type: Boolean},
    reason_of_return : { type : String },
    no_of_returned_carton : { type : Number},
    returned_cartoon_company : { type : String}
});

invoicesSchema.plugin(timestamps);

module.exports = mongoose.model('invoices', invoicesSchema);
