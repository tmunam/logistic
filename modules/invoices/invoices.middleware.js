
const winston = require('winston');

let checkinvoiceAddParam = (req, res, next) => {

    req.assert('invoice.vehicle', 9202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

let checkinvoiceEditParam = (req, res, next) => {

    req.assert('invoice.vehicle', 9202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

module.exports = {
    checkinvoiceAddParam,
    checkinvoiceEditParam
};
