

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    async = require('async'),
    path = require('path'),
    _invoices = mongoose.model('invoices'),
    _vehicles = mongoose.model('vehicles'),
    _drivers = mongoose.model('driver'),
    _companies = mongoose.model('companies'),
    _destinations = mongoose.model('destinations'),
    _distributions = mongoose.model('distributions'),
    _addrates = mongoose.model('addrates'),
    json2csv = require('json2csv'),
    exceljs = require('exceljs'),
    uuidv4 = require('uuid/v4'),
    unwindArray = require('javascript-unwind'),
    addExpenseHelper = require('../expenses/expenses.controller').addExpenseHelper,
    responseModule = require('../../config/response');


let listAllInvoices = (req, res, next) => {

    const offset = parseInt(req.body.offset) || 0,
        limit = parseInt(req.body.limit) || 10,
        searchTerm = req.body.searchTerm || '',
        searchType = req.body.searchType || '';

    let searchInvoices = { status : true };

    if (searchTerm) {
        if (searchType == 'bilti'){
            console.log('*********');
            console.log('searching by bilti #');
            console.log('*********');
            searchInvoices = {'$and' : [{'$or': [
                        {
                            $where : "/" + searchTerm  +"/.test(this.bilti_no)"
                        }]},{status : true}]};
        }
        else if (searchType == 'distributor'){
            console.log('*********');
            console.log('searching by distrubutor');
            console.log('*********');
            searchInvoices = {'$and' : [{'$or': [
                        {distribution_name : {'$regex': searchTerm, '$options': 'i'}},
                    ]},{status : true}]};
        }
        else if (searchType == 'dcn'){
            console.log('*********');
            console.log('searching by dcn');
            console.log('*********');
            searchInvoices = {'$and' : [{'$or': [
                        {dcn_list : {$elemMatch: {dcn_no : {'$regex': searchTerm, '$options': 'i'}}}}
                    ]},{status : true}]};
        }
    }

    let countInvoices = 0;

    return _invoices.count(searchInvoices).then(invoicesCount => {
        countInvoices = invoicesCount;

        return _invoices.find(searchInvoices).skip(offset).limit(limit).sort({'createdAt': '-1'})
            .populate("driver", "name")
            .populate("vehicle", "number")
            .populate("destination", "destination_name")
            .populate("distribution", "distribution_name")
            .populate("company", "company_name")
            .then(invoicesFound => {
            return invoicesFound;
        });
    }).then(invoicesFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "invoices fetched successfully.",
            data: { invoices: invoicesFound, count: countInvoices }
        });


    }).catch(err => {
        return next(err);
    });

};

let addInvoice = (req, res, next) => {

    var tripId = uuidv4();
    var rate = 0,
        expenseObj = {},
        is_fixed_veh = false,
        fixed_price = 0,
        fixed_vehicle_no = null,
        fixed_driver_name = null,
        is_shahzore = false,
        zone_to_zone = false,
        shahzore_driver_name = '',
        shahzore_bill = 0,
        driver = null,
        vehicle = null,
        total_bill = 0;
    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };
    var findRateFilter = {company_id : req.body.invoice.company._id,
        destination_id : req.body.invoice.destination._id};

    if (req.body.invoice.vehicle && req.body.invoice.vehicle.number == 'Shahzore For RWP/ISD' && req.body.invoice.is_shahzore){
        findRateFilter = { company_id : req.body.invoice.company._id, destination_name : 'SHAHZORE FOR RWP/ISD' };
        is_shahzore = true;
        shahzore_driver_name = req.body.invoice.shahzore_driver_name;
    }
    _addrates.findOne(findRateFilter).then(rt => {
       rate = rt.rate;
    }).then(() => {
        if (req.body.invoice.vehicle && !req.body.invoice.is_shahzore){
            vehicle = (req.body.invoice.vehicle._id);
            driver = (req.body.invoice.driver._id);
        }
        else if (req.body.invoice.is_fixed_veh){
            fixed_vehicle_no = req.body.invoice.fixed_vehicle_no;
            fixed_driver_name = req.body.invoice.fixed_driver_name;
        }
        else if (req.body.invoice.is_shahzore){

        }
        const company = (req.body.invoice.company._id),
            destination = (req.body.invoice.destination._id),
            distribution = (req.body.invoice.distribution._id),
            distribution_name = (req.body.invoice.distribution.distribution_name),
            bilti_no = (req.body.invoice.bilti_no),
            bilti_date = req.body.invoice.bilti_date,
            delivery_date = req.body.invoice.delivery_date,
            is_delivered = req.body.invoice.is_delivered ? req.body.invoice.is_delivered : false,
            is_returned = req.body.invoice.is_returned,
            dcn_description = req.body.invoice.dcn_description || '',
            is_two_way = req.body.invoice.is_two_way ? req.body.invoice.is_two_way : false;

        var dcn_list = [],
            //two_way_detail = [],
            returned_cartoon_company = '',
            reason_of_return = '',
            no_of_returned_carton = '',
            fuel_charges = '',
            offloading_charges = '',
            police_challan = '',
            transit_loss = '',
            toll_charges = '',
            detention_charges = '';

        if (is_shahzore){
            shahzore_bill = rate;
            total_bill = shahzore_bill;
            if (req.body.invoice.dcn_list){
                _.forEach(req.body.invoice.dcn_list, od => {
                    //od.bill = od.quantity * rate;
                    //total_bill = total_bill + od.bill;
                    dcn_list.push(od);
                });
            }
        }
        else{
            if (['PESHAWAR','LAHORE','FAISALABAD','MULTAN'].includes(distribution_name)){
                zone_to_zone = true;
                total_bill = rate;
                if (req.body.invoice.dcn_list){
                    _.forEach(req.body.invoice.dcn_list, od => {
                        //od.bill = od.quantity * rate;
                        //total_bill = total_bill + od.bill;
                        dcn_list.push(od);
                    });
                }
            }
            else{
                if (!req.body.invoice.is_fixed_veh){
                    if (req.body.invoice.dcn_list){
                        _.forEach(req.body.invoice.dcn_list, od => {
                            od.bill = od.quantity * rate;
                            total_bill = total_bill + od.bill;
                            dcn_list.push(od);
                        });
                    }
                }
                else{
                    if (req.body.invoice.dcn_list){
                        _.forEach(req.body.invoice.dcn_list, od => {
                            od.bill = od.quantity * rate;
                            total_bill = total_bill + od.bill;
                            dcn_list.push(od);
                        });
                    }
                    is_fixed_veh = true;
                    fixed_price = req.body.invoice.fixed_price;
                    //total_bill = req.body.invoice.fixed_price;
                }
            }
        }

        if (is_delivered){
            fuel_charges = req.body.invoice.fuel_charges ? parseInt(req.body.invoice.fuel_charges) : null,
            offloading_charges = req.body.invoice.offloading_charges ? parseInt(req.body.invoice.offloading_charges) : null,
            police_challan = req.body.invoice.police_challan ? parseInt(req.body.invoice.police_challan) : null,
            transit_loss = req.body.invoice.transit_loss ? parseInt(req.body.invoice.transit_loss) : null,
            toll_charges = req.body.invoice.toll_charges ? parseInt(req.body.invoice.toll_charges) : null,
            detention_charges = req.body.invoice.detention_charges ? parseInt(req.body.invoice.detention_charges) : null;
            expenseObj = {
                fuel_charges,
                offloading_charges,
                police_challan,
                transit_loss,
                toll_charges,
                detention_charges
            };

            for (var propName in expenseObj) {
                if (expenseObj[propName] === null || expenseObj[propName] === undefined) {
                    delete expenseObj[propName];
                }
            }

            /*if (is_returned){
                returned_cartoon_company = req.body.invoice.returned_cartoon_company._id,
                reason_of_return = req.body.invoice.reason_of_return,
                no_of_returned_carton = req.body.invoice.no_of_returned_carton;
            }*/

            /*if (is_two_way){
                _.forEach(req.body.invoice.two_way_detail, twd => {
                    twd.destination = twd.destination._id;
                    two_way_detail.push(twd);
                });

            }*/
        }

        let invoiceObject = {
            tripId,
            date : new Date(),
            driver,
            vehicle,
            fixed_vehicle_no,
            fixed_driver_name,
            destination,
            distribution,
            company,
            bilti_no,
            bilti_date,
            delivery_date,
            dcn_list,
            is_delivered,
            total_bill,
            dcn_description,
            status : true,
            is_fixed_veh,
            fixed_price,
            is_shahzore,
            shahzore_bill,
            shahzore_driver_name,
            distribution_name,
            zone_to_zone
        };

        if (is_delivered){
            invoiceObject.is_delivered = is_delivered;
            invoiceObject.fuel_charges =  fuel_charges;
            invoiceObject.offloading_charges =  offloading_charges;
            invoiceObject.police_challan =  police_challan;
            invoiceObject.transit_loss =  transit_loss;
            invoiceObject.toll_charges =  toll_charges;
            invoiceObject.detention_charges =  detention_charges;

        }

        /*if (is_returned){
            invoiceObject.is_returned = is_returned;
            invoiceObject.returned_cartoon_company =  returned_cartoon_company;
            invoiceObject.reason_of_return =  reason_of_return;
            invoiceObject.no_of_returned_carton =  no_of_returned_carton;
        }*/

        /*if (is_two_way){
            invoiceObject.is_two_way = is_two_way;
            invoiceObject.two_way_detail = two_way_detail;
        }*/

        return _invoices.findOne({ tripId : tripId }).then(InvoiceFound => {
            if (InvoiceFound) {
                throw {msgCode: 9206};
            }
            else {
                //console.log(invoiceObject);
                let invoiceCreateObject = new _invoices(invoiceObject);

                return invoiceCreateObject.save();
            }
        }).then(invoiceCreated => {

            resultObject.message = "Invoice created successfully.";
            resultObject.data = invoiceCreated;
            if (is_delivered){
                if (expenseObj.fuel_charges)
                    addExpenseHelper({invoice_id : invoiceCreated._id, expense_type : 'Diesel', amount : expenseObj.fuel_charges});
                if (expenseObj.offloading_charges)
                    addExpenseHelper({invoice_id : invoiceCreated._id, expense_type : 'Offloading charges', amount : expenseObj.offloading_charges});
                if (expenseObj.police_challan)
                    addExpenseHelper({invoice_id : invoiceCreated._id, expense_type : 'Police fine', amount : expenseObj.police_challan});
                if (expenseObj.transit_loss)
                    addExpenseHelper({invoice_id : invoiceCreated._id, expense_type : 'Transit loss', amount : expenseObj.transit_loss});
                if (expenseObj.toll_charges)
                    addExpenseHelper({invoice_id : invoiceCreated._id, expense_type : 'Toll tax', amount : expenseObj.toll_charges});
                if (expenseObj.detention_charges)
                    return addExpenseHelper({invoice_id : invoiceCreated._id, expense_type : 'Detention charges', amount : expenseObj.detention_charges});
                else
                    return;
            }
            else {
                return;
            }
        }).then(() => {
            responseModule.successResponse(res, resultObject);
        }).catch(err => {
            return next(err);
        });
    });
};

let editInvoice = (req, res, next) => {

    var rate = 0,
        expenseObj = {},
        is_fixed_veh = false,
        fixed_price = 0,
        fixed_vehicle_no = null,
        fixed_driver_name = null,
        is_shahzore = false,
        zone_to_zone = false,
        shahzore_driver_name = '',
        shahzore_bill = 0,
        driver = null,
        vehicle = null,
        total_bill = 0;
    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    var findRateFilter = {company_id : req.body.invoice.company._id,
        destination_id : req.body.invoice.destination._id};

    if (req.body.invoice.vehicle && req.body.invoice.vehicle.number == 'Shahzore For RWP/ISD' && req.body.invoice.is_shahzore){
        findRateFilter = { company_id : req.body.invoice.company._id, destination_name : 'SHAHZORE FOR RWP/ISD' };
        is_shahzore = true;
        shahzore_driver_name = req.body.invoice.shahzore_driver_name;
    }

    _addrates.findOne(findRateFilter).then(rt => {
        rate = rt.rate;
    }).then(() => {
        if (req.body.invoice.vehicle && !req.body.invoice.is_shahzore){
            vehicle = (req.body.invoice.vehicle._id);
            driver = (req.body.invoice.driver._id);
        }
        else if (req.body.invoice.is_fixed_veh){
            fixed_vehicle_no = req.body.invoice.fixed_vehicle_no;
            fixed_driver_name = req.body.invoice.fixed_driver_name;
        }
        else if (req.body.invoice.is_shahzore){

        }
        const tripId = _.trim(req.body.invoice.tripId),
            _id = (req.body.invoice._id),
            company = (req.body.invoice.company._id),
            destination = (req.body.invoice.destination._id),
            distribution = (req.body.invoice.distribution._id),
            distribution_name = (req.body.invoice.distribution.distribution_name),
            bilti_no = (req.body.invoice.bilti_no),
            bilti_date = req.body.invoice.bilti_date,
            delivery_date = req.body.invoice.delivery_date,
            is_delivered = req.body.invoice.is_delivered ? req.body.invoice.is_delivered : false,
            is_returned = req.body.invoice.is_returned,
            dcn_description = req.body.invoice.dcn_description || '',
            is_two_way = req.body.invoice.is_two_way ? req.body.invoice.is_two_way : false,
            status = (req.body.invoice.status);

        var dcn_list = [],
            //two_way_detail = [],
            returned_cartoon_company = '',
            reason_of_return = '',
            no_of_returned_carton = '',
            fuel_charges = '',
            offloading_charges = '',
            police_challan = '',
            transit_loss = '',
            toll_charges = '',
            detention_charges = '';

        if (is_shahzore){
            shahzore_bill = rate;
            total_bill = shahzore_bill;
            if (req.body.invoice.dcn_list){
                _.forEach(req.body.invoice.dcn_list, od => {
                    //od.bill = od.quantity * rate;
                    //total_bill = total_bill + od.bill;
                    dcn_list.push(od);
                });
            }
        }
        else{
            if (['PESHAWAR','LAHORE','FAISALABAD','MULTAN'].includes(distribution_name)){
                zone_to_zone = true;
                total_bill = rate;
                if (req.body.invoice.dcn_list){
                    _.forEach(req.body.invoice.dcn_list, od => {
                        //od.bill = od.quantity * rate;
                        //total_bill = total_bill + od.bill;
                        dcn_list.push(od);
                    });
                }
            }
            else{
                if (!req.body.invoice.is_fixed_veh){
                    if (req.body.invoice.dcn_list){
                        _.forEach(req.body.invoice.dcn_list, od => {
                            od.bill = od.quantity * rate;
                            total_bill = total_bill + od.bill;
                            dcn_list.push(od);
                        });
                    }
                }
                else{
                    if (req.body.invoice.dcn_list){
                        _.forEach(req.body.invoice.dcn_list, od => {
                            od.bill = od.quantity * rate;
                            total_bill = total_bill + od.bill;
                            dcn_list.push(od);
                        });
                    }
                    is_fixed_veh = true;
                    fixed_price = req.body.invoice.fixed_price;
                    //total_bill = req.body.invoice.fixed_price;
                }
            }
        }

        if (is_delivered){
            fuel_charges = req.body.invoice.fuel_charges ? parseInt(req.body.invoice.fuel_charges) : null,
                offloading_charges = req.body.invoice.offloading_charges ? parseInt(req.body.invoice.offloading_charges) : null,
                police_challan = req.body.invoice.police_challan ? parseInt(req.body.invoice.police_challan) : null,
                transit_loss = req.body.invoice.transit_loss ? parseInt(req.body.invoice.transit_loss) : null,
                toll_charges = req.body.invoice.toll_charges ? parseInt(req.body.invoice.toll_charges) : null,
                detention_charges = req.body.invoice.detention_charges ? parseInt(req.body.invoice.detention_charges) : null;
            expenseObj = {
                fuel_charges,
                offloading_charges,
                police_challan,
                transit_loss,
                toll_charges,
                detention_charges
            };

            for (var propName in expenseObj) {
                if (expenseObj[propName] === null || expenseObj[propName] === undefined) {
                    delete expenseObj[propName];
                }
            }

            /*if (is_returned){
                returned_cartoon_company = req.body.invoice.returned_cartoon_company._id,
                reason_of_return = req.body.invoice.reason_of_return,
                no_of_returned_carton = req.body.invoice.no_of_returned_carton;
            }*/

            /*if (is_two_way){
                _.forEach(req.body.invoice.two_way_detail, twd => {
                    twd.destination = twd.destination._id;
                    two_way_detail.push(twd);
                });

            }*/
        }

        let invoiceObject = {
            tripId,
            driver,
            vehicle,
            fixed_vehicle_no,
            fixed_driver_name,
            destination,
            distribution,
            company,
            bilti_no,
            bilti_date,
            delivery_date,
            dcn_list,
            is_delivered,
            total_bill,
            dcn_description,
            status,
            is_fixed_veh,
            fixed_price,
            is_shahzore,
            shahzore_bill,
            shahzore_driver_name,
            distribution_name,
            zone_to_zone
        };

        if (is_delivered){
            invoiceObject.is_delivered = is_delivered;
            invoiceObject.fuel_charges =  fuel_charges;
            invoiceObject.offloading_charges =  offloading_charges;
            invoiceObject.police_challan =  police_challan;
            invoiceObject.transit_loss =  transit_loss;
            invoiceObject.toll_charges =  toll_charges;
            invoiceObject.detention_charges =  detention_charges;

        }

        /*if (is_returned){
            invoiceObject.is_returned = is_returned;
            invoiceObject.returned_cartoon_company =  returned_cartoon_company;
            invoiceObject.reason_of_return =  reason_of_return;
            invoiceObject.no_of_returned_carton =  no_of_returned_carton;
        }*/

        /*if (is_two_way){
            invoiceObject.is_two_way = is_two_way;
            invoiceObject.two_way_detail = two_way_detail;
        }*/

        return _invoices.findOneAndUpdate({ _id: _id }, {$set: invoiceObject}, {new: true}).exec().then(invoiceCreated => {
            resultObject.message = "Invoice updated successfully.";
            resultObject.data = invoiceCreated;

            responseModule.successResponse(res, resultObject);
        }).catch(err => {
            return next(err);
        });
    });

};

let getOtherDetials = (req, res, next) => {
    async.parallel({
        vehicles : function(callback) {
            _vehicles.find({ status : true },{number :1 , driver : 1, is_fixed_veh : 1}).then(vehicles =>{
                callback(null, vehicles);
            });
        },
        drivers : function(callback) {
            _drivers.find({ status : true }, { name : 1 }).then(drivers =>{
                callback(null, drivers);
            });
        },
        companies : function(callback) {
            _companies.find({ status : true }, { company_name : 1 }).then(companies =>{
                callback(null, companies);
            });
        },
        destinations : function(callback) {
            _destinations.find({ status : true }, { destination_name : 1 }).sort({ 'destination_name' : 1 }).then(destinations =>{
                callback(null, destinations);
            });
        },
        distributions : function(callback) {
            _distributions.find({ status : true }, { distribution_name : 1, destination_id: 1 }).sort({ 'distribution_name' : 1 }).then(destinations =>{
                callback(null, destinations);
            });
        }
    }, function(err, results) {
        if (results){
            return responseModule.successResponse(res, {
                success: 1,
                message: "Details fetched successfully.",
                data: results
            });
        }
    });
};

let deleteInvoice = (req, res, next) => {
    const _id = (req.body.invoice._id);

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };
    return _invoices.findOneAndUpdate({ _id: _id }, {$set: {status : false}}).exec().then(invoiceCreated => {
        resultObject.message = "Invoice deleted successfully.";
        resultObject.data = invoiceCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
};

/*let getAllRiders = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchRider = {status: true};

    if (searchText) {
        searchRider.name = {'$regex': searchText, '$options': 'i'};
    }

    let countRiders = 0;

    return _driver.count(searchRider).then(ridersCount => {
        countRiders = ridersCount;

        return _driver.find(searchRider, {
            status: 1,
            name: 1,
            description: 1,
            address: 1,
            phone: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(ridersFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Riders fetched successfully.",
            data: {riders: ridersFound, count: countRiders}
        });


    }).catch(err => {
        return next(err);
    });

}*/

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

let createDestinations = (req, res) => {
    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://localhost:27017";
    var ObjectID = require('mongodb').ObjectID;
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        //console.log(db);
        var dbo = db.db("zameer_transport");
        _destinations.find().then(function(result) {
            //if (err) throw err;
            /*var distNameList = result.map(function (i){
                return i["Customer Name"];
            });*/

            //var unique = distNameList.filter( onlyUnique );
            //console.log(unique);
            /*_.forEach(result, function(item){

            });*/

            async.eachSeries(result, function(item, inCb){
                var destName = item.destination_name;
                console.log('*******');
                dbo.collection("testKM").findOne({ "City" : destName}).then(dest => {
                    /*console.log(dest);
                    inCb();*/
                    var obj = {
                        company_id : ObjectID('5b145692b1724231c74ff1dd'),
                        company_name : 'Colgate',
                        destination_name : destName,
                        destination_id : item._id,
                        rate : dest.Rate
                    };
                    /*console.log(obj);
                    inCb();*/
                    var distObj = new _addrates(obj);
                    distObj.save().then(ok => {
                        inCb();
                    });
                });
            },function(err) {
                console.log('!!!!!!!!!!!!!!!!!!!');
                db.close();
                res.send({staus : 'OK'});
            });

        });
    });
};

let generateBill = (req, res, next) => {
    var startDate = req.body.startDate,
        endDate = req.body.endDate,
        testCount = 0;

    startDate = new Date(startDate);
    //startDate = startDate.toISOString();
    endDate = new Date(endDate);
    //endDate = endDate.toISOString();

    console.log('********')
    console.log(startDate)
    console.log(endDate)
    console.log('********')

    _invoices.find({ '$and' : [ {createdAt: { $gte : startDate }}, {createdAt : { $lte : endDate}}] })
        .populate("driver", "name")
        .populate("vehicle", "number")
        .populate("destination", "destination_name")
        .populate("distribution", "distribution_name")
        .populate("company", "company_name")
        .then(invoices => {
            var totalBill = invoices.map(a => a.total_bill);
            totalBill = totalBill.reduce((a,b) => a+b);
            var workbook = new exceljs.Workbook(),
                fileName = 'bill-' + new Date() + '.xlsx',
                filePath = 'public/uploads/' + fileName;
            var sheet = workbook.addWorksheet('My Sheet');
            sheet.columns = [
                { header: 'Bilti No', key: 'bilti_no', width : 20 },
                { header: 'Driver Name', key: 'driver_name', width : 20 },
                { header: 'Vehicle No.', key: 'vehicle_no', width : 20 },
                { header: 'Destination', key: 'destination_name', width : 20 },
                { header: 'Distribution', key: 'distribution_name', width : 20 },
                { header: 'Company', key: 'company_name', width : 20 },
                { header: 'Bilti Date', key: 'bilti_date', width : 20 },
                { header: 'Delivery Date', key: 'delivery_date', width : 20 },
                { header: 'DCN', key: 'dcn_str' },
                { header: 'Quantity', key: 'quantity' },
                { header: 'Rate', key: 'rate' },
                { header: 'Bill', key: 'total_bill', width : 20 },

            ];
            console.log('**************')
            console.log(invoices.length);
            console.log('**************')

            try{
                async.eachSeries(invoices, function(inv, inCb){
                    var unwindDcn = unwindArray(inv, 'dcn_list');
                    console.log('************** not unwind')
                    console.log(inv);
                    console.log('************** not unwind')
                    _addrates.findOne({destination_id : inv.destination}).then(rt => {
                        //console.log(':::::::::::::::::::::::::::::');
                        inv.driver_name = inv.driver ? inv.driver.name : inv.fixed_driver_name;
                        inv.vehicle_no = inv.vehicle ? inv.vehicle.number : inv.fixed_vehicle_no;
                        inv.destination_name = inv.destination.destination_name;
                        inv.distribution_name = inv.distribution.distribution_name;
                        inv.company_name = inv.company.company_name;
                        if (!inv.driver_name && inv.is_shahzore)
                            inv.driver_name = inv.shahzore_driver_name;
                        if (!inv.vehicle_no && inv.is_shahzore)
                            inv.vehicle_no = 'Shahzore For RWP/ISD';
                        var unwindDcn = unwindArray(inv, 'dcn_list');

                        _.forEach(unwindDcn, uDCN => {
                            uDCN.bilti_date = inv.bilti_date;
                            uDCN.delivery_date = inv.delivery_date;
                            uDCN.distribution_name = inv.distribution.distribution_name;
                            uDCN.dcn_str = uDCN.dcn_list.dcn_no;
                            uDCN.quantity = uDCN.dcn_list.quantity;
                            uDCN.rate = rt.rate;
                            uDCN.total_bill = uDCN.quantity * uDCN.rate;
                            console.log('************** unwind')
                            console.log(uDCN.distribution_name);
                            console.log(uDCN.destination_name);
                            console.log('************** unwind')
                            sheet.addRow(uDCN);
                            //console.log(testCount++);
                        });
                        /*inv.dcn_str = inv.dcn_list.map(function(e){
                            return (e.dcn_no + ' (' + e.quantity + ')' + ' (' + rt.rate + ')');
                        }).join(', ');
                        sheet.addRow(inv);*/
                        inCb();
                    });
                },function(err) {
                    //console.log('{{{{{{{{{{{{}}}}}}}}}}}}}');
                    sheet.addRow({});
                    sheet.addRow({ 'rate' : 'Total Bill', 'total_bill' : totalBill });

                    workbook.xlsx.writeFile(filePath).then(function (doneWriting) {

                        responseModule.successResponse(res, {
                            success: 1,
                            message: "Details fetched successfully.",
                            data: { fileName : fileName }
                        });
                    });
                });

            }
             catch (e) {
                 console.log(e);
             }

        });
};

let updateFixedInvoicesRates = (req, res) => {
    _invoices.find({is_fixed_veh : true}).then(invoices => {
        if (invoices && invoices.length > 0){
            async.eachSeries(invoices, function(inv, inCb){
                if (inv.is_fixed_veh){
                    _addrates.findOne({destination_id : inv.destination}).then(rt => {
                        var dcn_list = inv.dcn_list.map((x) => {
                            x.bill = x.quantity * rt.rate;
                            return x;
                        });
                        inv.dcn_list = dcn_list;
                        var totalBill = inv.dcn_list.map(a => a.bill);
                        totalBill = totalBill.reduce((a,b) => a+b);
                        inv.total_bill = totalBill;
                        inv.save().then(invSave => {
                            console.log('fixed')
                            inCb();
                        });

                    });
                }
                else{
                    console.log('not fixed');
                    inCb();
                }

            },function (err){
                responseModule.successResponse(res, {
                    success: 1,
                    message: "Invoices update.",
                    data: 'Invoices update'
                });
            });
        }
        else{
            responseModule.successResponse(res, {
                success: 1,
                message: "No invoice found.",
                data: 'No invoice found'
            });
        }
    });
}

let pushDistributorName = (req, res) => {
    _invoices.find({}).then(invoices => {
        if (invoices && invoices.length > 0){
            async.eachSeries(invoices, function(inv, inCb){
                _distributions.findOne({_id : inv.distribution}).then(dist => {
                    inv.distribution_name = dist.distribution_name;
                    inv.save().then(invSave => {
                        console.log('fixed')
                        inCb();
                    });
                });
            },function (err){
                responseModule.successResponse(res, {
                    success: 1,
                    message: "Invoices update.",
                    data: 'Invoices update'
                });
            });
        }
        else{
            responseModule.successResponse(res, {
                success: 1,
                message: "No invoice found.",
                data: 'No invoice found'
            });
        }
    });
}

module.exports = {
    listAllInvoices,
    addInvoice,
    editInvoice,
    getOtherDetials,
    deleteInvoice,
    createDestinations,
    generateBill,
    updateFixedInvoicesRates,
    pushDistributorName
    /*getAllRiders*/
};
