const invoicesMiddleware = require('./invoices.middleware.js'),
    invoicesController = require('./invoices.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/invoices/getAllInvoices', passport.isAuthenticated, invoicesController.listAllInvoices);
    app.post(version + '/invoices/addInvoice', passport.isAuthenticated , invoicesController.addInvoice);
    app.post(version + '/invoices/editInvoice', passport.isAuthenticated, invoicesController.editInvoice);
    app.post(version + '/invoices/deleteInvoice', passport.isAuthenticated, invoicesController.deleteInvoice);
    app.get(version + '/invoices/getOtherDetials', passport.isAuthenticated, invoicesController.getOtherDetials);
    app.get(version + '/create/destinations', invoicesController.createDestinations);
    app.post(version + '/bill/generateBill', passport.isAuthenticated, invoicesController.generateBill);
    app.get('/invoices/updateFixedInvoicesRates', invoicesController.updateFixedInvoicesRates);
    app.get('/invoices/pushDistributorName', invoicesController.pushDistributorName);
    //app.get(version + '/rider/getAllRidersArchived', passport.isAuthenticated, vehicleController.getAllRiders);

};
