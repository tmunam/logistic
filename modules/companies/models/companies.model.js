
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;


let companiesSchema = new Schema({
    //type: {type: String, enum: ['USER', 'PET', 'ADMIN']},
    company_name: {type : String},
    city : { type : String },
    status : {type: Boolean, default: true},
    companyLocation : {
        type: { type : String },
        coordinates : [Number]
    }
});

companiesSchema.plugin(timestamps);
//companiesSchema.index({dcompanyLocation: '2dsphere'});

module.exports = mongoose.model('companies', companiesSchema);
