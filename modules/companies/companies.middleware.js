
const winston = require('winston');

let checkCompanyAddParam = (req, res, next) => {

    req.assert('company.company_name', 9200).notEmpty();
    // req.assert('address', 9201).notEmpty();
    req.assert('company.city', 9202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

let checkCompanyEditParam = (req, res, next) => {

    req.assert('company.company_name', 9204).notEmpty();
    req.assert('company.city', 9204).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

module.exports = {
    checkCompanyAddParam,
    checkCompanyEditParam
};
