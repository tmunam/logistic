const companiesMiddleware = require('./companies.middleware.js'),
    companiesController = require('./companies.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/companies/getAllCompanies/:offset/:limit', passport.isAuthenticated, companiesController.listAllCompanies);
    app.post(version + '/companies/addCompany', passport.isAuthenticated , companiesMiddleware.checkCompanyAddParam, companiesController.addCompany);
    app.post(version + '/companies/editCompany', passport.isAuthenticated, companiesMiddleware.checkCompanyEditParam, companiesController.editCompany);
    //app.get(version + '/rider/getAllRidersArchived', passport.isAuthenticated,passport.isAuthorized('ADMIN'), vehicleController.getAllRiders);

};