

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _companies = mongoose.model('companies'),
    responseModule = require('../../config/response');


let listAllCompanies = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchCompanies = { status : true };

    if (searchText) {
        searchCompanies.name = {'$regex': searchText, '$options': 'i'};
    }

    let countCompanies = 0;

    return _companies.count(searchCompanies).then(companiesCount => {
        countCompanies = companiesCount;

        return _companies.find(searchCompanies).skip(offset).limit(limit).sort({'company_name': '1'})
    }).then(companiesFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Companies fetched successfully.",
            data: { companies: companiesFound, count: countCompanies }
        });


    }).catch(err => {
        return next(err);
    });

};

let addCompany = (req, res, next) => {

    const company_name = _.trim(req.body.company.company_name),
        city = _.trim(req.body.company.city) || "";

    let companyObject = {
        company_name: company_name,
        city: city,
        status : true
    };

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };
    return _companies.findOne({ company_name : company_name }).then(companyFound => {
        if (companyFound) {
            throw {msgCode: 9206};
        }
        else {
            let companyCreateObject = new _companies(companyObject);

            return companyCreateObject.save();
        }
    }).then(companyCreated => {

        resultObject.message = "Destination created successfully.";
        resultObject.data = companyCreated;

        responseModule.successResponse(res, resultObject);

    }).catch(err => {
        return next(err);
    });
};

let editCompany = (req, res, next) => {

    const company_name = _.trim(req.body.company.company_name),
        _id = _.trim(req.body.company._id) || "",
        city = _.trim(req.body.company.city) || "",
        status = req.body.company.status;

    let companyObject = {
        company_name: company_name,
        city: city,
        status : status
    };


    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };

    return _companies.findOneAndUpdate({ _id: _id }, {$set: companyObject}, {new: true}).exec().then(companyCreated => {
        resultObject.message = "Company updated successfully.";
        resultObject.data = companyCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
};

/*let getAllRiders = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchRider = {status: true};

    if (searchText) {
        searchRider.name = {'$regex': searchText, '$options': 'i'};
    }

    let countRiders = 0;

    return _driver.count(searchRider).then(ridersCount => {
        countRiders = ridersCount;

        return _driver.find(searchRider, {
            status: 1,
            name: 1,
            description: 1,
            address: 1,
            phone: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(ridersFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Riders fetched successfully.",
            data: {riders: ridersFound, count: countRiders}
        });


    }).catch(err => {
        return next(err);
    });

}
*/

module.exports = {
    listAllCompanies,
    addCompany,
    editCompany,
    /*getAllRiders*/
};
