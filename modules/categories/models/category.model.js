/**
 * Created by Asif on 8/4/2017.
 */
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let categorySchema = new Schema({
    name: {type: String, required: true},
    description: {type: String},
    status: {type: Boolean, default: true},
    image : {type : String, require : true},
    subCategory: [
        {type : Schema.Types.ObjectId, ref : 'subCategories'}
    ]
});

categorySchema.plugin(timestamps);
categorySchema.index({name: 1}, {background: true, name: 'IDX_CATEGORY_NAME'});

module.exports = mongoose.model('categories', categorySchema);