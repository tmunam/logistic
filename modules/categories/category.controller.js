/**
 * Created by Asif on 8/5/2017.
 */

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    category = mongoose.model('categories'),
    responseModule = require('../../config/response'),
    _products = mongoose.model('products');

let getAllCategories = (req, res, next) => {
    const limit = parseInt(req.params.limit) || 1000,
        offset = parseInt(req.params.offset) || 0;

    let resultObject = {
        success: 1,
        message: "",
        data: {}
    }

    return category.find({
        status: true,
        'subCategory.0': {$exists: true}
    }).populate('subCategory').skip(offset).limit(limit).then(categories => {
        if (categories && categories.length) {
            resultObject.data = {categories: categories};
            resultObject.message = 'Categories fetched successfully.'
        }
        else {
            resultObject.data = {categories: []};
            resultObject.message = 'No categories found.'
        }

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        winston.error(err);
        return next(err);
    });
}

let getAllCategoriesAdmin = (req, res, next) => {

    let limit = parseInt(req.params.limit) || 1000,
        offset = parseInt(req.params.offset) || 0,
        searchText = _.trim(req.body.searchText) || "",
        categoryCount = 0,
        categoriesArray = [];

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }
    let queryObject = {
        name: ""
    }

    if (searchText) {
        queryObject.name = {'$regex': searchText, '$options': 'i'};
    }
    else {
        delete queryObject.name;
    }

    function search(nameKey, myArray) {
        let flag = false;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i]._id.toString() == nameKey.toString()) {
                flag = true;
                return myArray[i];
            }
            if (flag) {
                break;
            }
        }
    }

    return category.count(queryObject).then(count => {
        categoryCount = count;
        return category.find(queryObject).populate('subCategory').skip(offset).limit(limit).sort({'createdAt': '-1'}).lean();
    }).then(categories => {
        categoriesArray = categories;

        const categoriesIdArray = _.map(categoriesArray, '_id');

        return _products.aggregate([{
            $match: {category: {$in: categoriesIdArray}}
        }, {$group: {_id: "$category", productsCount: {$sum: 1}}}]);
    }).then(productsArrayCount => {

        _.forEach(categoriesArray, (category, index) => {
            let countFind = null;

            countFind = search(category._id, productsArrayCount);

            if (countFind) {
                categoriesArray[index].productsCount = countFind.productsCount;
            }
            else {
                categoriesArray[index].productsCount = 0;
            }
            categoriesArray[index].subCategoryCount = categoriesArray[index].subCategory.length;
        });

        if (categoriesArray && categoriesArray.length) {
            resultObject.data = {categories: categoriesArray, categoryCount: categoryCount};
            resultObject.message = 'Categories fetched successfully.'
        }
        else {
            resultObject.data = {categories: [], categoryCount: 0};
            resultObject.message = 'No categories found.'
        }

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        winston.error(err);
        return next(err);
    });
}

let addCategoryAsync = (req, res, next) => {

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    const name = _.startCase(_.trim(req.body.name)),
        description = _.trim(req.body.description),
        image = _.trim(req.body.image);

    const categoryObject = {
        name: name,
        description: description,
        image: image
    }

    return category.findOne({name: name}).then(categoryFound => {
        if (categoryFound) {
            throw {msgCode: 6002};
        }
        else {
            let categoryCreate = new category(categoryObject);

            return categoryCreate.save();
        }
    }).then(categoryCreated => {
        resultObject.message = "Category created successfully.";
        resultObject.data = categoryCreated;
        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

let editCategoryAsync = (req, res, next) => {

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    const categoryId = _.trim(req.body.categoryId),
        name = _.startCase(_.trim(req.body.name)),
        description = _.trim(req.body.description) || "",
        image = _.trim(req.body.image),
        status = parseInt(req.body.status);

    const categoryObject = {
        name: "",
        description: "",
        image: "",
        status: 0
    }

    if (name) {
        categoryObject.name = name;
    }
    else {
        delete categoryObject.name;
    }

    if (req.body.hasOwnProperty('description')) {
        categoryObject.description = description;
    }
    else {
        delete categoryObject.description;
    }

    if (image) {
        categoryObject.image = image;
    }
    else {
        delete categoryObject.image;
    }

    if (status == 1) {
        categoryObject.status = status;
    }
    else if (status == 0) {
        categoryObject.status = status;
    }
    else {
        delete categoryObject.status;
    }

    return category.findOne({name: name}).then(categoryFound => {
        if (categoryFound) {
            throw {msgCode: 6002};
        }
        else {
            return category.findOneAndUpdate({_id: categoryId}, {$set: categoryObject}, {new: true});
        }
    }).then(categoryCreated => {
        if (categoryCreated) {
            resultObject.message = "Category updated successfully.";

            resultObject.data = categoryCreated;

            responseModule.successResponse(res, resultObject);
        }
        else {
            throw {msgCode: 6004};
        }
    }).catch(err => {
        return next(err);
    });
}

let mediaUploaded = (req, res, next) => {
    try {
        return responseModule.successResponse(res, {
            success: 1,
            message: "",
            data: {url: req.files[0].location}
        });
    }
    catch (err) {
        winston.error(err);
        return next(err);
    }
}

module.exports = {
    getAllCategories,
    addCategoryAsync,
    editCategoryAsync,
    mediaUploaded,
    getAllCategoriesAdmin
};
