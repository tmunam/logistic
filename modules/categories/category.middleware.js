/**
 * Created by Asif on 8/4/2017.
 */


const winston = require('winston');

let createCategoryParams = (req, res, next) => {

    req.assert('name', 6000).notEmpty();
    req.assert('image', 6001).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let editCategoryParams = (req, res, next) => {

    req.assert('categoryId', 6003).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

module.exports = {
    createCategoryParams,
    editCategoryParams
}