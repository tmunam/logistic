/**
 * Created by Asif on 8/4/2017.
 */

const categoryMiddleware = require('./category.middleware'),
    categoryController = require('./category.controller'),
    passport = require('../../config/passport'),
    multer = require('../../config/multer');

let imageUpload = multer.upload(config.aws.s3.categoryDirectory);

module.exports = function (app, version) {

    app.get(version + '/categories/getAllCategories/:offset/:limit', categoryController.getAllCategories);

    app.post(version + '/categories/getAllCategories/:offset/:limit', categoryController.getAllCategoriesAdmin);

    app.post(version + '/categories/category', passport.isAuthenticated, categoryMiddleware.createCategoryParams, categoryController.addCategoryAsync);

    app.post(version + '/categories/category/edit', passport.isAuthenticated, categoryMiddleware.editCategoryParams, categoryController.editCategoryAsync);

    app.post(version + '/categories/uploadImage', passport.isAuthenticated, imageUpload.array('image', 1), categoryController.mediaUploaded);

};