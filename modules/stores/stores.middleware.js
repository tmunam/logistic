/**
 * Created by Asif on 8/8/2017.
 */


const winston = require('winston');

let checkStoreAddParam = (req, res, next) => {

    req.assert('name', 9000).notEmpty();
    req.assert('address', 9001).notEmpty();
    req.assert('contactPersonName', 9004).notEmpty();
    req.assert('contactPersonEmail', 9005).notEmpty();
    req.assert('contactPersonEmail', 9006).isValidEmail();


    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let checkStoreId = (req, res, next) => {

    req.assert('storeId', 9007).notEmpty();
    req.assert('storeId', 9007).isValidObjectId();



    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}


module.exports = {
    checkStoreAddParam,
    checkStoreId
}




