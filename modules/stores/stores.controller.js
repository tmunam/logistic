/**
 * Created by Asif on 8/8/2017.
 */


const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    stores = mongoose.model('stores'),
    responseModule = require('../../config/response');

let getAllStores = (req, res, next) => {

    let offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10;

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return stores.find({}).skip(offset).limit(limit).then(storesFound => {
        resultObject.message = "Stores found successfully.";
        resultObject.data = storesFound;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

let addStore = (req, res, next) => {

    const name = _.trim(req.body.name),
        description = _.trim(req.body.description) || "",
        address = _.trim(req.body.address) || "",
        phone = _.trim(req.body.phone) || "",
        status = parseInt(req.body.status) || 0,
        logo = _.trim(req.body.logo) || "",
        contactPersonName = _.trim(req.body.contactPersonName) || "",
        contactPersonEmail = _.trim(req.body.contactPersonEmail) || "",
        latitude = req.body.latitude || 0,
        longitude = req.body.longitude || 0;

    let storeObject = {
        name: name,
        description: description,
        address: address,
        phone: phone,
        status: status,
        logo: logo,
        contactPersonName: contactPersonName,
        contactPersonEmail: contactPersonEmail,
        storeLocation: {type: 'Point', coordinates: [latitude, longitude]}
    }

    if (!latitude && !longitude) {
        delete storeObject.storeLocation;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }
    return stores.findOne({name: name}).then(storesFound => {
        if (storesFound) {
            throw {msgCode: 9009};
        }
        else {

            let storeCreateObject = new stores(storeObject);

            return storeCreateObject.save();
        }
    }).then(storeCreated => {
        resultObject.message = "Store created successfully.";
        resultObject.data = storeCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}


let getAllStoresPostReq = (req, res, next) => {

    let searchStore = _.trim(req.body.searchText) || "",
        offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 20,
        sortByName = parseInt(req.body.sortByName) || 0;

    let sortObject = {};

    if (sortByName) {
        sortObject.name = -1;
    }
    else {
        sortObject.name = 1;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    let queryObject = {
        name: ""
    }

    if (searchStore) {
        queryObject.name = {'$regex': searchStore, '$options': 'i'};
    }
    else {
        delete queryObject.name;
    }

    let countStore = 0;

    return stores.count(queryObject).then(storeCount => {
        countStore = storeCount;
        return stores.find(queryObject).skip(offset).limit(limit).sort({'createdAt': '-1'});
    }).then(storesArray => {
        if (storesArray && storesArray.length) {
            resultObject.message = "Stores fetched successfully.";
            resultObject.data = {stores: storesArray, count: countStore};
        }
        else {
            resultObject.message = "No Store found.";
            resultObject.data = {stores: [], count: countStore};
        }
        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

let editStore = (req, res, next) => {

    const storeId = _.trim(req.body.storeId);

    const name = _.trim(req.body.name),
        description = _.trim(req.body.description) || "",
        address = _.trim(req.body.address) || "",
        phone = _.trim(req.body.phone) || "",
        status = parseInt(req.body.status) || 0,
        logo = _.trim(req.body.logo) || "",
        contactPersonName = _.trim(req.body.contactPersonName) || "",
        contactPersonEmail = _.trim(req.body.contactPersonEmail) || "",
        latitude = req.body.latitude || 0,
        longitude = req.body.longitude || 0;

    let storeObject = {
        name: "",
        description: "",
        address: "",
        phone: "",
        status: 0,
        logo: "",
        contactPersonName: "",
        contactPersonEmail: "",
        storeLocation: {type: "", coordinates: []}
    }

    if (name) {
        storeObject.name = name;
    } else {
        delete storeObject.name;
    }
    if (description) {
        storeObject.description = description;
    }
    else {
        delete storeObject.description;
    }
    if (address) {
        storeObject.address = address;
    }
    else {
        delete storeObject.address;
    }
    if (phone) {
        storeObject.phone = phone;
    }
    else {
        delete storeObject.phone;
    }
    if (status !== 0) {
        storeObject.status = status;
    }
    else {
        delete storeObject.status;
    }
    if (logo) {
        storeObject.logo = logo;
    }
    else {
        delete storeObject.logo;
    }
    if (contactPersonName) {
        storeObject.contactPersonName = contactPersonName;
    }
    else {
        delete storeObject.contactPersonName;
    }
    if (contactPersonEmail) {
        storeObject.contactPersonEmail = contactPersonEmail;
    }
    else {
        delete storeObject.contactPersonEmail;
    }
    if (latitude && longitude) {
        storeObject.storeLocation.type = 'Point';
        let latLong = [latitude, longitude];
        storeObject.storeLocation.coordinates = latLong;
    } else {
        delete storeObject.storeLocation;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

            return stores.findOneAndUpdate({_id: storeId}, {$set: storeObject}, {new: true}).then(storeUpdated => {
        if (storeUpdated) {
            resultObject.message = "Store updated successfully";
            resultObject.data = storeUpdated;

            responseModule.successResponse(res, resultObject);
        }
        else {
            throw {msgCode: 9007};
        }
    }).catch(err => {
        return next(err);
    });
}

module.exports = {
    getAllStores,
    addStore,
    editStore,
    getAllStoresPostReq
}
