/**
 * Created by Asif on 8/8/2017.
 */


const storesMiddleware = require('./stores.middleware.js'),
    storesController = require('./stores.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {
    app.get(version + '/store/getAllStoresListing/:offset/:limit', passport.isAuthenticated, storesController.getAllStores);
    app.post(version + '/store/addStore', passport.isAuthenticated, passport.isAuthorized('ADMIN'), storesMiddleware.checkStoreAddParam, storesController.addStore);
    app.post(version + '/store/editStore', passport.isAuthenticated, passport.isAuthorized('ADMIN'), storesMiddleware.checkStoreId, storesController.editStore);
    app.post(version + '/store/getAllStores/:offset/:limit', passport.isAuthenticated, storesController.getAllStoresPostReq);
};