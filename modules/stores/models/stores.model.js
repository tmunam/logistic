/**
 * Created by Asif on 8/8/2017.
 */
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let storeSchema = new Schema({
    storeId: {type: Schema.Types.ObjectId, ref: 'stores'},
    name: {type: String, required: true},
    description: {type: String},
    address: {type: String},
    phone: {type: String},
    status: {type: Boolean, default: true},
    logo: {type: String},
    contactPersonName: {type: String},
    contactPersonEmail: {type: String},
	storeLocation : {
        type: { type: String },
        coordinates : [Number]
	}
});

storeSchema.plugin(timestamps);
storeSchema.index({storeLocation: '2dsphere'})

module.exports = mongoose.model('stores', storeSchema);

