const vehicleMiddleware = require('./vehicles.middleware.js'),
    vehicleController = require('./vehicles.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/vehicles/getAllVehicles/:offset/:limit', passport.isAuthenticated, vehicleController.listAllVehicles);
    app.post(version + '/vehicle/addVehicle', passport.isAuthenticated , vehicleMiddleware.checkVehicleAddParam, vehicleController.addVehicle);
    app.post(version + '/vehicle/editVehicle', passport.isAuthenticated, vehicleMiddleware.checkVehicleEditParam, vehicleController.editVehicle);
    //app.get(version + '/rider/getAllRidersArchived', passport.isAuthenticated, vehicleController.getAllRiders);

};