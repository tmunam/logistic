
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

const driver = {
    _id : { type : Schema.Types.ObjectId },
    name : { type : String }
};

let vehicleSchema = new Schema({
    number: {type : String, required: true},
    //type: {type: String, enum: ['USER', 'PET', 'ADMIN']},
    type: {type : String},
    driver : driver,
    is_token_paid : {type: Boolean, default: false},
    is_fixed_veh : {type: Boolean, default: false},
    annual_token_fee : {type: String},
    detention_charges : {type: Number},
    current_value : {type: Number},
    status : {type: Boolean, default: true},
    vehicleLocation : {
        type: { type : String },
        coordinates : [Number]
    }
});

vehicleSchema.plugin(timestamps);
//riderSchema.index({riderLocation: '2dsphere'});

module.exports = mongoose.model('vehicles', vehicleSchema);
