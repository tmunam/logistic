
const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _vehicles = mongoose.model('vehicles'),
    responseModule = require('../../config/response');


let listAllVehicles = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchVehicle = {status : true};

    if (searchText) {
        searchVehicle.name = {'$regex': searchText, '$options': 'i'};
    }

    let countVehicles = 0;

    return _vehicles.count(searchVehicle).then(vehiclesCount => {
        countVehicles = vehiclesCount;

        return _vehicles.find(searchVehicle).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(vehiclesFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Vehicles fetched successfully.",
            data: {vehicles: vehiclesFound, count: countVehicles}
        });


    }).catch(err => {
        return next(err);
    });

};

let addVehicle = (req, res, next) => {

    const number = _.trim(req.body.vehicle.number),
        type = _.trim(req.body.vehicle.type) || "",
        driver = {
            _id : (req.body.vehicle.driver._id),
            name : (req.body.vehicle.driver.name)
        },
        is_token_paid = (req.body.vehicle.is_token_paid),
        is_fixed_veh = (req.body.vehicle.is_fixed_veh),
        annual_token_fee = _.trim(req.body.vehicle.annual_token_fee) || "",
        detention_charges = parseInt(req.body.vehicle.detention_charges) || 0,
        current_value = parseInt(req.body.vehicle.current_value) || 0,
        latitude = req.body.vehicle.latitude,
        longitude = req.body.vehicle.longitude;

    let vehicleObject = {
        number: number,
        type: type,
        driver : driver,
        is_token_paid: is_token_paid,
        is_fixed_veh: is_fixed_veh,
        annual_token_fee: annual_token_fee,
        detention_charges: detention_charges,
        current_value: current_value,
        vehicleLocation: {type: 'Point', coordinates: [latitude, longitude]}
    };

    if (!latitude && !longitude) {
        delete vehicleObject.vehicleLocation;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };
    return _vehicles.findOne({number: number}).then(vehicleFound => {
        if (vehicleFound) {
            throw {msgCode: 9206};
        }
        else {
            let vehicleCreateObject = new _vehicles(vehicleObject);

            return vehicleCreateObject.save();
        }
    }).then(vehicleCreated => {

        resultObject.message = "Vehicle created successfully.";
        resultObject.data = vehicleCreated;

        responseModule.successResponse(res, resultObject);

    }).catch(err => {
        return next(err);
    });
};

let editVehicle = (req, res, next) => {

    const number = _.trim(req.body.vehicle.number),
        _id = (req.body.vehicle._id),
        type = _.trim(req.body.vehicle.type) || "",
        driver = {
            _id : (req.body.vehicle.driver._id),
            name : (req.body.vehicle.driver.name)
        },
        status = req.body.vehicle.status,
        is_token_paid = (req.body.vehicle.is_token_paid),
        is_fixed_veh = (req.body.vehicle.is_fixed_veh),
        annual_token_fee = _.trim(req.body.vehicle.annual_token_fee) || "",
        detention_charges = parseInt(req.body.vehicle.detention_charges) || 0,
        current_value = parseInt(req.body.vehicle.current_value) || 0;

    let vehicleObject = {
        number: number,
        type: type,
        driver : driver,
        status : status,
        is_token_paid: is_token_paid,
        is_fixed_veh: is_fixed_veh,
        annual_token_fee: annual_token_fee,
        detention_charges: detention_charges,
        current_value: current_value
    };


    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return _vehicles.findOneAndUpdate({_id: _id}, {$set: vehicleObject}, {new: true}).exec().then(vehicleCreated => {
        resultObject.message = "Vehicle updated successfully.";
        resultObject.data = vehicleCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

/*let getAllRiders = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchRider = {status: true};

    if (searchText) {
        searchRider.name = {'$regex': searchText, '$options': 'i'};
    }

    let countRiders = 0;

    return _driver.count(searchRider).then(ridersCount => {
        countRiders = ridersCount;

        return _driver.find(searchRider, {
            status: 1,
            name: 1,
            description: 1,
            address: 1,
            phone: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(ridersFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Riders fetched successfully.",
            data: {riders: ridersFound, count: countRiders}
        });


    }).catch(err => {
        return next(err);
    });

}
*/

module.exports = {
    listAllVehicles,
    addVehicle,
    editVehicle,
    /*getAllRiders*/
};
