
const winston = require('winston');

let checkVehicleAddParam = (req, res, next) => {

    req.assert('vehicle.number', 9200).notEmpty();
    // req.assert('address', 9201).notEmpty();
    req.assert('vehicle.type', 9202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

let checkVehicleEditParam = (req, res, next) => {

    req.assert('vehicle.number', 9204).notEmpty();
    req.assert('vehicle._id', 9204).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

module.exports = {
    checkVehicleAddParam,
    checkVehicleEditParam
};
