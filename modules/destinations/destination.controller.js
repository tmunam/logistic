/**
 * Created by Asif on 8/22/2017.
 */


const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _destinations = mongoose.model('destinations'),
    _distributors = mongoose.model('distributions'),
    responseModule = require('../../config/response');


let listAllDestinations = (req, res, next) => {

    const offset = parseInt(req.body.offset) || 0,
        limit = parseInt(req.body.limit) || 10,
        searchTerm = _.trim(req.body.searchTerm) || "";

    let searchDestinations = { status : true };

    if (searchTerm) {
        searchDestinations.destination_name = {'$regex': searchTerm, '$options': 'i'};
    }

    let countDestinations = 0;

    return _destinations.count(searchDestinations).then(destinationCount => {
        countDestinations = destinationCount;

        return _destinations.find(searchDestinations).skip(offset).limit(limit).sort({'destination_name': '1'})
    }).then(destinationsFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Vehicles fetched successfully.",
            data: {destinations: destinationsFound, count: countDestinations}
        });


    }).catch(err => {
        return next(err);
    });

};

let listAllDistributors = (req, res, next) => {

    const offset = parseInt(req.body.offset) || 0,
        limit = parseInt(req.body.limit) || 10,
        searchTerm = _.trim(req.body.searchTerm) || "";

    let searchDistributors = { status : true };

    if (searchTerm) {
        searchDistributors.distribution_name = {'$regex': searchTerm, '$options': 'i'};
        /*searchDistributors = {'$or': [
                {distribution_name : {'$regex': searchTerm, '$options': 'i'}},
                {destination_name : {'$regex': searchTerm, '$options': 'i'}}
            ]};*/

        searchDistributors = {'$and' : [{'$or': [
                    {distribution_name : {'$regex': searchTerm, '$options': 'i'}},
                    {destination_name : {'$regex': searchTerm, '$options': 'i'}}
                ]},{status : true}]}
    }

    let countDistributors = 0;

    return _distributors.count(searchDistributors).then(distributorsCount => {
        countDistributors = distributorsCount;

        return _distributors.find(searchDistributors).skip(offset).limit(limit).sort({'distribution_name': '1'})
    }).then(distributorsFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "distributors fetched successfully.",
            data: {distributors: distributorsFound, count: countDistributors}
        });


    }).catch(err => {
        return next(err);
    });

};

let addDestination = (req, res, next) => {

    var destination_id = _.trim(req.body.destination.destination_id),
        destination_name = _.trim(req.body.destination.destination_name) || "",
        //rate = (req.body.destination.rate),
        distance = (req.body.destination.distance);

    destination_name = destination_name.toUpperCase();

    let destinationObject = {
        destination_id: destination_id,
        destination_name: destination_name,
        distance : distance,
        //rate : rate,
        status : true
    };

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };
    return _destinations.findOne({destination_id: destination_id}).then(destinationFound => {
        if (destinationFound) {
            throw {msgCode: 9206};
        }
        else {
            let destinationCreateObject = new _destinations(destinationObject);

            return destinationCreateObject.save();
        }
    }).then(destinationCreated => {

        resultObject.message = "Destination created successfully.";
        resultObject.data = destinationCreated;

        responseModule.successResponse(res, resultObject);

    }).catch(err => {
        return next(err);
    });
};

let addDistributor = (req, res, next) => {

    var destination_id = _.trim(req.body.distributor.destination._id),
        destination_name = _.trim(req.body.distributor.destination.destination_name) || "",
        distribution_name = _.trim(req.body.distributor.distribution_name) || "";

    destination_name = destination_name.toUpperCase();

    let distributorObject = {
        destination_id: destination_id,
        destination_name: destination_name,
        distribution_name : distribution_name,
        status : true
    };

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    };
    return _distributors.findOne({distributor_name: distribution_name }).then(distributorFound => {
        if (distributorFound) {
            throw {msgCode: 9206};
        }
        else {
            let distributorCreateObject = new _distributors(distributorObject);

            return distributorCreateObject.save();
        }
    }).then(distributorCreated => {

        resultObject.message = "Distributor created successfully.";
        resultObject.data = distributorCreated;

        responseModule.successResponse(res, resultObject);

    }).catch(err => {
        return next(err);
    });
};

let editDestination = (req, res, next) => {

    var destination_id = _.trim(req.body.destination.destination_id),
        _id = _.trim(req.body.destination._id) || "",
        destination_name = _.trim(req.body.destination.destination_name) || "",
        status = req.body.destination.status,
        distance = (req.body.destination.distance);

    destination_name = destination_name.toUpperCase();

    let destinationObject = {
        destination_id: destination_id,
        destination_name: destination_name,
        distance : distance,
        status : status
    };


    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return _destinations.findOneAndUpdate({ _id: _id }, {$set: destinationObject}, {new: true}).exec().then(destinationCreated => {
        resultObject.message = "Vehicle updated successfully.";
        resultObject.data = destinationCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
};

let editDistributor = (req, res, next) => {

    var destination_id = _.trim(req.body.distributor.destination_id),
        destination_name = _.trim(req.body.distributor.destination_name) || "",
        status = req.body.distributor.status,
        _id = _.trim(req.body.distributor._id) || "",
        distribution_name = _.trim(req.body.distributor.distribution_name) || "";

    destination_name = destination_name.toUpperCase();

    let distributorObject = {
        destination_id: destination_id,
        destination_name: destination_name,
        distribution_name : distribution_name,
        status : status
    };


    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    return _distributors.findOneAndUpdate({ _id: _id }, {$set: distributorObject}, {new: true}).exec().then(distributionCreated => {
        resultObject.message = "distribution updated successfully.";
        resultObject.data = distributionCreated;

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
};

/*let getAllRiders = (req, res, next) => {

    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10,
        searchText = _.trim(req.body.searchText) || "";

    let searchRider = {status: true};

    if (searchText) {
        searchRider.name = {'$regex': searchText, '$options': 'i'};
    }

    let countRiders = 0;

    return _driver.count(searchRider).then(ridersCount => {
        countRiders = ridersCount;

        return _driver.find(searchRider, {
            status: 1,
            name: 1,
            description: 1,
            address: 1,
            phone: 1
        }).skip(offset).limit(limit).sort({'createdAt': '-1'})
    }).then(ridersFound => {

        return responseModule.successResponse(res, {
            success: 1,
            message: "Riders fetched successfully.",
            data: {riders: ridersFound, count: countRiders}
        });


    }).catch(err => {
        return next(err);
    });

}
*/

module.exports = {
    listAllDestinations,
    listAllDistributors,
    addDestination,
    addDistributor,
    editDestination,
    editDistributor,
    /*getAllRiders*/
};
