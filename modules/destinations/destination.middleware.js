
const winston = require('winston');

let checkDestinationAddParam = (req, res, next) => {
    req.assert('destination.destination_name', 9202).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

let checkDestinationEditParam = (req, res, next) => {
    req.assert('destination.destination_name', 9204).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
};

module.exports = {
    checkDestinationAddParam,
    checkDestinationEditParam
};
