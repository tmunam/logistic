
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;


let destinationSchema = new Schema({
    //destination_id: {type : Number, required: true},
    //type: {type: String, enum: ['USER', 'PET', 'ADMIN']},
    destination_name: {type : String},
    distance : { type : Number },
    //rate : { type : Number },
    status : {type: Boolean, default: true},
    destinationLocation : {
        type: { type : String },
        coordinates : [Number]
    }
});

destinationSchema.plugin(timestamps);
//riderSchema.index({destinationLocation: '2dsphere'});

module.exports = mongoose.model('destinations', destinationSchema);
