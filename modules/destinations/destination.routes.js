const destinationMiddleware = require('./destination.middleware.js'),
    destinationController = require('./destination.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/destinations/getAllDestinations', passport.isAuthenticated, destinationController.listAllDestinations);
    app.post(version + '/distributor/getAllDistributors', passport.isAuthenticated, destinationController.listAllDistributors);
    app.post(version + '/destinations/addDestination', passport.isAuthenticated , destinationMiddleware.checkDestinationAddParam, destinationController.addDestination);
    app.post(version + '/distributor/addDistributor', passport.isAuthenticated , destinationController.addDistributor);
    app.post(version + '/destinations/editDestination', passport.isAuthenticated, destinationMiddleware.checkDestinationEditParam, destinationController.editDestination);
    app.post(version + '/distributor/editDistributor', passport.isAuthenticated, destinationController.editDistributor);
    //app.get(version + '/rider/getAllRidersArchived', passport.isAuthenticated, vehicleController.getAllRiders);

};