/**
 * Created by Asif on 8/16/2017.
 */

const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    orders = mongoose.model('orders'),
    tempOrder = mongoose.model('tempOrder'),
    responseModule = require('../../config/response'),
    products = mongoose.model('products'),
    orderHelper = require('./orders.helper'),
    promoCodes = mongoose.model('promoCodes'),
    User = mongoose.model('User'),
    lib = require('../common/common.library'),
    moment = require('moment'),
    ratings = mongoose.model('ratings'),
    sms = require('../../config/sms'),
    agendaHelper = require('../agenda/agenda.helper'),
    async = require('async');

function sendVerificationSms(number, text, cb) {
    let smsObject = {
        to: number,
        text: text,
    };

    sms.sendMessage(smsObject, (err) => {
        if (err) {
            return cb(err);
        }
        else {
            return cb();
        }
    });
}

let proceedToCheckout = (req, res, next) => {

    const userId = _.trim(req.user._id),
        productsArray = req.body.products,
        promoCodeId = req.body.promoCodeId,
        finalResult = {},
        checkoutId = req.body.checkoutId,
        promoCode = _.trim(req.body.promoCode).toLowerCase() || "";

    const fetchProductIds = _.map(productsArray, '_id');

    const currentDate = moment.utc().unix();

    return products.find({_id: {$in: fetchProductIds}}, {
        price: 1,
        name: 1,
        unit: 1,
        images: 1,
        isDiscounted: 1,
        discountedPrice: 1,
        discountPercentage: 1
    }).then(productsFound => {
        if (productsFound && productsFound.length) {
            return orderHelper.generateOrderSummary(productsArray, productsFound, userId);
        }
        else {
            throw {msgCode: 9102};
        }
    }).then(objectReturned => {
        finalResult.objectReturned = objectReturned;

        if (promoCode) {
            return promoCodes.findOne({
                code: promoCode, status: true,
                startDate: {$lte: currentDate},
                expiryDate: {$gte: currentDate}
            });
        }
        else {
            return null;
        }
    }).then(promoCode => {
        let tempOrderObject = {
            orderAmount: finalResult.objectReturned.totalAmount,
            deliveryCharges: 0,
            products: finalResult.objectReturned.productFinalArray,
            promoCodeApplied: 0,
            promoCodeUsed: null,
            discountedAmount: 0,
            totalAmount: parseFloat(finalResult.objectReturned.totalAmount),
            userId: userId
        }
        if (tempOrderObject.totalAmount < 100) {
            tempOrderObject.totalAmount += 25;
            tempOrderObject.deliveryCharges = 25;
        }
        else if (tempOrderObject.totalAmount == 100 || tempOrderObject.totalAmount <= 249) {
            tempOrderObject.totalAmount += 50;
            tempOrderObject.deliveryCharges = 50;
        }
        else if (tempOrderObject.totalAmount == 250 || tempOrderObject.totalAmount <= 499) {
            tempOrderObject.totalAmount += 75;
            tempOrderObject.deliveryCharges = 75;
        }
        else if (tempOrderObject.totalAmount == 500 || tempOrderObject.totalAmount <= 1499) {
            tempOrderObject.totalAmount += 100;
            tempOrderObject.deliveryCharges = 100;
        }
        else {
            tempOrderObject.deliveryCharges = 0;
        }

        if (promoCode) {
            tempOrderObject.promoCodeApplied = true;
            tempOrderObject.promoCodeUsed = promoCode._id;

            tempOrderObject.discountedAmount = finalResult.objectReturned.totalAmount * (promoCode.percentage / 100);
            tempOrderObject.totalAmount = finalResult.objectReturned.totalAmount - tempOrderObject.discountedAmount;
            tempOrderObject.totalAmount = tempOrderObject.totalAmount + tempOrderObject.deliveryCharges;
        }

        if (checkoutId) {
            return tempOrder.findOneAndUpdate({_id: checkoutId}, tempOrderObject, {new: true});
        }
        else {
            return new tempOrder(tempOrderObject).save();
        }
    }).then(tempOrderSaved => {
        tempOrderSaved = tempOrderSaved.toObject();
        tempOrderSaved.totalAmount = parseFloat(tempOrderSaved.totalAmount.toFixed(2));
        tempOrderSaved.orderAmount = parseFloat(tempOrderSaved.orderAmount.toFixed(2));
        if (tempOrderSaved.promoCodeUsed == null) {
            tempOrderSaved.promoCodeUsed = "";
        }

        delete tempOrderSaved.__v;
        tempOrderSaved.checkoutId = tempOrderSaved._id;
        return responseModule.successResponse(res, {
            success: 1,
            message: "Product summary fetched successfully.",
            data: tempOrderSaved
        });
    }).catch(err => {
        return next(err);
    });
}

let confirmOrder = (req, res, next) => {

    const checkoutId = _.trim(req.body.checkoutId),
        addressId = _.trim(req.body.addressId),
        orderNote = _.trim(req.body.orderNote) || "",
        userId = _.trim(req.user._id),
        orderObject = {};

    return User.findOne({_id: userId}, {address: 1}).then(userFound => {

        let userAddress = _.find(userFound.address, (address) => {
            return address._id == addressId;
        });

        if (!userAddress) {
            throw {msgCode: 9109};
        }

        orderObject.shippingAddress = {};

        orderObject.shippingAddress.address = userAddress.address;
        orderObject.shippingAddress.location = userAddress.location;

        return tempOrder.findOne({_id: checkoutId});

    }).then(tempOrder => {

        let expTime = new Date(),
            orderCancelTime = expTime.setMinutes(expTime.getMinutes() + 30);
        orderObject.orderCancelTime = orderCancelTime;
        orderObject.isCancelledEnable = true;
        orderObject.orderId = tempOrder.orderId;
        orderObject.totalAmount = parseFloat(tempOrder.totalAmount.toFixed(2));
        orderObject.deliveryCharges = tempOrder.deliveryCharges;
        orderObject.orderAmount = tempOrder.orderAmount;
        orderObject.orderNote = orderNote;
        orderObject.orderDate = new Date();
        orderObject.status = 'Pending';
        orderObject.products = tempOrder.products;
        orderObject.promoCodeApplied = tempOrder.promoCodeApplied;
        orderObject.promoCodeUsed = tempOrder.promoCodeUsed;
        orderObject.discountedAmount = tempOrder.discountedAmount;
        orderObject.userId = tempOrder.userId;
        orderObject.checkoutId = tempOrder.checkoutId;

        return new orders(orderObject).save();
    }).then(orderCreated => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Order created successfully.",
            data: orderCreated
        });
    }).catch(err => {
        return next(err);
    });
}


let getOrderHistory = (req, res, next) => {

    const userId = _.trim(req.user._id),
        offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10;

    return orders.find({userId: userId}, {
        orderId: 1,
        totalAmount: 1,
        orderDate: 1,
        status: 1,
        deliveryCharges: 1
    }).skip(offset).limit(limit).sort({createdAt: '-1'}).lean().then(orders => {
        _.forEach(orders, (order, index) => {
            orders[index].orderDate = lib.formatDate(order.orderDate);
            orders[index].totalAmount = parseFloat(order.totalAmount.toFixed(2));

        });

        return responseModule.successResponse(res, {
            success: 1,
            message: "Order history fetched successfully.",
            data: {orders: orders}
        });
    }).catch(err => {
        return next(err);
    });
}

let getOrderDetail = (req, res, next) => {
    const orderId = _.trim(req.params.orderId);

    return orders.findOne({orderId: orderId}, {
        orderId: 1,
        orderAmount: 1,
        shippingAddress: 1,
        orderNote: 1,
        orderDate: 1,
        status: 1,
        deliveryDate: 1,
        products: 1,
        promoCodeApplied: 1,
        promoCodeUsed: 1,
        discountedAmount: 1,
        totalAmount: 1,
        paymentMethod: 1,
        userId: 1,
        checkoutId: 1,
        isCancelledEnable: 1,
        deliveryCharges: 1
    }).lean().then(orderFound => {
        orderFound.orderDate = lib.formatDate(orderFound.orderDate);

        return responseModule.successResponse(res, {
            success: 1,
            message: "Order history fetched successfully.",
            data: {order: orderFound}
        });
    }).catch(err => {
        return next(err);
    });
}


let verifyPromoCode = (req, res, next) => {

    const promoCode = _.trim(req.body.promoCode).toLowerCase(),
        currentDate = Math.round(new Date().getTime() / 1000),
        checkoutId = _.trim(req.body.checkoutId);

    let promoCodeDetail = {};

    return promoCodes.findOne({
        code: promoCode, status: true,
        startDate: {$lte: currentDate},
        expiryDate: {$gte: currentDate}
    }).then(promoCode => {
        if (promoCode) {
            promoCodeDetail = promoCode;
            return tempOrder.findOne({_id: checkoutId}, {'__v': 0}).lean();
        }
        else {
            throw {msgCode: 9107};
        }
    }).then(tempOrderLoaded => {
        if (tempOrderLoaded) {
            tempOrderLoaded.promoCodeApplied = true;
            tempOrderLoaded.promoCodeUsed = promoCodeDetail._id;

            tempOrderLoaded.discountedAmount = tempOrderLoaded.orderAmount * (promoCodeDetail.percentage / 100);
            tempOrderLoaded.totalAmount = tempOrderLoaded.orderAmount - tempOrderLoaded.discountedAmount + tempOrderLoaded.deliveryCharges;

            tempOrderLoaded.checkoutId = tempOrderLoaded._id;
            tempOrderLoaded.promoCodeId = promoCodeDetail._id;
            tempOrderLoaded.totalAmount = parseFloat(tempOrderLoaded.totalAmount.toFixed(2));
            return responseModule.successResponse(res, {
                success: 1,
                message: "Promo code exists.",
                data: tempOrderLoaded
            });
        }
        else {
            throw {msgCode: 9117};
        }

    }).catch(err => {
        return next(err);
    });
}

let addPromoCode = (req, res, next) => {
    const code = _.trim(req.body.promoCode).toLowerCase(),
        startDate =moment.utc(req.body.startDate).hours(0).unix(),
        endDate = moment.utc(req.body.endDate).hours(24).unix(),
        promoText = _.trim(req.body.promoText);

    let percentage = parseInt(req.body.percentage),
        dates = moment();

    let promoObject = {
        code: code,
        startDate: startDate,
        expiryDate: endDate,
        percentage: percentage,
        promoText: promoText
    }
    if (startDate > endDate) {
        return next({msgCode: 9120});
    } else {
        promoObject.startDate = startDate;
    }
    if (startDate < endDate) {
        promoObject.expiryDate = endDate;
    } else {
        return next({msgCode: 9120});
    }
    if (code) {
        promoObject.code = code.toLowerCase();
    }


    if (percentage >= 100) {
        return next({msgCode: 9119});
    }
    else {
        promoObject.percentage = percentage;
    }

    return promoCodes.findOne({code: code}).then(codeFound => {
        if (codeFound) {
            throw {msgCode: 9114};
        }
        else {
            let promoCreatedObject = new promoCodes(promoObject);
            return promoCreatedObject.save();
        }
    }).then(promoCode => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Promo code created successfully.",
            data: {promoCode: promoCode}
        });
    }).catch(err => {
        return next(err);
    });
}

let editPromoCode = (req, res, next) => {
    let promoCodeId = _.trim(req.body.promoCodeId),
        code = _.trim(req.body.promoCode),
        startDate =moment.utc(req.body.startDate).unix(),
        endDate = moment.utc(req.body.endDate).hours(23).unix(),
        percentage = parseInt(req.body.percentage),
        status = -1,
        promoText = _.trim(req.body.promoText),
        currentDate = moment.utc().unix();
    if (req.body.hasOwnProperty('status')) {
        if (parseInt(req.body.status)) {
            status = 1;
        }
        else {
            status = 0;
        }
    }

    let updateObject = {};

    if (code) {
        updateObject.code = code.toLowerCase();
    }

    if (startDate) {
        updateObject.startDate = startDate;
    }

    if (endDate > startDate) {
        updateObject.expiryDate = endDate;
    }
    else {
        return next({msgCode: 9120});
    }

    if (percentage) {
        updateObject.percentage = percentage;
    }

    if (startDate > endDate) {
        return next({msgCode: 9120});
    } else {
        updateObject.startDate = startDate;
    }
    if (startDate < endDate) {
        updateObject.expiryDate = endDate;
    } else {
        return next({msgCode: 9120});
    }
    if (code) {
        updateObject.code = code.toLowerCase();
    }

    if (promoText) {
        updateObject.promoText = promoText;
    }

    return promoCodes.findOneAndUpdate({_id: promoCodeId}, {$set: updateObject}, {new: true}).then(promoCode => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Promo code edited successfully.",
            data: {promoCode: promoCode}
        });
    }).catch(err => {
        return next(err);
    });
}

let getAllPromoCodes = (req, res, next) => {
    const offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10;

    let promoCodesCount = 0;

    return promoCodes.count({}).then(countOfCodes => {
        promoCodesCount = countOfCodes;
        return promoCodes.find({}).skip(offset).limit(limit).sort({'createdAt': -1})
    }).then(promoCodes => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Promo codes fetched successfully.",
            data: {promoCodes: promoCodes, count: promoCodesCount}
        });
    }).catch(err => {
        return next(err);
    });
}


let getOrderHistoryAdmin = (req, res, next) => {

    const userId = _.trim(req.user._id),
        offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 10;

    let count = 0;

    return orders.count({}).then(orderCount => {
        count = orderCount;
        return orders.find({}, {
            orderId: 1,
            totalAmount: 1,
            orderDate: 1,
            riderId: 1,
            status: 1,
            deliveryCharges: 1
        }).skip(offset).limit(limit).sort({createdAt: '-1'}).populate({
            path: 'riderId',
            model: 'rider',
            select: {'name': 1},
        }).lean();

    }).then(orders => {
        _.forEach(orders, (orders) => {
            orders.totalAmount = parseFloat(orders.totalAmount.toFixed(2));
            if (!orders.riderId) {
                orders.riderId = {name: "Please Select Rider"};
            }
        })

        return responseModule.successResponse(res, {
            success: 1,
            message: "Order history fetched successfully.",
            data: {orders: orders, orderCount: count}
        });

    }).catch(err => {
        return next(err);
    });
}

let getAnalyticsCount = (req, res, next) => {

    const delivered = _.trim(req.body.status);

    let finalObject = {
        ordersCount: 0,
        deliveredOrders: 0,
        averageAppRatings: 0,
        totalRevenue: 0
    };
    return orders.count({}).then(ordersCount => {
        finalObject.ordersCount = ordersCount;

        return ratings.aggregate(
            [
                {
                    $group: {
                        _id: null,
                        averageAppRatings: {$avg: "$appRatings"}
                    }
                }
            ]);
    }).then(averageAppRatings => {

        finalObject.averageAppRatings = averageAppRatings;
        return orders.aggregate(
            [
                {$match: {status: "Delivered"}},
                {
                    $group: {
                        _id: null,
                        totalAmount: {$sum: "$totalAmount"}
                    }
                }
            ]);

    }).then(totalRevenue => {

        finalObject.totalRevenue = totalRevenue;

        return orders.count({status: 'Delivered'});
    }).then(deliveredOrdersCount => {
        finalObject.deliveredOrders = deliveredOrdersCount;

        return responseModule.successResponse(res, {
            success: 1,
            message: "Count of all Delivered orders fetched successfully.",
            data: {finalObject}
        });
    }).catch(err => {
        return next(err);
    });
}

let updateOrderStatus = (req, res, next) => {
    const orderId = parseInt(req.body.orderId),
        status = _.trim(req.body.status);


    return orders.findOne({orderId: orderId}).populate('userId').then(orderFound => {
        if (orderFound.status == "Cancelled by User") {
            throw next({msgCode: 9121});
        }
        else {
            if (orderFound.isCancelledEnable == false) {
                if (status == "Cancelled by User") {

                    throw next({msgCode: 9122});
                }
                else {
                    return orders.findOneAndUpdate({orderId: orderId}, {$set: {status: status}}, {new: true}).populate('userId');
                }
            }
            else if (orderFound.isCancelledEnable == true) {
                if (status == "Cancelled by User") {
                    return orders.findOneAndUpdate({orderId: orderId}, {
                        $set: {
                            "status": status,
                            "isCancelledEnable": false
                        }
                    }, {new: true});
                }
                else {
                    return orders.findOneAndUpdate({orderId: orderId}, {$set: {status: status}}, {new: true}).populate('userId');
                }
            }
        }

    }).then(statusUpdated => {
        if (statusUpdated.status == 'Delivered') {
            async.parallel([
                    (mail)=> {
                        let vars = {
                            name: _.trim(statusUpdated.userId.name),
                            orderId: statusUpdated.orderId,
                            orderAmount: statusUpdated.orderAmount,
                            orderDate: lib.formatDate(statusUpdated.orderDate),
                            discountedAmount: statusUpdated.discountedAmount,
                            totalAmount: statusUpdated.totalAmount,
                            Products: statusUpdated.products,
                            address: statusUpdated.shippingAddress.address
                        }
                        lib.sendEmail(statusUpdated.userId.email, 'Thank you for Shopping with Us!', vars, 'order_delivered', (err) => {
                            if (err) {
                                return mail(err);
                            } else {
                                return mail();
                            }
                        });

                    },
                    (sms)=> {
                        let mobile = statusUpdated.userId.mobileNo,
                            text = 'Hey ' + statusUpdated.userId.name + ', We hope that we were able to make your Grocery Shopping Experience an Amazing one. We would also like to thank you for your feedback on our services. Thank You! Team RAFT Bazaar';
                        sendVerificationSms(mobile, text, (err) => {
                            if (err) {
                                return sms(err);
                            }
                            else {
                                return sms();
                            }
                        });
                    }

                ],
                (err) => {
                    if (err) {
                        return next(err);
                    }
                    else {
                        return responseModule.successResponse(res, {
                            success: 1,
                            message: "Order status updated successfully.",
                            data: {}
                        });
                    }
                });
        }
        else if (statusUpdated.status == 'Confirmed') {
            async.parallel([
                    (mail) => {
                        let vars = {
                            name: _.trim(statusUpdated.userId.name),
                            orderId: statusUpdated.orderId,
                            orderAmount: statusUpdated.orderAmount,
                            orderDate: lib.formatDate(statusUpdated.orderDate),
                            discountedAmount: statusUpdated.discountedAmount,
                            totalAmount: statusUpdated.totalAmount,
                            Products: statusUpdated.products,
                            address: statusUpdated.shippingAddress.address
                        }
                        lib.sendEmail(statusUpdated.userId.email, 'Your Order Confirmation!', vars, 'order_posted', (err) => {
                            if (err) {
                                return mail(err);
                            } else {
                                return mail();
                            }
                        });

                    },
                    (mailCb) => {
                        return User.find({userType: {$eq: "ADMIN"}}, {
                            email: 1,
                        }).then(adminFound => {
                            let vars = {
                                name: _.trim(req.user.name),
                                orderId: statusUpdated.orderId,
                                orderAmount: statusUpdated.orderAmount,
                                orderDate: lib.formatDate(statusUpdated.orderDate),
                                discountedAmount: statusUpdated.discountedAmount,
                                totalAmount: statusUpdated.totalAmount,
                                Products: statusUpdated.products,
                                address: statusUpdated.shippingAddress.address
                            }
                            lib.sendEmail(adminFound[0].email, 'Raft: Order Received', vars, 'order_recieved', (err) => {
                                if (err) {
                                    return mailCb(err);
                                } else {
                                    return mailCb();
                                }
                            });
                        });
                    },
                    (sms)=> {
                        let mobile = statusUpdated.userId.mobileNo,
                            text = 'Your order has been confirmed. It will be delivered to your Doorstep by our Associate Rider within 99 Minutes.Thank You!';

                        sendVerificationSms(mobile, text, (err) => {
                            if (err) {
                                return sms(err);
                            }
                            else {
                                return sms();
                            }
                        });
                    }
                ],
                (err) => {
                    if (err) {
                        return next(err);
                    }
                    else {
                        return responseModule.successResponse(res, {
                            success: 1,
                            message: "Order status updated successfully.",
                            data: {}
                        });
                    }
                });
        }
        else {
            return responseModule.successResponse(res, {
                success: 1,
                message: "Order status updated successfully.",
                data: {}
            });
        }


    }).catch(err => {
        return next(err);
    });

};

let updateOrderRider = (req, res, next) => {
    const orderId = parseInt(req.body.orderId),
        riderId = _.trim(req.body.riderId);

    return orders.findOneAndUpdate({orderId: orderId}, {$set: {riderId: riderId}}, {new: true}).then(statusUpdated => {
        return responseModule.successResponse(res, {
            success: 1,
            message: "Order updated successfully.",
            data: {}
        });
    }).catch(err => {
        return next(err);
    });
}

let notifyUserUsingSms = (req, res, next) => {

    let promoCodeId = _.trim(req.params.promoCodeId);
    const currentDate = Math.round(new Date().getTime() / 1000);

    return promoCodes.findOne({
        _id: promoCodeId, allSend: false, status: true,
        startDate: {$lt: currentDate},
        expiryDate: {$gt: currentDate}
    }, {
        allSend: 1,
        status: 1,
        offset: 1,
        code: 1,
        promoText: 1,
        percentage: 1
    }).then(promoCodeFound => {
        if (promoCodeFound) {
            agendaHelper.pushJob({
                promoText: promoCodeFound.promoText,
                percentage: promoCodeFound.percentage,
                code: promoCodeFound.code,
                promoCodeId: promoCodeId
            }, 2);
        }
        else {
            throw {msgCode: 9107};
        }
    }).catch(err => {
        return next(err);
    });

}

module.exports = {
    proceedToCheckout,
    confirmOrder,
    verifyPromoCode,
    getOrderHistory,
    getOrderDetail,
    addPromoCode,
    editPromoCode,
    getAllPromoCodes,
    getAnalyticsCount,
    getOrderHistoryAdmin,
    updateOrderStatus,
    updateOrderRider,
    notifyUserUsingSms
}
