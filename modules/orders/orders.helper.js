/**
 * Created by Asif on 8/16/2017.
 */

const winston = require('winston'),
    _ = require('lodash'),
    agendaHelper = require('../agenda/agenda.helper');

let generateOrderSummary = (productArrayFromUser, productArrayFromDB, userId) => {

    return new Promise((resolve, reject) => {
        try {
            let tempOrderArray = [],
                totalAmount = 0;

            _.forEach(productArrayFromUser, (arrayObject) => {

                let productDesc = _.find(productArrayFromDB, (productArrayObject) => {
                    return productArrayObject._id.toString() == arrayObject._id.toString();
                });

                let productAmount = 0,
                    discountedAmount = 0;

                if (productDesc && productDesc.isDiscounted) {
                    productAmount = productDesc.price * arrayObject.quantity;
                    discountedAmount = (productDesc.discountedPrice * arrayObject.quantity).toFixed(1);
                    totalAmount += parseFloat(discountedAmount);
                }
                else {
                    productAmount = productDesc.price * arrayObject.quantity;
                    totalAmount += parseFloat(productAmount);
                }

                tempOrderArray.push({
                    productId: arrayObject._id,
                    quantity: arrayObject.quantity,
                    priceCount: productAmount,
                    isDiscounted: productDesc.isDiscounted,
                    discountPercentage: productDesc.isDiscounted ? productDesc.discountPercentage : 0,
                    name: productDesc.name,
                    unit: productDesc.unit,
                    discountedPrice: discountedAmount,
                    userId: userId
                });
            });
            return resolve({totalAmount: totalAmount.toFixed(1), productFinalArray: tempOrderArray});
        } catch (err) {
            winston.error(err);
            return reject({msgCode: 9103});
        }
    });
}

let notificationOnOrderStatusChange = (status, userId, orderId) => { 
    if (status == "Confirmed" ||
        status == "Delivered" ||
        status == "Canceled") {
        let statusCase = 2,
            notificationMessage = '';
        switch (status) {
            case "Confirmed":
                statusCase = 2;
                notificationMessage = 'Your order has been confirmed. It will be delivered to your Doorstep by our Associate Rider within 99 Minutes. Thank You!';
                break;
            case "Delivered":
                statusCase = 3;
                notificationMessage = 'Your order has been delivered. Thank You for using RAFT Bazaar. Please rate our services.';
                break;
            case "Canceled":
                statusCase = 4;
                notificationMessage = 'Sorry we are unable to process your order at this time. Please contact us for further help.';
                break;
        }


        agendaHelper.pushJob({
            userId: userId,
            message: notificationMessage,
            messageType: 'order_status',
            senderName: 'RAFT Bazaar',
            badge: 1,
            resource: orderId,
            statusCase: statusCase
        }, 1);
    }

}
    module.exports = {
        generateOrderSummary,
        notificationOnOrderStatusChange
    }