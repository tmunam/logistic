
const ordersMiddleware = require('./orders.middleware.js'),
    ordersController = require('./orders.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/order/checkout', passport.isAuthenticated, ordersMiddleware.checkoutParamsValidation, ordersController.proceedToCheckout);

    app.post(version + '/order/completeOrder', passport.isAuthenticated, ordersMiddleware.orderValidation, ordersController.confirmOrder);

    app.get(version + '/order/orderHistory/:offset/:limit', passport.isAuthenticated, ordersController.getOrderHistory);

    app.get(version + '/order/orderHistoryAdmin/:offset/:limit', passport.isAuthenticated, ordersController.getOrderHistoryAdmin);

    app.get(version + '/order/:orderId', passport.isAuthenticated, ordersMiddleware.orderDetailValidation, ordersController.getOrderDetail);

    app.post(version + '/order/checkPromoCode', passport.isAuthenticated, ordersMiddleware.promoCodeValidation, ordersController.verifyPromoCode);

    app.post(version + '/order/addPromoCode', passport.isAuthenticated, ordersMiddleware.promoCodeAddValidation, ordersController.addPromoCode);

    app.post(version + '/order/editPromoCode', passport.isAuthenticated, ordersMiddleware.promoCodeEditValidation, ordersController.editPromoCode);

    app.get(version + '/order/getPromoCodes/:offset/:limit', passport.isAuthenticated, ordersController.getAllPromoCodes);

    app.get(version + '/order/getAnalyticsCount/:offset/:limit', passport.isAuthenticated, ordersController.getAnalyticsCount);

    app.post(version + '/order/updateOrderStatus', passport.isAuthenticated,ordersMiddleware.orderUpdateValidation , ordersController.updateOrderStatus);

    app.post(version + '/order/addRiderToOrder', passport.isAuthenticated,ordersMiddleware.riderAttachValidation , ordersController.updateOrderRider);

    app.get(version + '/order/sendPromoCodeNotifications/:promoCodeId', passport.isAuthenticated,passport.isAuthorized('ADMIN'), ordersMiddleware.promoCodeEditValidation, ordersController.notifyUserUsingSms);
};