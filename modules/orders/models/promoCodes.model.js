/**
 * Created by Asif on 8/16/2017.
 */
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let promoCodesSchema = new Schema({
    code: {type: String},
    isPromoNew: {type:Boolean, default:true},
    allSend: {type: Boolean, default: false},
    status: {type: Boolean, default: true},
    startDate: Number,
    expiryDate: Number,
    offset: {type: Number, default:0},
    percentage: Number,
    promoText : {type : String}
});

promoCodesSchema.plugin(timestamps);

module.exports = mongoose.model('promoCodes', promoCodesSchema);