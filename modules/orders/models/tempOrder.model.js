/**
 * Created by Asif on 8/16/2017.
 */

'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');

let tempOrderSchema = new Schema({
    orderId: {type: Number},
    orderAmount: {type: Number, required: true},
    products: [
        {
            productId: {type: Schema.Types.ObjectId, ref: 'products'},
            quantity: {type: Number},
            priceCount: {type: Number},
            discountPercentage: {type: Number},
            discountedPrice: {type: Number},
            name: String,
            unit: String,
            isDiscounted: Boolean
        }
    ],
    promoCodeApplied: {type: Boolean, default: false},
    promoCodeUsed: {type: Schema.Types.ObjectId, ref: 'promoCodes'},
    discountedAmount: {type: Number},
    totalAmount: {type: Number},
    deliveryCharges: {type: Number},
    paymentMethod: {type: String, enum: ['Cash on Delivery'], default: 'Cash on Delivery'},
    userId: {type: Schema.Types.ObjectId, ref: 'User'}
});

tempOrderSchema.plugin(timestamps);
tempOrderSchema.plugin(autoIncrement.plugin, {model: 'tempOrder', field: 'orderId'});
tempOrderSchema.index({orderId: 1}, {background: true, name: 'IDX_ORDER_ID'});

module.exports = mongoose.model('tempOrder', tempOrderSchema);