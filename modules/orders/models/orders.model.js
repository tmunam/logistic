/**
 * Created by Asif on 8/16/2017.
 */
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let orderSchema = new Schema({
    orderId: {type: Number},
    orderAmount: {type: Number, required: true},
    shippingAddress: {address: String, location: {type: String, coordinates: [Number]}},
    orderNote: {type: String},
    orderDate: Date,
    status: {type: String, enum: ['Pending', 'Confirmed', 'In Process', 'Delivered','Canceled'], default: 'Pending'},
    deliveryDate: Date,
    products: [
        {
            productId: {type: Schema.Types.ObjectId, ref: 'products'},
            quantity: {type: Number},
            priceCount: {type: Number},
            discountPercentage: {type: Number},
            discountedPrice: {type: Number},
            name: String,
            unit: String,
            isDiscounted: Boolean
        }
    ],
    orderCancelTime: Date,
    isCancelledEnable: {type: Boolean, default: true},
    promoCodeApplied: {type: Boolean, default: false},
    promoCodeUsed: {type: Schema.Types.ObjectId, ref: 'promoCodes'},
    discountedAmount: {type: Number},
    totalAmount: {type: Number},
    paymentMethod: {type: String, enum: ['Cash on Delivery'], default: 'Cash on Delivery'},
    deliveryCharges: {type: Number},
    userId: {type: Schema.Types.ObjectId, ref: 'User'},
	checkoutId : {type : Schema.Types.ObjectId, ref : 'tempOrder'},
    riderId : {type : Schema.Types.ObjectId, ref : 'rider'}
});

orderSchema.plugin(timestamps);
orderSchema.index({orderId : 1}, {background: true, name: 'IDX_ORDER_ID'});

module.exports = mongoose.model('orders', orderSchema);