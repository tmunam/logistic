/**
 * Created by Asif on 8/16/2017.
 */


let checkoutParamsValidation = (req, res, next) => {

    req.assert('products', 9100).isArray();

    req.assert('products', 9101).arrayIsNotEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}


let orderValidation = (req, res, next) => {

    req.assert('checkoutId', 9104).notEmpty();

    req.assert('addressId', 9105).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let promoCodeValidation = (req, res, next) => {
    req.assert('promoCode', 9106).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let orderDetailValidation = (req, res, next) => {
    req.assert('orderId', 9106).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let promoCodeAddValidation = (req, res, next) => {
    req.assert('promoCode', 9106).notEmpty();
    req.assert('startDate', 9110).notEmpty();
    req.assert('endDate', 9111).notEmpty();
    req.assert('percentage', 9112).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let promoCodeEditValidation = (req, res, next) => {
    req.assert('promoCodeId', 9113).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let orderUpdateValidation = (req, res, next) => {
    req.assert('orderId', 9108).notEmpty();
    req.assert('status', 9115).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}

let riderAttachValidation = (req, res, next) => {
    req.assert('orderId', 9108).notEmpty();
    req.assert('riderId', 9116).notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return next(errors[0]);
    }

    return next();
}



module.exports = {
    checkoutParamsValidation,
    orderValidation,
    promoCodeValidation,
    orderDetailValidation,
    promoCodeAddValidation,
    promoCodeEditValidation,
    orderUpdateValidation,
    riderAttachValidation
}