
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;


let distributionsSchema = new Schema({
    //destination_id: {type : Number, required: true},
    //type: {type: String, enum: ['USER', 'PET', 'ADMIN']},
    distribution_name: {type : String},
    destination_id : { type : Schema.Types.ObjectId , ref : 'destinations' },
    destination_name: {type : String},
    //rate : { type : Number },
    status : {type: Boolean, default: true},
    distributionLocation : {
        type: { type : String },
        coordinates : [Number]
    }
});

distributionsSchema.plugin(timestamps);
//riderSchema.index({destinationLocation: '2dsphere'});

module.exports = mongoose.model('distributions', distributionsSchema);
