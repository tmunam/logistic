
const keywordsController = require('./keyword.controller.js'),
    passport = require('../../config/passport');

module.exports = function (app, version) {

    app.post(version + '/keywords/addKeywords',passport.isAuthenticated, keywordsController.addKeywords);
    app.post(version + '/keywords/getAllKeywords',passport.isAuthenticated, keywordsController.getAllKeywords);

};