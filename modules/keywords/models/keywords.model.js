
'use strict';

let mongoose = require('mongoose'),
    timestamps = require('mongoose-timestamp'),
    Schema = mongoose.Schema;

let keywordsSchema = new Schema({
    keyWords: {type: String},
    products: [
        {type : Schema.Types.ObjectId, ref : 'products'}
    ]
});

keywordsSchema.plugin(timestamps);
module.exports = mongoose.model('keywords', keywordsSchema);