const mongoose = require('mongoose'),
    winston = require('winston'),
    _ = require('lodash'),
    _keyWords = mongoose.model('keywords'),
    responseModule = require('../../config/response');


let addKeywords = (req, res, next) => {

    let keyWords = _.trim(req.body.keyWords),
        products = req.body.products || [];
    let keywordObject = {
        keyWords: keyWords,
        products: products

    }
        let resultObject = {
            success: 1,
            message: "",
            data: ""
        }
        let queryObject = {
            name: ""
        }

        return _keyWords.findOne({keyWords: keyWords}).then(keywordFound => {
            if (keywordFound) {
                return keywordFound;
            }
            else {
                let keyWordSaveObject = new _keyWords(keywordObject);
                return keyWordSaveObject.save({keyWords: keyWords});
            }

        }).then(keywordCreated => {
            resultObject.message = "keyword created successfully.";
            resultObject.data = keywordCreated;
            responseModule.successResponse(res, resultObject);
        }).catch(err => {
            return next(err);
        });
}

let getAllKeywords = (req, res, next) => {
    let searchKeywords = _.trim(req.body.searchText) || "",
        offset = parseInt(req.params.offset) || 0,
        limit = parseInt(req.params.limit) || 20,
        sortByName = parseInt(req.body.sortByName) || 0;

    let sortObject = {};

    if (sortByName) {
        sortObject.keyWords = -1;
    }
    else {
        sortObject.keyWords = 1;
    }

    let resultObject = {
        success: 1,
        message: "",
        data: ""
    }

    let queryObject = {
        keyWords: "",
    }

    if (searchKeywords) {
        queryObject.keyWords = {'$regex': searchKeywords, '$options': 'i'};
    }
    else {
        delete queryObject.keyWords;
    }
    return _keyWords.find({}, queryObject).sort(sortObject).skip(offset).limit(limit).populate({
        path: 'products',
        model: 'products',
        select: {'name': 1}
    }).then(keywordsArray => {
        if (keywordsArray && keywordsArray.length) {
            resultObject.message = "Keyword fetched successfully.";
            resultObject.data = {keywords: keywordsArray};
        }
        else {
            resultObject.message = "No keyword found.";
            resultObject.data = {keywords: []};
        }

        responseModule.successResponse(res, resultObject);
    }).catch(err => {
        return next(err);
    });
}

module.exports = {
    addKeywords,
    getAllKeywords
}
